<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Teknik_penilaian_model extends MY_Model{
	public $_table = 'teknik_penilaian';
	public $primary_key = 'teknik_penilaian_id';
	public $belongs_to = array(
		'semester' => array('model' => 'semester_model', 'primary_key' => 'semester_id'),
	);//1 ke 1
	public $before_create = array( 'timestamps' );
	public $before_update = array( 'timestamps' );
    protected function timestamps($data){
        $loggeduser = $this->ion_auth->user()->row();
        $data['last_sync'] = date('Y-m-d H:i:s');
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        return $data;
    }
}