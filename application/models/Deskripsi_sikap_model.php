<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Deskripsi_sikap_model extends MY_Model{
	public $_table = 'deskripsi_sikap';
	public $primary_key = 'deskripsi_sikap_id';
	public $belongs_to = array(
		'siswa' => array('model' => 'siswa_model', 'primary_key' => 'siswa_id'),
		'rombongan_belajar' => array('model' => 'rombongan_belajar_model', 'primary_key' => 'rombongan_belajar_id'),
	);//1 ke 1
	public $before_create = array( 'timestamps' );
	public $before_update = array( 'timestamps' );
    protected function timestamps($data){
        $loggeduser = $this->ion_auth->user()->row();
        $data['last_sync'] = date('Y-m-d H:i:s');
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        return $data;
    }
}