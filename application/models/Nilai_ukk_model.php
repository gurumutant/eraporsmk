<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Nilai_ukk_model extends MY_Model{
	public $_table = 'nilai_ukk';
	public $primary_key = 'nilai_ukk_id';
	public $before_create = array( 'timestamps', 'generate_uuid' );
	public $before_update = array( 'timestamps' );
    protected function timestamps($data){
        $loggeduser = $this->ion_auth->user()->row();
        $data['last_sync'] = date('Y-m-d H:i:s');
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        return $data;
    }
	protected function generate_uuid($data){
		$data['nilai_ukk_id'] = gen_uuid();
        return $data;
    }
}