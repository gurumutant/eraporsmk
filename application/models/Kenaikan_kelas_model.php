<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kenaikan_kelas_model extends MY_Model{
	public $_table = 'kenaikan_kelas';
	public $primary_key = 'kenaikan_kelas_id';
	public $before_create = array( 'timestamps' );
	public $before_update = array( 'timestamps' );
    protected function timestamps($data){
        $loggeduser = $this->ion_auth->user()->row();
        $data['last_sync'] = date('Y-m-d H:i:s');
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        return $data;
    }
}