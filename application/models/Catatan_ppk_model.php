<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Catatan_ppk_model extends MY_Model{
	public $_table = 'catatan_ppk';
	public $primary_key = 'catatan_ppk_id';
	public $belongs_to = array(
		'siswa' => array('model' => 'siswa_model', 'primary_key' => 'siswa_id')
	);//1 ke 1
	public $before_create = array( 'timestamps' );
	public $before_update = array( 'timestamps' );
    protected function timestamps($data){
        $loggeduser = $this->ion_auth->user()->row();
        $data['last_sync'] = date('Y-m-d H:i:s');
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        return $data;
    }
}