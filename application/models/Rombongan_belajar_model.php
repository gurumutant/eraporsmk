<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rombongan_belajar_model extends MY_Model{
	public $_table = 'rombongan_belajar';
	public $primary_key = 'rombongan_belajar_id';
	public $has_many = array(
		'pembelajaran' => array('model' => 'pembelajaran_model', 'primary_key' => 'rombongan_belajar_id'),
	);//1 ke 1
	public $before_create = array( 'timestamps' );
	public $before_update = array( 'timestamps' );
    protected function timestamps($data){
       $loggeduser = $this->ion_auth->user()->row();
        $data['last_sync'] = date('Y-m-d H:i:s');
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        return $data;
    }
}