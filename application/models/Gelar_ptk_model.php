<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gelar_ptk_model extends MY_Model{
	public $_table = 'gelar_ptk';
	public $primary_key = 'gelar_ptk_id';
	public $belongs_to = array( 'gelar' => array('model' => 'gelar_model', 'primary_key' => 'gelar_akademik_id'));//1 ke 1
	public $before_create = array( 'timestamps' );
	public $before_update = array( 'timestamps' );
    protected function timestamps($data){
        $loggeduser = $this->ion_auth->user()->row();
        $data['last_sync'] = date('Y-m-d H:i:s');
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        return $data;
    }
}