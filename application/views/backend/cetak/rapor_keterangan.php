<style>
.spasi_setengah{margin-bottom:5px;}
.lurus{text-align:justify;}
ol.kosong{margin-left:0px;}
</style>
<div class="strong text-center">PETUNJUK PENGISIAN</div>
<br />
<div class="lurus">
<ol class="kosong">
	<li class="spasi_setengah">Rapor merupakan ringkasan hasil penilaian terhadap seluruh aktivitas pembelajaran yang dilakukan siswa dalam kurun waktu tertentu;</li>
	<li class="spasi_setengah">Rapor dipergunakan selama siswa yang bersangkutan mengikuti seluruh program pembelajaran di Sekolah Menengah Kejuruan tersebut;</li>
	<li class="spasi_setengah">Identitas Sekolah diisi dengan data yang sesuai dengan keberadaan Sekolah Menengah Kejuruan;</li>
	<li class="spasi_setengah">Keterangan tentang diri Siswa diisi lengkap;</li>
	<li class="spasi_setengah">Rapor harus dilengkapi dengan pas foto berwarna (3x4) dan pengisiannya dilakukan oleh Wali Kelas;</li>
	<li class="spasi_setengah">Deskripsi sikap spiritual diambil dari hasil observasi terutama pada mata pelajaran Pendidikan Agama dan Budi pekerti, dan PPKn;</li>
	<li class="spasi_setengah">Deskripsi sikap sosial diambil dari hasil observasi pada semua mata pelajaran;</li>
	<li class="spasi_setengah">Deskripsi pada kompetensi sikap ditulis dengan kalimat positif untuk aspek yang sangat baik atau kurang baik;</li>
	<li class="spasi_setengah">Capaian siswa dalam kompetensi pengetahuan dan kompetensi keterampilan ditulis dalam bentuk angka, predikat dan deskripsi untuk masing-masing mata pelajaran;</li>
	<li class="spasi_setengah">Predikat ditulis dalam bentuk huruf sesuai kriteria;</li>
	<li class="spasi_setengah">Kolom KKM (Kriteria Ketuntasan Minimal) merupakan acuan bagi kriteria kenaikan kelas sehingga wali kelas wajib menerangkan konsekuensi ketuntasan belajar tersebut kepada orangtua/wali;</li>
	<li class="spasi_setengah">Batas bawah predikat C adalah nilai KKM;</li>
	<li class="spasi_setengah">Rentang predikat capaian kompetensi dapat dihitung dengan cara:
	<table border="0" width="200px;">
		<tr>
			<td width="80">
			Rentang =
			</td>
			<td align="center" colspan="3">
			<u>100 &minus; nilai KKM</u><br />3
			</td>
		</tr>
		<tr>
			<td colspan="4">Contoh:</td>
		</tr>
		<tr>
			<td align="center">
			<u>100 &minus; 60</u><br />3
			</td>
			<td align="center">=</td>
			<td align="center">
			<u>40</u><br />3
			</td>
			<td align="center">= 13,3</td>
		</tr>
	</table>
	<p>Setelah dibulatkan maka rentang predikat ada pada 13 atau 14. Maka rentang predikatnya:</p>
	<table border="0" width="200px;">
		<tr>
			<td width="100">Sangat Baik (A)</td>
			<td width="5">:</td>
			<td width="95">88&minus;100</td>
		</tr>
		<tr>
			<td>Baik (B)</td>
			<td>:</td>
			<td>74&minus;87</td>
		</tr>
		<tr>
			<td>Cukup (C)</td>
			<td>:</td>
			<td>60&minus;73</td>
		</tr>
		<tr>
			<td>Kurang (D)</td>
			<td>:</td>
			<td>&lt;60</td>
		</tr>
	</table>
	<li class="spasi_setengah">Deskripsi pada kompetensi pengetahuan dan kompetensi keterampilan ditulis dengan kalimat positif sesuai capaian tertinggi dan terendah yang diperoleh siswa. Apabila capaian kompetensi dasar yang diperoleh dalam muatan pelajaran itu sama, kolom deskripsi ditulis berdasarkan capaian yang diperoleh;</li>
	<li class="spasi_setengah">Laporan Praktik Kerja Lapangan diisi berdasarkan kegiatan praktik kerja yang diikuti oleh siswa di industri atau perusahaan mitra;</li>
	<li class="spasi_setengah">Laporan Ekstrakurikuler diisi berdasarkan kegiatan ekstrakurikuler yang diikuti oleh siswa;</li>
	<li class="spasi_setengah">Saran-saran wali kelas diisi berdasarkan kegiatan yang perlu mendapatkan perhatian siswa;</li>
	<li class="spasi_setengah">Prestasi diisi dengan prestasi yang dicapai oleh siswa dalam bidang akademik dan non akademik;</li>
	<li class="spasi_setengah">Ketidakhadiran diisi dengan data akumulasi ketidakhadiran siswa karena sakit, izin, atau tanpa keterangan selama satu semester;</li>
	<li class="spasi_setengah">Tanggapan orangtua/wali adalah tanggapan atas pencapaian hasil belajar siswa;</li>
	<li class="spasi_setengah">Keterangan pindah keluar sekolah diisi dengan alasan kepindahan. Sedangkan pindah masuk diisi dengan sekolah asal.</li>
</ol>
</div>