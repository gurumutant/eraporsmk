<?php
$s = $this->siswa->get($siswa_id);
$sekolah = $this->sekolah->get($sekolah_id);
$setting = $this->settings->get(1);
$rombel = $this->rombongan_belajar->get($rombel_id);
$find_ppk = $this->catatan_ppk->find("semester_id = $ajaran_id AND siswa_id = '$siswa_id'");
$catatan_ppk_id = ($find_ppk) ? $find_ppk->catatan_ppk_id : gen_uuid();
$find_karakter_integritas = $this->nilai_karakter->find("semester_id = $ajaran_id AND catatan_ppk_id = '$catatan_ppk_id' AND sikap_id = 19");
$find_karakter_religius = $this->nilai_karakter->find("semester_id = $ajaran_id AND catatan_ppk_id = '$catatan_ppk_id' AND sikap_id = 1");
$find_karakter_nasionalis = $this->nilai_karakter->find("semester_id = $ajaran_id AND catatan_ppk_id = '$catatan_ppk_id' AND sikap_id = 20");
$find_karakter_mandiri = $this->nilai_karakter->find("semester_id = $ajaran_id AND catatan_ppk_id = '$catatan_ppk_id' AND sikap_id = 7");
$find_karakter_gotong_royong = $this->nilai_karakter->find("semester_id = $ajaran_id AND catatan_ppk_id = '$catatan_ppk_id' AND sikap_id = 21");
?>
<br>
<div class="strong">G.&nbsp;&nbsp;Deskripsi Perkembangan Karakter</div>
<table width="100%" border="1">
	<thead>
		<tr>
			<th style="vertical-align:middle;width: 100px;" align="center" class="text-center">Karakter yang dibangun</th>
			<th style="vertical-align:middle;" align="center" class="text-center">Deskripsi</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="vertical-align:middle;" align="center" class="text-center">Integritas</td>
			<td><?php echo ($find_karakter_integritas) ? $find_karakter_integritas->deskripsi : '-'; ?></td>
		</tr>
		<tr>
			<td style="vertical-align:middle;" align="center" class="text-center">Religius</td>
			<td><?php echo ($find_karakter_religius) ? $find_karakter_religius->deskripsi : '-'; ?></td>
		</tr>
		<tr>
			<td style="vertical-align:middle;" align="center" class="text-center">Nasionalis</td>
			<td><?php echo ($find_karakter_nasionalis) ? $find_karakter_nasionalis->deskripsi : '-'; ?></td>
		</tr>
		<tr>
			<td style="vertical-align:middle;" align="center" class="text-center">Mandiri</td>
			<td><?php echo ($find_karakter_mandiri) ? $find_karakter_mandiri->deskripsi : '-'; ?></td>
		</tr>
		<tr>
			<td style="vertical-align:middle;" align="center" class="text-center">Gotong-royong</td>
			<td><?php echo ($find_karakter_gotong_royong) ? $find_karakter_gotong_royong->deskripsi : '-'; ?></td>
		</tr>
	</tbody>
</table>
<br>
<div class="strong">H.&nbsp;&nbsp;Catatan Perkembangan Karakter</div>
<table width="100%" border="1">
  <tr>
    <td style="padding:10px;"><?php echo ($find_ppk) ? $find_ppk->capaian : '-'; ?></td>
  </tr>
</table>