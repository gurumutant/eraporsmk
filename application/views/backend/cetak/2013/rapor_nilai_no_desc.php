<?php
$uri = $this->uri->segment_array();
if(isset($uri[3])){
	if($uri[3] == 'review_nilai'){
		$border = '';
		$class = 'table table-bordered';
	} else {
		$border = 'border="1"';
		$class = 'table';
	}
}
$s = $this->siswa->get($siswa_id);
$sekolah = $this->sekolah->get($sekolah_id);
$setting = $this->settings->get(1);
$rombel = $this->rombongan_belajar->get($rombel_id);
$ajaran = $this->semester->get($ajaran_id);
$mapel_a = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 1 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 1 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
foreach($mapel_a as $mapela){
	$mapel_a_id[] = $mapela->mata_pelajaran_id;
}
if(isset($mapel_a_id)){
	$mapel_agama = array(100011070, 100012050, 100013010, 100014140, 100015010, 100016010);
	$mapel_a = filter_agama_mapel($ajaran_id,$mapel_agama, $mapel_a_id,$s->agama_id);
}
$mapel_b = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 2 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 2 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
$mapel_c1 = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 3 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 3 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
$mapel_c2 = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 4 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 4 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
$mapel_c3 = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 5 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 5 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
$mapel_tambahan = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 99 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 99 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
$check_2018 = check_2018();
if($check_2018){
	$teks_kkm = 'SKM';
} else {
	$teks_kkm = 'KKM';
}
?>
<table <?php echo $border; ?> class="<?php echo $class; ?>">
	<thead>
  <tr>
    <th style="vertical-align:middle;width: 2px;" align="center" rowspan="2">No</th>
    <th style="vertical-align:middle;width: 200px;" rowspan="2">Mata Pelajaran</th>
    <th colspan="3" align="center" class="text-center">Pengetahuan</th>
	<th colspan="3" align="center" class="text-center">Keterampilan</th>
  </tr>
  <tr>
    <th align="center" style="width:10px;" class="text-center"><?php echo $teks_kkm; ?></th>
	<th align="center" style="width:10px;" class="text-center">Angka</th>
	<th align="center" style="width:10px;" class="text-center">Predikat</th>
    <th align="center" style="width:10px;" class="text-center"><?php echo $teks_kkm; ?></th>
	<th align="center" style="width:10px;" class="text-center">Angka</th>
	<th align="center" style="width:10px;" class="text-center">Predikat</th>
  </tr>
	</thead>
	<tbody>
<?php
	$i=1;
	if($mapel_a){
	?>
		<tr>
			<td colspan="10"><b style="font-size: 13px;">Kelompok A</b></td>
		</tr>
<?php
	foreach($mapel_a as $mapela){
		$nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $mapela, $s->siswa_id);
		$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $mapela, $s->siswa_id);
	?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $mapela); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapela); ?></td>
			<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapela),$nilai_pengetahuan_value); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapela); ?></td>
			<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapela),$nilai_keterampilan_value); ?></td>
		</tr>
	<?php
	$i++; }
	} 
	?>
		
	<?php
	$i=isset($i) ? $i : 1;
	if($mapel_b){
	?>
	<tr>
		<td colspan="10"><b style="font-size: 13px;">Kelompok B</b></td>
	</tr>
	<?php
	foreach($mapel_b as $mapelb){
		$nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $mapelb->mata_pelajaran_id, $s->siswa_id);
		$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $mapelb->mata_pelajaran_id, $s->siswa_id);
	?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $mapelb->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapelb->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapelb->mata_pelajaran_id),$nilai_pengetahuan_value); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapelb->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapelb->mata_pelajaran_id),$nilai_keterampilan_value); ?></td>
		</tr>
	<?php
	$i++; }
	} 
	?>
	<tr>
		<td colspan="10"><b style="font-size: 13px;">Kelompok C (Peminatan)</b></td>
	</tr>
	<?php if($mapel_c1){ ?>
        <tr>
            <td colspan="10"><b>C1. Dasar Bidang Keahlian</b></td>
        </tr>
    <?php 
		$i=isset($i) ? $i : 1;
		foreach($mapel_c1 as $mapelc1) {
            $nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $mapelc1->mata_pelajaran_id, $s->siswa_id);
			$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $mapelc1->mata_pelajaran_id, $s->siswa_id);
		?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $mapelc1->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapelc1->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapelc1->mata_pelajaran_id),$nilai_pengetahuan_value); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapelc1->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapelc1->mata_pelajaran_id),$nilai_keterampilan_value); ?></td>
            </tr>
            <?php
            $i++;
        }
	} 
	?>
	<?php if($mapel_c2){ ?>
        <tr>
            <td colspan="10"><b>C2. Dasar Program Keahlian</b></td>
        </tr>
    <?php 
		$i=isset($i) ? $i : 1;
        foreach($mapel_c2 as $mapelc2) {
            $nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $mapelc2->mata_pelajaran_id, $s->siswa_id);
			$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $mapelc2->mata_pelajaran_id, $s->siswa_id);
		?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $mapelc2->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapelc2->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapelc2->mata_pelajaran_id),$nilai_pengetahuan_value, 1); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapelc2->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapelc2->mata_pelajaran_id),$nilai_keterampilan_value, 1); ?></td>
            </tr>
            <?php
            $i++;
        }
	} 
	?>
	<?php 
		if($mapel_c3){
	?>
	<tr>
		<td colspan="10"><b>C3. Paket Keahlian</b></td>
	</tr>
	<?php 
		$i=isset($i) ? $i : 1;
        foreach($mapel_c3 as $mapelc3) {
            $nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $mapelc3->mata_pelajaran_id, $s->siswa_id);
			$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $mapelc3->mata_pelajaran_id, $s->siswa_id);
		?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $mapelc3->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapelc3->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapelc3->mata_pelajaran_id),$nilai_pengetahuan_value, 1); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapelc3->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapelc3->mata_pelajaran_id),$nilai_keterampilan_value, 1); ?></td>
            </tr>
            <?php
            $i++;
        }
	} 
	?>
	<?php
	if($mapel_tambahan){
	?>
	<tr>
		<td colspan="10"><b style="font-size: 13px;">Muatan Lokal</b></td>
	</tr>
	<?php
	$i=isset($i) ? $i : 1;
	foreach($mapel_tambahan as $tambahan){
		$nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $tambahan->mata_pelajaran_id, $s->siswa_id);
		$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $tambahan->mata_pelajaran_id, $s->siswa_id);
	?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $tambahan->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$tambahan->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$tambahan->mata_pelajaran_id),$nilai_pengetahuan_value); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$tambahan->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$tambahan->mata_pelajaran_id),$nilai_keterampilan_value); ?></td>
		</tr>
	<?php
	$i++;
	}
	} 
	?>
	</tbody>
</table>