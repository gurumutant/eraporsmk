<?php
$uri = $this->uri->segment_array();
if(isset($uri[3])){
	if($uri[3] == 'review_desc'){
		$border = '';
		$class = 'table table-bordered';
	} else {
		$border = 'border="1"';
		$class = 'table';
	}
}
$s = $this->siswa->get($siswa_id);
$sekolah = $this->sekolah->get($sekolah_id);
$setting = $this->settings->get(1);
$rombel = $this->rombongan_belajar->get($rombel_id);
$ajaran = $this->semester->get($ajaran_id);
$mapel_a = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 1 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 1 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
foreach($mapel_a as $mapela){
	$mapel_a_id[] = $mapela->mata_pelajaran_id;
}
if(isset($mapel_a_id)){
	$mapel_agama = array(100011070, 100012050, 100013010, 100014140, 100015010, 100016010);
	$mapel_a = filter_agama_mapel($ajaran_id,$mapel_agama, $mapel_a_id,$s->agama_id);
}
$mapel_b = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 2 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 2 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
$mapel_c1 = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 3 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 3 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
$mapel_c2 = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 4 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 4 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
$mapel_c3 = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 5 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 5 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
$mapel_tambahan = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 99 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 99 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
?>
<table <?php echo $border; ?> class="<?php echo $class; ?>">
	<thead>
  <tr>
    <th style="vertical-align:middle;width: 2px;" align="center" rowspan="2">No</th>
    <th style="vertical-align:middle;width: 400px;" rowspan="2">Mata Pelajaran</th>
    <th align="center" class="text-center">Pengetahuan</th>
	<th align="center" class="text-center">Keterampilan</th>
  </tr>
  <tr>
	<th align="center" class="text-center">Deskripsi</th>
	<th align="center" class="text-center">Deskripsi</th>
  </tr>
	</thead>
	<tbody>
	<?php
	$i=1;
	if($mapel_a){
	?>
		<tr>
			<td colspan="10"><strong>Kelompok A</strong></td>
		</tr>
	<?php
	foreach($mapel_a as $mapela){
		$deskripsi_pengetahuan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapela, $s->siswa_id,1);
		$deskripsi_keterampilan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapela, $s->siswa_id,2);
	
	?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $mapela); ?></td>
			<td valign="top"><?php echo $deskripsi_pengetahuan; ?></td>
			<td valign="top"><?php echo $deskripsi_keterampilan; ?></td>
		</tr>
	<?php
	$i++; }
	} 
	$i=isset($i) ? $i : 1;
	?>
	<?php
	if($mapel_b){
	?>
	<tr>
		<td colspan="10"><strong>Kelompok B</strong></td>
	</tr>
	<?php
	foreach($mapel_b as $mapelb){
		$deskripsi_pengetahuan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapelb->mata_pelajaran_id, $s->siswa_id,1);
		$deskripsi_keterampilan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapelb->mata_pelajaran_id, $s->siswa_id,2);
	?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $mapelb->mata_pelajaran_id); ?></td>
			<td valign="top"><?php echo $deskripsi_pengetahuan; ?></td>
			<td valign="top"><?php echo $deskripsi_keterampilan; ?></td>
		</tr>
	<?php
	$i++; }
	} 
	?>
		<tr>
			<td colspan="10"><strong>Kelompok C (Peminatan)</strong></td>
		</tr>
    <?php 
		if($mapel_c1){
	?>
		<tr>
            <td colspan="10"><b>C1. Dasar Bidang Keahlian</b></td>
        </tr>
	<?php
		$i=isset($i) ? $i : 1;
		foreach($mapel_c1 as $mapelc1) {
            $deskripsi_pengetahuan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapelc1->mata_pelajaran_id, $s->siswa_id,1);
			$deskripsi_keterampilan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapelc1->mata_pelajaran_id, $s->siswa_id,2);
		?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $mapelc1->mata_pelajaran_id); ?></td>
			<td valign="top"><?php echo $deskripsi_pengetahuan; ?></td>
			<td valign="top"><?php echo $deskripsi_keterampilan; ?></td>
		</tr>
            <?php
            $i++;
        }
	} 
	?>
    <?php 
		if($mapel_c2){
	?>
		<tr>
            <td colspan="10"><b>C2. Dasar Program Keahlian</b></td>
        </tr>
	<?php
		$i=isset($i) ? $i : 1;
		foreach($mapel_c2 as $mapelc2) {
            $deskripsi_pengetahuan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapelc2->mata_pelajaran_id, $s->siswa_id,1);
			$deskripsi_keterampilan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapelc2->mata_pelajaran_id, $s->siswa_id,2);
		?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $mapelc2->mata_pelajaran_id); ?></td>
			<td valign="top"><?php echo $deskripsi_pengetahuan; ?></td>
			<td valign="top"><?php echo $deskripsi_keterampilan; ?></td>
		</tr>
            <?php
            $i++;
        }
	} ?>
    <?php 
		if($mapel_c3){ 
	?>
		<tr>
            <td colspan="10"><b>C3. Paket Keahlian</b></td>
        </tr>
	<?php
		$i=isset($i) ? $i : 1;
		foreach($mapel_c3 as $mapelc3) {
            $deskripsi_pengetahuan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapelc3->mata_pelajaran_id, $s->siswa_id,1);
			$deskripsi_keterampilan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapelc3->mata_pelajaran_id, $s->siswa_id,2);
		?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $mapelc3->mata_pelajaran_id); ?></td>
			<td valign="top"><?php echo $deskripsi_pengetahuan; ?></td>
			<td valign="top"><?php echo $deskripsi_keterampilan; ?></td>
		</tr>
            <?php
            $i++;
        }
	} 
	?>
	<?php
	if($mapel_tambahan){
	?>
		<tr>
			<td colspan="10"><b style="font-size: 13px;">Muatan Lokal</b></td>
		</tr>
	<?php
	$i=isset($i) ? $i : 1;
	foreach($mapel_tambahan as $tambahan){
		$deskripsi_pengetahuan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $tambahan->mata_pelajaran_id, $s->siswa_id,1);
		$deskripsi_keterampilan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $tambahan->mata_pelajaran_id, $s->siswa_id,2);
	?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $tambahan->mata_pelajaran_id); ?></td>
			<td valign="top"><?php echo $deskripsi_pengetahuan; ?></td>
			<td valign="top"><?php echo $deskripsi_keterampilan; ?></td>
		</tr>
	<?php
	$i++;
	}
	}
	?>
	</tbody>
</table>