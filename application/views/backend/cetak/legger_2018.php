<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
				<div class="table-responsive no-padding">
					<a href="#" id ="download" role='button' class="btn btn-success">Download</a>
					<table class="table table-bordered table-hover table2excelasd" id="legger2018">
						<tr>
							<td colspan="2">Nama Peserta Didik</td>
							<td>SKM</td>
							<?php foreach($data_siswa['data'] as $siswa){ ?>
							<td><?php echo strtoupper($siswa->nama); ?></td>
							<?php } ?>
							<td>Rata-rata</td>
						</tr>
						<?php foreach($data_mapel as $key=>$mapel){ ?>
						<tr>
							<td colspan="2"><?php echo $mapel->nama_mata_pelajaran; ?></td>
							<td class="text-center"><?php echo get_kkm($mapel->semester_id, $mapel->rombongan_belajar_id, $mapel->mata_pelajaran_id); ?></td>
							<?php foreach($data_siswa['data'] as $siswa){ ?>
							<td>-</td>
							<?php } ?>
						</tr>
						<tr>
							<td colspan="2">Pengetahuan</td>
							<td class="text-center"><?php echo get_kkm($mapel->semester_id, $mapel->rombongan_belajar_id, $mapel->mata_pelajaran_id); ?></td>
							<?php foreach($data_siswa['data'] as $siswa){ ?>
							<td>&nbsp;</td>
							<?php } ?>
						</tr>
						<?php
						$this->db->select('kd_id, id_kompetensi');
						$this->db->from('kd_nilai as a');
						$this->db->join('rencana_penilaian as b', 'a.rencana_penilaian_id = b.rencana_penilaian_id');
						$this->db->where('b.sekolah_id', $loggeduser->sekolah_id);
						$this->db->where('b.semester_id', $mapel->semester_id);
						$this->db->where('b.mata_pelajaran_id', $mapel->mata_pelajaran_id);
						$this->db->where('b.rombongan_belajar_id', $mapel->rombongan_belajar_id);
						$this->db->where('b.kompetensi_id', 1);
						$this->db->order_by('kd_id', 'asc');
						$this->db->group_by('kd_id, id_kompetensi');
						$query = $this->db->get();
						$all_kd_nilai = $query->result();
						foreach($all_kd_nilai as $kd_nilai){
							$get_kd = $this->kompetensi_dasar->get($kd_nilai->kd_id);
						?>
						<tr>
							<td><?php echo $kd_nilai->id_kompetensi; ?></td>
							<td><?php echo ($get_kd) ? $get_kd->kompetensi_dasar : '-'; ?></td>
							<td class="text-center"><?php echo get_kkm($mapel->semester_id, $mapel->rombongan_belajar_id, $mapel->mata_pelajaran_id); ?></td>
							<?php 
							$total_nilai = 0;
							foreach($data_siswa['data'] as $siswa){ 
								$nilai_per_kd = get_nilai_siswa_by_kd($kd_nilai->kd_id, $mapel->semester_id, $kompetensi_id, $mapel->rombongan_belajar_id, $mapel->mata_pelajaran_id, $siswa->siswa_id);
								$total_nilai += $nilai_per_kd;
							?>
							<td class="text-center"><?php echo ($nilai_per_kd) ? number_format($nilai_per_kd,0) : 0; ?></td>
							<?php } ?>
							<td class="text-center"><?php $rata_rata = $total_nilai / count($data_siswa['data']); echo number_format($rata_rata,0); ?></td>
						</tr>
						<?php } ?>
						<tr>
							<td colspan="2">Keterampilan</td>
							<td class="text-center"><?php echo get_kkm($mapel->semester_id, $mapel->rombongan_belajar_id, $mapel->mata_pelajaran_id); ?></td>
							<?php foreach($data_siswa['data'] as $siswa){ ?>
							<td>&nbsp;</td>
							<?php } ?>
						</tr>
						<?php
						$this->db->select('kd_id, id_kompetensi');
						$this->db->from('kd_nilai as a');
						$this->db->join('rencana_penilaian as b', 'a.rencana_penilaian_id = b.rencana_penilaian_id');
						$this->db->where('b.sekolah_id', $loggeduser->sekolah_id);
						$this->db->where('b.semester_id', $mapel->semester_id);
						$this->db->where('b.mata_pelajaran_id', $mapel->mata_pelajaran_id);
						$this->db->where('b.rombongan_belajar_id', $mapel->rombongan_belajar_id);
						$this->db->where('b.kompetensi_id', 2);
						$this->db->order_by('kd_id', 'asc');
						$this->db->group_by('kd_id, id_kompetensi');
						$query = $this->db->get();
						$all_kd_nilai = $query->result();
						foreach($all_kd_nilai as $kd_nilai){
							$get_kd = $this->kompetensi_dasar->get($kd_nilai->kd_id);
						?>
						<tr>
							<td><?php echo $kd_nilai->id_kompetensi; ?></td>
							<td><?php echo ($get_kd) ? $get_kd->kompetensi_dasar : '-'; ?></td>
							<td class="text-center"><?php echo get_kkm($mapel->semester_id, $mapel->rombongan_belajar_id, $mapel->mata_pelajaran_id); ?></td>
							<?php 
							$total_nilai = 0;
							foreach($data_siswa['data'] as $siswa){ 
								$nilai_per_kd = get_nilai_siswa_by_kd($kd_nilai->kd_id, $mapel->semester_id, $kompetensi_id, $mapel->rombongan_belajar_id, $mapel->mata_pelajaran_id, $siswa->siswa_id);
								$total_nilai += $nilai_per_kd;
							?>
							<td class="text-center"><?php echo ($nilai_per_kd) ? number_format($nilai_per_kd,0) : 0; ?></td>
							<?php } ?>
							<td class="text-center"><?php $rata_rata = $total_nilai / count($data_siswa['data']); echo number_format($rata_rata,0); ?></td>
						</tr>
						<?php } ?>
						<?php } ?>
					</table>
					<script>
					function generateExcel(el) {
						var clon = el.clone();
						var html = clon.wrap('<div>').parent().html();
						//add more symbols if needed...
						while (html.indexOf('�') != -1) html = html.replace(/�/g, '&aacute;');
						while (html.indexOf('�') != -1) html = html.replace(/�/g, '&eacute;');
						while (html.indexOf('�') != -1) html = html.replace(/�/g, '&iacute;');
						while (html.indexOf('�') != -1) html = html.replace(/�/g, '&oacute;');
						while (html.indexOf('�') != -1) html = html.replace(/�/g, '&uacute;');
						while (html.indexOf('�') != -1) html = html.replace(/�/g, '&ordm;');
						html = html.replace(/<td>/g, "<td>&nbsp;");
						window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
					}
					$("#download").click(function (event) {
						generateExcel($("#legger2018"));
					});
					</script>
				</div>
			</div>
		</div>
	</div>
</div>