<?php
$s = $this->siswa->get($siswa_id);
$sekolah = $this->sekolah->get($sekolah_id);
$setting = $this->settings->get(1);
$rombel = $this->rombongan_belajar->get($rombel_id);
$ajaran = $this->semester->get($ajaran_id);
$anggota_rombel = $this->anggota_rombel->find("semester_id = $ajaran_id AND rombongan_belajar_id = '$rombel_id' AND siswa_id = '$siswa_id'");
$kenaikan = $this->kenaikan_kelas->find("semester_id = $ajaran_id AND anggota_rombel_id = '$anggota_rombel->anggota_rombel_id'");
$deskripsi_kenaikan_kelas = '';
if($ajaran->semester == 2){ 
	if($rombel->tingkat == 12){
	$deskripsi_kenaikan_kelas .= '<p class="strong">Keputusan :</p>';
	$deskripsi_kenaikan_kelas .= '<p>Berdasarkan hasil yang dicapai pada semester 1, 2, 3, 4, 5 dan 6, peserta didik dinyatakan</p>';
	$deskripsi_kenaikan_kelas .= '<p>Lulus / Tidak Lulus</p>';
	} else {
		if($kenaikan){
			$status_kenaikan = ($kenaikan->status == 1) ? 'Naik ke kelas '.get_nama_rombel($kenaikan->ke_kelas) : 'Tinggal di kelas '.$rombel->nama;
			$deskripsi_kenaikan_kelas .= '<p>'.$status_kenaikan.'</p>';
		} else {
			$deskripsi_kenaikan_kelas .= '<p>Belum dilakukan kenaikan kelas</p>';
		}
	} 
} else {
	$deskripsi_kenaikan_kelas .= '<p>-</p>';
}
?>
<br>
<div class="strong">F.&nbsp;&nbsp;Kenaikan Kelas</div>
<table width="100%" border="1">
  <tr>
    <td style="padding:10px;"><?php echo $deskripsi_kenaikan_kelas; ?></td>
  </tr>
</table>
<br>
<br>
<table width="100%">
  <tr>
    <td style="width:40%">
		<p>Orang Tua/Wali</p><br>
<br>
<br>
<br>
<br>
<br>
		<p>...................................................................</p>
	</td>
	<td style="width:20%"></td>
    <td style="width:40%"><p><?php echo $sekolah->kabupaten; ?>, <?php echo TanggalIndo($setting->tanggal_rapor); ?><br>Wali Kelas</p><br>
<br>
<br>
<br>
<br>
<br>
<p>
<u><?php echo get_nama_guru($rombel->guru_id); ?></u><br>
NIP. <?php echo get_nip_guru($rombel->guru_id); ?>
</td>
  </tr>
</table>
<table width="100%" style="margin-top:10px;">
  <tr>
    <td style="width:100%;text-align:center;">
		<p>Mengetahui,<br>Kepala Sekolah</p>
	<br>
<br>
<br>
<br>
<br>
<p><u><?php echo get_nama_guru($sekolah->guru_id); ?></u><br>
NIP. <?php echo get_nip_guru($sekolah->guru_id); ?>
</p>
	</td>
  </tr>
</table>