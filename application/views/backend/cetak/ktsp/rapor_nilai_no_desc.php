<?php
$uri = $this->uri->segment_array();
if(isset($uri[3])){
	if($uri[3] == 'review_nilai'){
		$border = '';
		$class = 'table table-bordered';
	} else {
		$border = 'border="1"';
		$class = 'table';
	}
}
$s = $this->siswa->get($siswa_id);
$sekolah = $this->sekolah->get($sekolah_id);
$setting = $this->settings->get(1);
$rombel = $this->rombongan_belajar->get($rombel_id);
$ajaran = $this->semester->get($ajaran_id);
$mapel_normatif = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 11 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 11 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
foreach($mapel_normatif as $normatif){
	$normatif_id[] = $normatif->mata_pelajaran_id;
}
if(isset($normatif_id)){
	$mapel_agama = array(100011000, 100012000, 100013000, 100014000, 100015000, 100016000, 109011000);
	$mapel_normatif = filter_agama_mapel($ajaran_id,$mapel_agama, $normatif_id,$s->agama_id);
}
$mapel_adaptif = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 12 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 12 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
$mapel_produktif = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 13 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 13 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
$mapel_tambahan = $this->pembelajaran->with('mata_pelajaran')->find_all("kelompok_id = 99 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL OR kelompok_id = 99 AND semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL", '*','no_urut ASC');
$check_2018 = check_2018();
if($check_2018){
	$teks_kkm = 'SKM';
} else {
	$teks_kkm = 'KKM';
}
?>
<table <?php echo $border; ?> class="<?php echo $class; ?>">
	<thead>
  <tr>
    <th style="vertical-align:middle;width: 2px;" align="center" rowspan="2">No</th>
    <th style="vertical-align:middle;width: 200px;" rowspan="2" align="center" class="text-center">Mata Pelajaran</th>
    <th colspan="4" align="center" class="text-center">Pengetahuan</th>
    <th colspan="4" align="center" class="text-center">Keterampilan</th>
  </tr>
  <tr>
    <th align="center" style="width:10px;" class="text-center"><?php echo $teks_kkm; ?></th>
    <th align="center" style="width:10px;" class="text-center">Angka</th>
    <th align="center" style="width:10px;" class="text-center">Predikat</th>
    <th align="center" style="width:10px;" class="text-center"><?php echo $teks_kkm; ?></th>
    <th align="center" style="width:10px;" class="text-center">Angka</th>
    <th align="center" style="width:10px;" class="text-center">Predikat</th>
  </tr>
	</thead>
	<tbody>
    <?php
        $i=1;
		if($mapel_normatif){
	?>
		<tr>
            <td colspan="10" class="strong"><b style="font-size: 13px;">I NORMATIF</b></td>
        </tr>
	<?php
			foreach($mapel_normatif as $mapela){
				$nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $mapela, $s->siswa_id);
				$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $mapela, $s->siswa_id);
			?>
			<tr>
				<td align="center" valign="top"><?php echo $i; ?></td>
				<td valign="top"><?php echo get_nama_mapel($mapela); ?></td>
				<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapela); ?></td>
				<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
				<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapela),$nilai_pengetahuan_value); ?></td>
				<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapela); ?></td>
				<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
				<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapela),$nilai_keterampilan_value); ?></td>
			</tr>
		<?php
		$i++; }
    } 
	?>
	<?php
    if($mapel_adaptif){
	?>
		<tr>
			<td colspan="10" class="strong"><b style="font-size: 13px;">II ADAPTIF</b></td>
		</tr>
	<?php
		$i=isset($i) ? $i : 1;
		foreach($mapel_adaptif as $mapelb){
			$nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $mapelb->mata_pelajaran_id, $s->siswa_id);
			$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $mapelb->mata_pelajaran_id, $s->siswa_id);
		?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel($mapelb->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapelb->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapelb->mata_pelajaran_id),$nilai_pengetahuan_value); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapelb->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapelb->mata_pelajaran_id),$nilai_keterampilan_value); ?></td>
        </tr>
    <?php
    	$i++; }
    } 
	?>
	<?php
    if($mapel_produktif){
	?>
		<tr>
			<td colspan="10" class="strong"><b style="font-size: 13px;">III PRODUKTIF</b></td>
		</tr>
		<tr>
			<td colspan="10"><b>Produktif (Dasar Bidang Keahlian)</b></td>
		</tr>
	<?php
        $i=isset($i) ? $i : 1;
        foreach($mapel_produktif as $produktif) {
			$nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $produktif->mata_pelajaran_id, $s->siswa_id);
			$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $produktif->mata_pelajaran_id, $s->siswa_id);
		?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo ucfirst(strtolower(get_nama_mapel($produktif->mata_pelajaran_id))); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$produktif->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$produktif->mata_pelajaran_id),$nilai_pengetahuan_value, 1); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$produktif->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$produktif->mata_pelajaran_id),$nilai_keterampilan_value, 1); ?></td>
            </tr>
            <?php
			$i++;
        } // endforeach mapelc
	}
	if($mapel_tambahan){
	?>
		<tr>
			<td colspan="10" class="strong"><b style="font-size: 13px;">Muatan Lokal</b></td>
		</tr>
	<?php
	$i=isset($i) ? $i : 1;
	foreach($mapel_tambahan as $tambahan){
		$nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $tambahan->mata_pelajaran_id, $s->siswa_id);
		$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $tambahan->mata_pelajaran_id, $s->siswa_id);
		?>
		<tr>
			<td align="center" valign="top"><?php echo $i; ?></td>
			<td valign="top"><?php echo get_nama_mapel($tambahan->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$tambahan->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$tambahan->mata_pelajaran_id),$nilai_pengetahuan_value); ?></td>
			<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$tambahan->mata_pelajaran_id); ?></td>
			<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
			<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$tambahan->mata_pelajaran_id),$nilai_keterampilan_value); ?></td>
		</tr>
	<?php
	$i++;
	}
	}
	?>
	</tbody>
</table>