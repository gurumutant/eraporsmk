<div class="strong text-center">KETERANGAN PINDAH SEKOLAH</div>
<br />
<p>Nama Peserta Didik : <?php echo get_nama_siswa($siswa_id); ?></p>
<table border="1" width="100%" class="table">
	<thead>
		<tr>
			<th colspan="4" class="text-center">KELUAR</th>
		</tr>
		<tr>
			<th width="15%" class="text-center">Tanggal</th>
			<th width="15%">Kelas yang Ditinggalkan</th>
			<th width="30%">Sebab-sebab Keluar atau Atas Permintaan (Tertulis)</th>
			<th width="40%">Tanda Tangan Kepala Sekolah, Stempel Sekolah, dan Tanda Tangan OrangTua/Wali</th>
		</tr>
	</thead>
	<tbody>
		<?php for($i=0;$i<=3;$i++){?>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td>
				______,_______________________________<br />Kepala Sekolah<br /><br /><br /><br />________________________________<br />
				NIP. <br /><br />
				OrangTua/Wali<br /><br /><br /><br />_______________________
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>