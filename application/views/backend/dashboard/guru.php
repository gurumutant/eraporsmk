<?php
$user = $this->ion_auth->user()->row();
$guru_id = ($user->guru_id) ? $user->guru_id : 0;
$ajaran = get_ta();
if(!$guru_id){
$data_guru	= $this->guru->find_by_user_id($user->user_id);
$guru_id = ($data_guru) ? $data_guru->guru_id : 0;
$this->user->update($user->user_id, array('guru_id' => $guru_id));
}
$cari_mapel = $this->pembelajaran->find_all("semester_id = $ajaran->id and guru_id = '$guru_id'", '*','rombongan_belajar_id asc');
if(!$cari_mapel){
	$cari_mapel = $this->pembelajaran->find_all("semester_id = $ajaran->id and guru_pengajar_id = '$guru_id'", '*','rombongan_belajar_id asc');
}
//$cari_mulok = Mulok::find_all_by_ajaran_id_and_guru_id($ajaran->id, $user->data_guru_id);
$cari_rombel = $this->rombongan_belajar->find("guru_id = '$guru_id' and semester_id= $ajaran->id");
$nama_group = get_jabatan($user->user_id);
?>
<h4 class="page-header">Mata Pelajaran yang diampu di Tahun Pelajaran <?php echo $ajaran->tahun; ?> Semester <?php echo $ajaran->semester; ?></h4>
<div class="row">
	<div class="col-lg-12 col-xs-12">
	<?php if($cari_mapel){ ?>
		<table class="table table-bordered table-striped table-hover datatable">
			<thead>
				<tr>
					<th rowspan="2"  style="width: 10px;vertical-align:middle;">#</th>
					<th rowspan="2" style="vertical-align:middle;">Mata Pelajaran</th>
					<th rowspan="2" style="vertical-align:middle;">Rombel</th>
					<th rowspan="2" style="vertical-align:middle;">Wali Kelas</th>
					<th rowspan="2" style="vertical-align:middle;" class="text-center">Jumlah PD</th>
					<th class="text-center" colspan="2">Generate Nilai</th>
				</tr>
				<tr>
					<th class="text-center">Pengetahuan</th>
					<th class="text-center">Keterampilan</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$i=1;
				foreach($cari_mapel as $m){
					$class_pengetahuan = 'btn-success btn_generate';
					$class_keterampilan = 'btn-success btn_generate';
					$status_pengetahuan = 'Proses';
					$status_keterampilan = 'Proses';
					$nilai_akhir_pengetahuan = $this->nilai_akhir->find("semester_id = $ajaran->id AND kompetensi_id = 1 AND rombongan_belajar_id = '$m->rombongan_belajar_id' AND mata_pelajaran_id = $m->mata_pelajaran_id");
						$nilai_akhir_keterampilan = $this->nilai_akhir->find("semester_id = $ajaran->id AND kompetensi_id = 2 AND rombongan_belajar_id = '$m->rombongan_belajar_id' AND mata_pelajaran_id = $m->mata_pelajaran_id");
					if($nilai_akhir_pengetahuan){
						$class_pengetahuan = 'btn-danger';
						$status_pengetahuan = 'Update';
					}
					if($nilai_akhir_keterampilan){
						$class_keterampilan = 'btn-danger';
						$status_keterampilan = 'Update';
					}
					$jumlah_penilaian_pengetahuan = $this->nilai->find_count("semester_id = $ajaran->id AND kompetensi_id = 1 AND rencana_penilaian_id IN (SELECT rencana_penilaian_id FROM rencana_penilaian WHERE semester_id = $ajaran->id AND rombongan_belajar_id = '$m->rombongan_belajar_id' AND mata_pelajaran_id = $m->mata_pelajaran_id AND kompetensi_id = 1)");
					//$this->rencana_penilaian->find_count("semester_id = $ajaran->id AND rombongan_belajar_id = $m->rombongan_belajar_id AND mata_pelajaran_id = $m->mata_pelajaran_id AND kompetensi_id = 1");
					$jumlah_penilaian_keterampilan = $this->nilai->find_count("semester_id = $ajaran->id AND kompetensi_id = 2 AND rencana_penilaian_id IN (SELECT rencana_penilaian_id FROM rencana_penilaian WHERE semester_id = $ajaran->id AND rombongan_belajar_id = '$m->rombongan_belajar_id' AND mata_pelajaran_id = $m->mata_pelajaran_id AND kompetensi_id = 2)");
					//$this->rencana_penilaian->find_count("semester_id = $ajaran->id AND rombongan_belajar_id = $m->rombongan_belajar_id AND mata_pelajaran_id = $m->mata_pelajaran_id AND kompetensi_id = 2");
					$link_proses_pengetahuan = '-';
					$link_proses_keterampilan = '-';
					if($jumlah_penilaian_pengetahuan){
						$link_proses_pengetahuan = '<a href="'.site_url('admin/generate_nilai/proses_guru/1/'.$m->rombongan_belajar_id.'/'.$m->mata_pelajaran_id.'/'.$m->pembelajaran_id).'" class="generate_nilai btn btn-sm '.$class_pengetahuan.' btn-sm"><i class="fa fa-check-square-o"></i> '.$status_pengetahuan.'</a>';
					}
					if($jumlah_penilaian_keterampilan){
						$link_proses_keterampilan = '<a href="'.site_url('admin/generate_nilai/proses_guru/2/'.$m->rombongan_belajar_id.'/'.$m->mata_pelajaran_id.'/'.$m->pembelajaran_id).'" class="generate_nilai btn btn-sm '.$class_keterampilan.' btn-sm"><i class="fa fa-check-square-o"></i> '.$status_keterampilan.'</a>';
					}
					$nama_rombel = get_nama_rombel($m->rombongan_belajar_id);
					if($nama_rombel == '-'){
						$this->pembelajaran->delete($m->pembelajaran_id);
					}
				?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo get_nama_mapel($m->mata_pelajaran_id).' ('.$m->mata_pelajaran_id.')'; ?></td>
					<td><?php echo $nama_rombel; ?></td>
					<td><?php echo get_wali_kelas($m->rombongan_belajar_id); ?></td>
					<td class="text-center"><?php echo get_jumlah_siswa($m->rombongan_belajar_id, get_nama_mapel($m->mata_pelajaran_id)); ?></td>
					<td><?php echo '<div class="text-center">'.$link_proses_pengetahuan.'</div>'; ?></td>
					<td><?php echo '<div class="text-center">'.$link_proses_keterampilan.'</div>'; ?></td>
				</tr>
				<?php $i++;
				 }
				 ?>
			</tbody>
		</table>
	<?php } else { ?>
			<table class="table table-bordered table-striped table-hover">
				<tr><td class="text-center">Anda tidak memiliki jadwal mengajar!</td></tr>
			</table>
		<?php } if($cari_rombel){
		$data['cari_rombel'] = $cari_rombel;
		$this->load->view('backend/dashboard/walas', $data);
		}
		if(in_array('waka',$nama_group)){
			$this->load->view('backend/dashboard/wakakur');
		}
		?>
	</div>
</div>