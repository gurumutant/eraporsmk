<?php
$nama_rombel = isset($cari_rombel->nama) ? $cari_rombel->nama : '-';
$id_rombel = isset($cari_rombel->rombongan_belajar_id) ? $cari_rombel->rombongan_belajar_id : 0;
$ajaran = get_ta();
$kunci_nilai = ($cari_rombel) ? $cari_rombel->kunci_nilai : 0;
$kunci_nilai = ($kunci_nilai) ? 0 : 1;
$tombol['class'] = ($kunci_nilai) ? 'danger' : 'success';
$tombol['text'] = ($kunci_nilai) ? 'Non Aktifkan' : 'Aktikan';
if($check_2018){
	$text_kkm = 'SKM';
} else {
	$text_kkm = 'SKM';
}
?>
<div class="row">
	<div class="col-lg-12 col-xs-12">
		<section id="mata-pelajaran">
			<h4>Anda adalah Wali Kelas Rombongan Belajar <label class="label bg-green"><?php echo $nama_rombel; ?></label></h4>
			<h5>Daftar Mata Pelajaran di Rombongan Belajar <label class="label bg-green"><?php echo $nama_rombel ?></label></h5>
			<p>Status Penilaian di Rombongan Belajar ini : <?php echo status_label($kunci_nilai); ?> <a class="btn btn-<?php echo $tombol['class']; ?> btn-sm" href="<?php echo site_url('admin/dashboard/kunci_nilai/'.$id_rombel.'/'.$kunci_nilai); ?>"><i class="fa fa-power-off"></i> <?php echo $tombol['text']; ?></a></p>
			<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
			<?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
			<div class="row">
				<a name="pembelajaran"></a>
				<div class="col-lg-12 col-xs-12" style="margin-bottom:20px;">
					<table class="table table-bordered table-striped table-hover datatable">
						<thead>
							<tr>
								<th style="width: 10px; vertical-align:middle;" class="text-center" rowspan="2">#</th>
								<th rowspan="2" style="vertical-align:middle;">Mata Pelajaran</th>
								<th rowspan="2" style="vertical-align:middle;">Guru Mata Pelajaran</th>
								<th class="text-center" rowspan="2" style="vertical-align:middle;"><?php echo $text_kkm; ?></th>
								<th class="text-center" colspan="2">Jumlah Rencana Penilaian</th>
								<th class="text-center" colspan="2">Generate Nilai</th>
							</tr>
							<tr>
								<th class="text-center">Pengetahuan</th>
								<th class="text-center">Keterampilan</th>
								<th class="text-center">Pengetahuan</th>
								<th class="text-center">Keterampilan</th>
							</tr>
						</thead>
						<tbody>
					<?php
					$all_mapel = $this->pembelajaran->find_all("semester_id = $ajaran->id AND rombongan_belajar_id = '$id_rombel'");
					if($all_mapel){
						$count = 1;
						foreach($all_mapel as $m){
						$class_pengetahuan = 'btn-success btn_generate';
						$class_keterampilan = 'btn-success btn_generate';
						$status_pengetahuan = 'Proses';
						$status_keterampilan = 'Proses';
						$nilai_akhir_pengetahuan = $this->nilai_akhir->find("semester_id = $ajaran->id AND kompetensi_id = 1 AND rombongan_belajar_id = '$id_rombel' AND mata_pelajaran_id = $m->mata_pelajaran_id");
						$nilai_akhir_keterampilan = $this->nilai_akhir->find("semester_id = $ajaran->id AND kompetensi_id = 2 AND rombongan_belajar_id = '$id_rombel' AND mata_pelajaran_id = $m->mata_pelajaran_id");
						if($nilai_akhir_pengetahuan){
							$class_pengetahuan = 'btn-danger';
							$status_pengetahuan = 'Update';
						}
						if($nilai_akhir_keterampilan){
							$class_keterampilan = 'btn-danger';
							$status_keterampilan = 'Update';
						}
						$jumlah_penilaian_pengetahuan = $this->nilai->find_count("semester_id = $ajaran->id AND kompetensi_id = 1 AND rencana_penilaian_id IN (SELECT rencana_penilaian_id FROM rencana_penilaian WHERE semester_id = $ajaran->id AND rombongan_belajar_id = '$m->rombongan_belajar_id' AND mata_pelajaran_id = $m->mata_pelajaran_id AND kompetensi_id = 1)");
					//$this->rencana_penilaian->find_count("semester_id = $ajaran->id AND rombongan_belajar_id = $m->rombongan_belajar_id AND mata_pelajaran_id = $m->mata_pelajaran_id AND kompetensi_id = 1");
					$jumlah_penilaian_keterampilan = $this->nilai->find_count("semester_id = $ajaran->id AND kompetensi_id = 2 AND rencana_penilaian_id IN (SELECT rencana_penilaian_id FROM rencana_penilaian WHERE semester_id = $ajaran->id AND rombongan_belajar_id = '$m->rombongan_belajar_id' AND mata_pelajaran_id = $m->mata_pelajaran_id AND kompetensi_id = 2)");
					//$this->rencana_penilaian->find_count("semester_id = $ajaran->id AND rombongan_belajar_id = $m->rombongan_belajar_id AND mata_pelajaran_id = $m->mata_pelajaran_id AND kompetensi_id = 2");
						$link_proses_pengetahuan = '-';
						$link_proses_keterampilan = '-';
						if($jumlah_penilaian_pengetahuan){
							$link_proses_pengetahuan = '<a href="'.site_url('admin/generate_nilai/proses_walas/1/'.$m->rombongan_belajar_id.'/'.$m->mata_pelajaran_id.'/'.$m->pembelajaran_id).'" class="generate_nilai btn btn-sm '.$class_pengetahuan.' btn-sm"><i class="fa fa-check-square-o"></i> '.$status_pengetahuan.'</a>';
						}
						if($jumlah_penilaian_keterampilan){
							$link_proses_keterampilan = '<a href="'.site_url('admin/generate_nilai/proses_walas/2/'.$m->rombongan_belajar_id.'/'.$m->mata_pelajaran_id.'/'.$m->pembelajaran_id).'" class="generate_nilai btn btn-sm '.$class_keterampilan.' btn-sm"><i class="fa fa-check-square-o"></i> '.$status_keterampilan.'</a>';
						}
						?>
							<tr>
								<td class="text-center"><?php echo $count; ?>.</td> 
								<td><?php echo get_nama_mapel($m->mata_pelajaran_id); ?></td>
								<td><?php echo get_guru_mapel($ajaran->id,$m->rombongan_belajar_id,$m->mata_pelajaran_id,'nama'); ?></td>
								<td class="text-center"><?php echo get_kkm($ajaran->id,$m->rombongan_belajar_id,$m->mata_pelajaran_id); ?></td>
								<td class="text-center"><?php echo get_jumlah_penilaian($ajaran->id,$m->rombongan_belajar_id,$m->mata_pelajaran_id,1); ?></td>
								<td class="text-center"><?php echo get_jumlah_penilaian($ajaran->id,$m->rombongan_belajar_id,$m->mata_pelajaran_id,2); ?></td>
								<td><?php echo '<div class="text-center">'.$link_proses_pengetahuan.'</div>'; ?></td>
								<td><?php echo '<div class="text-center">'.$link_proses_keterampilan.'</div>'; ?></td>
							</tr>
						<?php
							$count++;
						}
					}
					?>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>
</div>