<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-pencil"></i> Backup Data</a></li>
					<li><a href="#tab_2" data-toggle="tab"><i class="fa fa-upload"></i> Restore Data</a></li>
                    <!--li><a href="#tab_3" data-toggle="tab"><i class="fa fa-download"></i> Download XML</a></li-->
				</ul>
			<div class="tab-content">
				<div class="tab-pane active text-center" id="tab_1">
				<a class="btn btn-success btn-lg btn-block" href="<?php echo site_url('admin/backup'); ?>">Backup</a>
				</div>
				<div style="clear:both"></div>
				<div class="tab-pane" id="tab_2">
						<p class="text-center"><span class="btn btn-danger btn-file btn-lg btn-block">Browse  <input type="file" id="fileupload_import" name="import" /></span></p>
						<div id="progress" class="progress" style="display:none;">
							<div class="progress-bar progress-bar-success"></div>
						</div>
						<div id="result" class="callout callout-info lead" style="display:none"></div>
				</div>
                <div style="clear:both;"></div>
			</div>
		</div>	
	</div>
</div>
<script>
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'server/php/';
	var url_import = '<?php echo site_url('import/config');?>';
    $('#fileupload_import').fileupload({
        url: url_import,
        //dataType: 'json',
		dataType: 'html',
        done: function (e, data) {
			$('#progress').hide();
			$('#result').show();
			$('#result').html(data.result);
            //swal({title:data.result.title,type:data.result.type,html:data.result.text}).then(function() {
				//window.location.replace('<?php echo site_url('admin/auth/logout'); ?>');
			//}).done();
        },
        progressall: function (e, data) {
			$('#progress').show();
			console.log(data);
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
/*$(function() {
var url_import = '<?php echo site_url('import/config');?>';
	//console.log(url_import);
	$('#fileupload_import').fileupload({
        url: url_import,
		dataType: 'json',
	}).on('fileuploadprogress', function (e, data) {
		console.log('atas');
		console.log(data);
		var progress = parseInt(data.loaded / data.total * 100, 10);
		$('#progress .progress-bar').css('width',progress + '%');
	}).on('fileuploadsubmit', function (e, data) {
		console.log('tengah');
		console.log(data);
		$('#progress').show();
	}).done(function (e, data) {
		console.log('bawah');
		console.log(data);
		console.log(e);
        //$.each(data.result.files, function (index, file) {
            //$('<p/>').text(file.name).appendTo('#files');
        //});
	/*.on('fileuploaddone', function (e, data) {
		$('#result').show();	
		$('#progress').hide();
		$('#progress .progress-bar').css('width','0%');
		console.log('bawah');
		console.log(data);
		console.log(e);
		window.setTimeout(function() { 
			$('#progress').show();
			$('#progress .progress-bar').removeClass('progress-bar-success').addClass('progress-bar-yellow');
			$('#progress').addClass('active');
			$('#progress .progress-bar').addClass('progress-bar-striped');
			$('#progress .progress-bar').css('width',data.result.persen+'%');
		}, 1000);
		$('#result').html(data);
		//DoAjaxProgressImport(data.result,data.result.jumlah,data.result.persen);
	}).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
/*var index = 0;
var set_insert = 0;
function DoAjaxProgressImport(data,length,persen) {
	$('#result').html('Proses restore akan membutuhkan waktu lama. Dari total <b>'+length+'</b> berkas, <b>'+set_insert+'</b> berkas berhasil di proses');
	$.ajax({
		url: '<?php echo site_url('import/config/proses'); ?>',
		type: 'post',
		data: {total:length, parameter:JSON.stringify(data.result[index])},
		success: function(response){
			var result = $.parseJSON(response);
			$('#progress').show();
			$('#progress .progress-bar').css('width',result.persen+'%');
			$('#result').html('Proses restore akan membutuhkan waktu lama. Dari total <b>'+result.total+'</b> berkas, <b>'+result.parameter+'</b> berkas berhasil di proses');
			set_insert = result.parameter;
			if(length == result.parameter){
				swal({title:result.title,type:result.type,html:result.text}).then(function() {
					window.location.replace('<?php echo site_url('admin/config/backup'); ?>');
				}).done();
				return false;
			}
			DoAjaxProgressImport(data,length);
		}
	});
	index++;
}
function DoAjaxProgressCallImport(data,length){
	setInterval( function() {
		DoAjaxProgressImport(data,length);
	}, 300 );
}*/
</script>