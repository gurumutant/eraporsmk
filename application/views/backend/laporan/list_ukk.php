<?php
$ajaran = get_ta();
$data_rombel = $this->rombongan_belajar->find("guru_id = '$guru_id' AND semester_id = $ajaran->id");
$data_siswa = get_siswa_by_rombel($data_rombel->rombongan_belajar_id);
?>
<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
<?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<div class="box box-info">
    <div class="box-body">
		<div class="col-md-6">
			<a class="btn btn-success btn-lg btn-block" href="<?php echo site_url('admin/get_excel/nilai_ukk/'.$data_rombel->rombongan_belajar_id); ?>">Download Template Nilai</a>
		</div>
		<div class="col-md-6">
			<p class="text-center"><span class="btn btn-danger btn-file btn-lg btn-block"> Import Template Nilai<input type="file" id="fileupload" name="import" /></span></p>
		</div>
		<div class="clearfix"></div>
		<!-- form start -->
            <?php 
			$attributes = array('class' => 'form-horizontal', 'id' => 'myform');
			echo form_open($form_action,$attributes);
			?>
			<input type="hidden" name="ajaran_id" value="<?php echo $ajaran->id; ?>" />
			<input type="hidden" name="rombel_id" value="<?php echo $data_rombel->rombongan_belajar_id; ?>" />
			<div class="table-responsive no-padding">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th width="50%">Nama Peserta Didik</th>
							<th width="25%">Nilai Teori Kejuruan</th>
							<th width="25%">Nilai Praktik Kejuruan</th>
						</tr>
					</thead>
					<tbody>
					<?php
					if($data_siswa){
						$style = '';
						foreach($data_siswa['data'] as $siswa){
						$siswa_id = $siswa->siswa_id;
					?>
					<?php
					$nilai_ukk = $this->nilai_ukk->find("semester_id = $ajaran->id AND rombongan_belajar_id = '$data_rombel->rombongan_belajar_id' AND siswa_id = '$siswa_id'");
					$nilai_teori = '';
					$nilai_praktik = '';
					if($nilai_ukk){
						$nilai_teori = $nilai_ukk->nilai_teori;
						$nilai_praktik = $nilai_ukk->nilai_praktik;
					}
					?>
					<input class="set_nilai" type="hidden" name="siswa_id[]" value="<?php echo $siswa->siswa_id; ?>" /> 
					<tr>
						<td>
							<?php echo strtoupper($siswa->nama); ?>
							<?php //echo $siswa->nisn.'<br />'; ?>
							<?php //$date = date_create($siswa->tanggal_lahir);
							//echo date_format($date,'d/m/Y'); ?>
						</td>
						<td>
							<input type="text" class="form-control" name="nilai_teori[]" value="<?php echo $nilai_teori; ?>" /> 
						</td>
						<td>
							<input type="text" class="form-control" name="nilai_praktik[]" value="<?php echo $nilai_praktik; ?>" /> 
						</td>
					</tr>
				<?php } } else { 
						$style = ' style="display:none;"';
				?>
					<tr>
						<td colspan="2">Belum ada anggota rombel di kelas <?php echo get_nama_rombel($data_rombel->id); ?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
              <!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-success"<?php echo $style; ?>>Simpan</button>
			</div>
            <?php echo form_close();  ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>