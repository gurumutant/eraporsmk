<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysiwyg/external/jquery.hotkeys.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysiwyg/external/google-code-prettify/prettify.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysiwyg/bootstrap-wysiwyg.js"></script>
<link href="<?php echo base_url(); ?>assets/css/editor.css" rel="stylesheet" type="text/css" />
<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
<?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<?php echo (isset($message) && $message != '') ? error_msg($message) : '';?>
<div class="box box-info">
    <div class="box-body">
		<!-- form start -->
            <?php
			$loggeduser = $this->ion_auth->user()->row();
			$attributes = array('class' => 'form-horizontal', 'id' => 'myform');
			echo form_open_multipart(uri_string(),$attributes);
			$ajaran = get_ta();
			$tahun_ajaran = $ajaran->tahun. ' (SMT '. $ajaran->semester.')';
			$data_rombel = $this->rombongan_belajar->find("guru_id = '$loggeduser->guru_id' AND semester_id = $ajaran->id");
			$rombongan_belajar_id = ($data_rombel) ? $data_rombel->rombongan_belajar_id : gen_uuid();
			$data_siswa = get_siswa_by_rombel($rombongan_belajar_id);
			//$all_ppk = $this->ref_ppk->get_all();
			$extra = 'class="select2 form-control required" id="siswa_ppk"';
			?>
              <div class="box-body">
			  	<div class="col-sm-12">
                <div class="form-group">
                  <label for="ajaran_id" class="col-sm-3 control-label">Tahun Ajaran</label>
				  <div class="col-sm-9">
				  	<input type="hidden" name="query" id="query" value="ppk" />
                    <input type="hidden" id="semester_id" name="semester_id" value="<?php echo $ajaran->id; ?>" />
                    <input type="text" class="form-control" value="<?php echo $tahun_ajaran; ?>" readonly />
                  </div>
                </div>
				<div class="form-group">
                  <label for="siswa" class="col-sm-3 control-label">Peserta Didik</label>
				  <div class="col-sm-9">
				  <?php
					$all_siswa = array();
					$all_siswa[''] = '== Pilih Peserta Didik ==';
					foreach($data_siswa['data'] as $data){
						$all_siswa[$data->siswa_id] = strtoupper($data->nama);
					}
					echo form_dropdown('siswa_ppk', $all_siswa, $siswa, $extra);
					?>
                  </div>
                </div>
				<?php
				$all_karakter[] = (object) array('sikap_id' => 19, 'butir_sikap' => 'Integritas');
				$all_karakter[] = (object) array('sikap_id' => 1, 'butir_sikap' => 'Religius');
				$all_karakter[] = (object) array('sikap_id' => 20, 'butir_sikap' => 'Nasionalis');
				$all_karakter[] = (object) array('sikap_id' => 7, 'butir_sikap' => 'Mandiri');
				$all_karakter[] = (object) array('sikap_id' => 21, 'butir_sikap' => 'Gotong-royong');
				foreach($all_karakter as $aspek_karakter){
					$find_indikator = $this->indikator_karakter->find_all("sikap_id = $aspek_karakter->sikap_id");
					if(!$find_indikator){
						insert_indikator($aspek_karakter->sikap_id);
					}
				?>
				<div class="form-group">
					<label for="capaian" class="col-sm-3 control-label">Catatan Penilaian Sikap <b><i><span id="nama_siswa"></span></i></b></label>
					<div class="col-sm-9" id="sugesti_<?php echo $aspek_karakter->sikap_id; ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="sikap_id_<?php echo $aspek_karakter->sikap_id; ?>" class="col-sm-3 control-label"><?php echo $aspek_karakter->butir_sikap; ?>
						<?php if($find_indikator){?>
						<ul style="font-weight:normal;">
							<?php foreach($find_indikator as $indikator){?>
							<li style="font-weight:normal;"><?php echo $indikator->nama; ?></li>
							<?php } ?>
						</ul>
						<?php } ?>
					</label>
					<div class="col-sm-9">
						<input type="hidden" id="sikap_id" name="sikap_id[<?php echo $aspek_karakter->sikap_id; ?>]" value="<?php echo $aspek_karakter->sikap_id; ?>" />
						<textarea id="sikap_id_<?php echo $aspek_karakter->sikap_id; ?>" name="deskripsi[<?php echo $aspek_karakter->sikap_id; ?>]" class="editor" style="width: 100%; height: 100%; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $deskripsi[$aspek_karakter->sikap_id]['value']; ?></textarea>
					</div>
				</div>
				<?php } ?>
				<div class="form-group">
					<label for="capaian" class="col-sm-3 control-label">Catatan Perkembangan Karakter</label>
					<div class="col-sm-9">
						<textarea name="capaian" id="capaian" class="editor" style="width: 100%; height: 100%; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $capaian; ?></textarea>
					</div>
				</div>
				<?php /*for($i=1; $i<=3; $i++){?>
				<div class="form-group">
					<label for="kegiatan" class="col-sm-3 control-label">Foto <?php echo $i; ?></label>
					<div class="col-sm-9">
						<input type="file" name="foto_<?php echo $i; ?>" />
					</div>
				</div>
				<?php } */?>
			</div>
		</div>
		<div class="box-footer">
			<button type="submit" class="btn btn-success simpan">Simpan</button>
		</div>
		<?php echo form_close();  ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>
<script>
/*$('#myform').submit(function(){
	//e.preventDefault();
	var capaian = $('#editor').html();
	$('#copy_editor').html(capaian);
	$.ajax({
		//url: '<?php echo site_url('admin/asesmen/add_ppk'); ?>',
		url: '<?php echo site_url('admin/asesmen/get_ppk'); ?>',
		type: 'post',
		data: $("#myform").serialize(),
		success: function(response){
			console.log(response);
		}
	});
});*/
var checkJSONSingle = function(m) {
	if (typeof m == 'object') { 
		try{ m = JSON.stringify(m); 
		} catch(err) { 
			return false; 
		}
	}
	if (typeof m == 'string') {
		try{ m = JSON.parse(m); 
		} catch (err) {
			return false;
		}
	}
	if (typeof m != 'object') { 
		return false;
	}
	return true;
};
$('#siswa_ppk').change(function(){
	var query = $('#query').val();
	var ini = $(this).val();
	if(ini == ''){
		return false;
	}
	$.ajax({
		url: '<?php echo site_url('admin/asesmen/get_ppk'); ?>',
		type: 'post',
		data: $("#myform").serialize(),
		success: function(response){
			var result = checkJSONSingle(response);
			if(result == true){
				var data = $.parseJSON(response);
				console.log(data);
				$('#nama_siswa').html(data.nama_siswa);
				$('#sugesti_1').html(data.sugesti_1);
				$('#sugesti_7').html(data.sugesti_7);
				$('#sugesti_19').html(data.sugesti_19);
				$('#sugesti_20').html(data.sugesti_20);
				$('#sugesti_21').html(data.sugesti_21);
				var editorObjSikap_id_1 = $("#sikap_id_1").data('wysihtml5');
				var editorSikap_id_1 = editorObjSikap_id_1.editor;
				editorSikap_id_1.setValue(data.sikap_id_1);
				var editorObjSikap_id_7 = $("#sikap_id_7").data('wysihtml5');
				var editorSikap_id_7 = editorObjSikap_id_7.editor;
				editorSikap_id_7.setValue(data.sikap_id_7);
				var editorObjSikap_id_19 = $("#sikap_id_19").data('wysihtml5');
				var editorSikap_id_19 = editorObjSikap_id_19.editor;
				editorSikap_id_19.setValue(data.sikap_id_19);
				var editorObjSikap_id_20 = $("#sikap_id_20").data('wysihtml5');
				var editorSikap_id_20 = editorObjSikap_id_20.editor;
				editorSikap_id_20.setValue(data.sikap_id_20);
				var editorObjSikap_id_21 = $("#sikap_id_21").data('wysihtml5');
				var editorSikap_id_21 = editorObjSikap_id_21.editor;
				editorSikap_id_21.setValue(data.sikap_id_21);
				var editorObjCapaian = $("#capaian").data('wysihtml5');
				var editorCapaian = editorObjCapaian.editor;
				editorCapaian.setValue(data.capaian);
			} else {
				$('#capaian').val(response);
			}
		}
	});
});
</script>