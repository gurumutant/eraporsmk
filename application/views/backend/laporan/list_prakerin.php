<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
<?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<div class="box box-info">
    <div class="box-body">
		<div class="row" style="margin-bottom:10px;">
		<?php 
			$tingkat_pendidikan = $this->tingkat_pendidikan->find_all("tingkat_pendidikan_id IN (10,11,12,13)");
			$get_jurusan = $this->jurusan_sp->find_all("sekolah_id = '$sekolah_id'");
		?>
			<div class="col-md-4">
				<select id="filter_jurusan" class="form-control">
					<option value="">==Filter Berdasar Kompetensi Keahlian==</option>
					<?php foreach($get_jurusan as $jurusan){ ?>
					<option value="<?php echo $jurusan->jurusan_id; ?>"><?php echo get_jurusan($jurusan->jurusan_id); ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-4">
				<select id="filter_tingkat" class="form-control" style="display:none;">
					<option value="">==Filter Berdasar Tingkat==</option>
					<?php foreach($tingkat_pendidikan as $tingkat){ ?>
					<option value="<?php echo $tingkat->tingkat_pendidikan_id; ?>"><?php echo $tingkat->nama; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-4">
				<select id="filter_rombel" class="form-control" style="display:none;"></select>
			</div>
		</div>
        <table id="datatable" class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th width="5%">Kelas</th>
                <th width="10%">Nama Peserta Didik</th>
                <th width="10%">Mitra</th>
                <th width="10%">Lokasi</th>
                <th width="5%">Lama</th>
                <th width="25%">Keterangan</th>
                <th style="width: 5%" class="text-center">Action</th>
            </tr>
            </thead>
			<tbody>
			</tbody>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>