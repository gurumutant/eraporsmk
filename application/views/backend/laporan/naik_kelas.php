<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
<?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<div class="box box-info">
    <div class="box-body">
<?php
$attributes = array('class' => 'form-horizontal', 'id' => 'myform');
echo form_open($form_action,$attributes);
?>
<input type="hidden" name="query" value="kenaikan" />
<input type="hidden" name="kelas" value="<?php echo $rombel->tingkat + 1; ?>" />
<input type="hidden" name="ajaran_id" value="<?php echo $ajaran->id; ?>" />
<input type="hidden" name="rombel_id" value="<?php echo $rombel->rombongan_belajar_id; ?>" />
<div class="table-responsive no-padding">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="10px" class="text-center">No</th>
				<th width="50%">Nama Peserta Didik</th>
				<th width="25%">Status Kenaikan</th>
				<th width="25%">Ke Kelas</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			if($data_siswa){
				$i=1;
				foreach($data_siswa as $siswa){
				//test($siswa);
				//die();
				$siswa_id = $siswa->siswa_id;
				$kenaikan = $this->kenaikan_kelas->find("semester_id = $ajaran->id AND anggota_rombel_id = '$siswa->anggota_rombel_id'");
			?>
			<tr>
				<td class="text-center"><?php echo $i; ?></td>
				<td>
					<input type="hidden" name="anggota_rombel_id[]" value="<?php echo $siswa->anggota_rombel_id; ?>" />
					<?php echo get_nama_siswa($siswa_id); ?>
				</td>
				<td>
					<select name="status[]" id="status" class="form-control">
						<option value="">== Pilih Status Kenaikan==</option>
						<option value="1"<?php echo ($kenaikan) && ($kenaikan->status == 1) ? ' selected="selected"' : ''; ?>>Naik Ke Kelas</option>
						<option value="2"<?php echo ($kenaikan) && ($kenaikan->status == 2) ? ' selected="selected"' : ''; ?>>Tidak Naik</option>
					</select>
				</td>
				<td>
					<select name="ke_kelas[]" id="ke_kelas" class="form-control select2">
						<option value="">== Pilih Rombongan Belajar ==</option>
						<?php if($kenaikan && $kenaikan->ke_kelas){ ?>
							<option value="<?php echo $kenaikan->ke_kelas; ?>" selected="selected"><?php echo get_nama_rombel($kenaikan->ke_kelas); ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<?php
				$i++;
				}
			} else {
			?>
			<tr>
				<td colspan="4" class="text-center">Tidak ada data peserta didik di rombongan belajar terpilih</td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-success">Simpan</button>
			</div>
            <?php echo form_close();  ?>
</div>
</div>
</div>
<script>
$('select#status').change(function(e) {
	e.preventDefault();
	var ini = $(this).val();
	var next_td = $(this).closest('td').next('td').find('select');
	if(ini == 2){
		$(next_td).html('<option value="">== Pilih Rombongan Belajar ==</option>');
	} else {
		$(next_td).trigger('change.select2');
		$.ajax({
			url: '<?php echo site_url('admin/ajax/get_rombel'); ?>',
			type: 'post',
			data: $("form").serialize(),
			success: function(response){
				var data = $.parseJSON(response);
				console.log(data);
				$(next_td).html('<option value="">== Pilih Rombongan Belajar ==</option>');
				if($.isEmptyObject(data.result)){
				} else {
					$.each(data.result, function (i, item) {
						$(next_td).append($('<option>', { 
							value: item.value,
							text : item.text
						}));
					});
				}
			}
		});
	}
});
</script>