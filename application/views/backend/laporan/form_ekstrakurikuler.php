<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
<?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<div class="box box-info">
    <div class="box-body">
		<!-- form start -->
            <?php 
			$attributes = array('class' => 'form-horizontal', 'id' => 'myform');
			echo form_open($form_action,$attributes);
			$ajaran = get_ta();
			$loggeduser = $this->ion_auth->user()->row();
			$find_akses = get_akses($loggeduser->id);
			$guru_id = $find_akses['id'][0];
			//$rombel = $this->rombongan_belajar->find("guru_id = '$guru_id' AND semester_id = $ajaran->id");
			//$all_ekskul = Ekskul::find_all_by_ajaran_id($ajaran->id);
			//$all_ekskul = $this->ekstrakurikuler->find_all("guru_id = '$guru_id'");
			$tingkat_pendidikan = $this->tingkat_pendidikan->find_all("tingkat_pendidikan_id IN (10,11,12,13)");
			//$this->ekstrakurikuler->get_all();
			//Ekskul::all();
			?>
			<div class="box-body">
				<input type="hidden" name="query" id="query" value="form_ekstrakurikuler" />
				<input type="hidden" name="ajaran_id" value="<?php echo $ajaran->id; ?>" />
				<div class="form-group">
                  <label for="kelas" class="col-sm-2 control-label">Kelas</label>
				  <div class="col-sm-5">
                    <select name="kelas" class="select2 form-control" id="kelas">
						<option value="">== Pilih Tingkat Kelas ==</option>
						<?php foreach($tingkat_pendidikan as $tingkat){?>
						<option value="<?php echo $tingkat->tingkat_pendidikan_id; ?>"><?php echo $tingkat->nama; ?></option>
						<?php } ?>
					</select>
                  </div>
                </div>
				<div class="form-group">
                  <label for="rombel" class="col-sm-2 control-label">Rombongan Belajar</label>
				  <div class="col-sm-5">
                    <select name="rombel_id" class="select2 form-control" id="rombel">
						<option value="">== Pilih Rombongan Belajar ==</option>
					</select>
                  </div>
                </div>
				<div class="form-group">
                  <label for="rombel" class="col-sm-2 control-label">Ekstrakurikuler</label>
				  <div class="col-sm-5">
                    <select name="ekskul_id" class="select2 form-control" id="ekskul">
						<option value="">== Pilih Nama Ekstrakurikuler ==</option>
						<?php /*foreach($all_ekskul as $ekskul){ ?>
						<option value="<?php echo $ekskul->ekstrakurikuler_id; ?>"><?php echo $ekskul->nama_ekskul; ?> (<?php echo get_nama_guru($ekskul->guru_id); ?>)</option>
						<?php } */?>
					</select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
			<div class="box-footer table-responsive no-padding">
				<div id="result"></div>
				<button type="submit" class="btn btn-success simpan" style="display:none;">Simpan</button>
			</div>
            <?php echo form_close();  ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>