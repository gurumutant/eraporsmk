<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="row">
<!-- left column -->
<div class="col-md-12">
<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title"><?php echo $page_title.' ('.$total_rows.' => '.$inserted. ')'; ?></h3>
	</div>
    <div class="box-body">
	<div class="text-center">
		<?php //echo $pagination; ?>
	</div>
	<table class="table table-bordered table-striped table-hover">
            <thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Nama Ekskul</th>
					<th class="text-center">Nama Pembina</th>
					<th class="text-center">Prasarana</th>
					<th class="text-center">status</th>
	            </tr>
            </thead>
			<tbody>
			<?php
				$no = $this->uri->segment('4') + 1;
				foreach($dapodik as $data){
					$get_guru_id = $this->guru->find_by_guru_id_dapodik($data->ptk_id);
					$guru_id = ($get_guru_id) ? $get_guru_id->guru_id : $data->ptk_id;
					$nm_ekskul = strtolower($data->nm_ekskul);
					$find_kelas_ekskul = $this->ekstrakurikuler->find("id_kelas_ekskul = '$data->ID_kelas_ekskul' OR lower(nama_ekskul) = '$nm_ekskul'");
					$data_ekskul = array(
						'semester_id' => $ajaran->id,
						'guru_id' => $guru_id,
						'nama_ekskul' => $data->nm_ekskul,
						'is_dapodik' => 1, 
						'id_kelas_ekskul' => $data->ID_kelas_ekskul, 
						'alamat_ekskul' => $data->nama_prasarana, 
					);
					if($find_kelas_ekskul){
						$result = 'update';
						$class = 'table-danger"';
						//test($update_ekskul);
						$this->ekstrakurikuler->update($find_kelas_ekskul->ekstrakurikuler_id, $data_ekskul);
					} else {
						$result = 'insert';
						$class = 'table-success"';
						$data_ekskul['ekstrakurikuler_id'] = gen_uuid();
						$this->ekstrakurikuler->insert($data_ekskul);
					}
			?>
				<tr class="<?php echo $class; ?>">
					<td class="text-center"><?php echo $no++; ?></td>
					<td><?php echo $data->nm_ekskul; ?></td>
					<td class="text-center"><?php echo $data->nama_pembina; ?></td>
					<td class="text-center"><?php echo $data->nama_prasarana; ?></td>
					<td class="text-center"><?php echo $result; ?></td>
				</tr>
			<?php
			//}
			//break; 
			} ?>
			</tbody>
		</table>
    </div><!-- /.box-body -->
	<div class="box-footer text-center">
		<?php echo $pagination; ?>
	</div>
</div><!-- /.box -->
</div>
<script>
$(document).ready(function(){
	$('body').mouseover(function(){
		$(this).css({cursor: 'wait'});
	});
	var cari = $('body').find('.next');
	if(cari.length>0){
		var cari_a = $(cari).find('a');
		var url = $(cari_a).attr('href');
		window.location.replace(url);
	} else {
		window.location.replace('<?php echo site_url('admin/sinkronisasi'); ?>');
	}
})
</script>