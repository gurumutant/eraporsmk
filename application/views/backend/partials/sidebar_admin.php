<?php
$settings 	= $this->settings->get(1);
$user = $this->ion_auth->user()->row();
$uri = $this->uri->segment_array();
$semester = get_ta();
?>
<aside class="main-sidebar">              
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php $img = ($user->photo!= '')  ? site_url(PROFILEPHOTOSTHUMBS.$user->photo) : site_url('assets/img/avatar3.png'); ?>
                <img src="<?php echo $img;?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Selamat Datang<br /><?php echo $user->username; ?></p>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
			<li class="header">Periode Aktif : <?php echo $semester->tahun.' SMT '.$semester->semester;?></li>
			<?php //$get_menu = get_menu(); echo $get_menu; ?>
            <li class="treeview <?php echo (isset($activemenu) && $activemenu == 'dashboard') ?  'active' : ''; ?>">
                <a href="<?php echo site_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i> <span>Beranda</span>
                </a>
            </li>
			<li class="treeview <?php echo (isset($activemenu) && $activemenu == 'sinkronisasi') ?  'active' : ''; ?>">
                <a href="#">
                    <i class="fa fa-refresh"></i> <span>Sinkronisasi</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
                </a>
                <ul class="treeview-menu">
					<li<?php echo (isset($activemenu) && $activemenu == 'sinkronisasi' && 
					!isset($uri[3])
					||
					isset($uri[3]) && $uri[3] == 'guru' 
					|| 
					isset($uri[3]) && $uri[3] == 'rombongan_belajar' 
					|| 
					isset($uri[3]) && $uri[3] == 'siswa'
					|| 
					isset($uri[3]) && $uri[3] == 'pembelajaran'
					) ?  ' class="active"' : ''; ?>>
						<a href="<?php echo site_url('admin/sinkronisasi'); ?>">
                	    <i class="fa fa-download"></i> <span>Ambil Data</span><span class="pull-right-container"><small class="label pull-right bg-green">Online</small></span>
		                </a>
        		    </li>
					<li<?php echo (isset($activemenu) && $activemenu == 'sinkronisasi' && 
					isset($uri[3]) && $uri[3] == 'sync' 
					) ?  ' class="active"' : ''; ?>>
						<a href="<?php echo site_url('admin/sinkronisasi/sync'); ?>">
                	    <i class="fa fa-upload"></i> <span>Kirim Data</span><span class="pull-right-container"><small class="label pull-right bg-green">Online</small>
		                </a>
        		    </li>
					<!--li<?php echo (isset($activemenu) && $activemenu == 'sinkronisasi' && 
					isset($uri[3]) && $uri[3] == 'referensi' 
					) ?  ' class="active"' : ''; ?>>
						<a href="<?php echo site_url('admin/sinkronisasi/referensi'); ?>">
                	    <i class="fa fa-download"></i> <span>Referensi Dapodik</span><span class="pull-right-container"><small class="label pull-right bg-green">Online</small>
		                </a>
        		    </li-->
					<!--li<?php echo (isset($activemenu) && $activemenu == 'sinkronisasi' && isset($uri[2]) && $uri[2] == 'sinkronisasi_offline' 
					||
					isset($uri[3]) && $uri[3] == 'guru' 
					|| 
					isset($uri[3]) && $uri[3] == 'rombongan_belajar' 
					|| 
					isset($uri[3]) && $uri[3] == 'siswa'
					|| 
					isset($uri[3]) && $uri[3] == 'pembelajaran'
					) ?  ' class="active"' : ''; ?>>
						<a href="<?php echo site_url('admin/sinkronisasi_offline'); ?>">
                	    <i class="fa fa-download"></i> <span>Ambil Data</span><span class="pull-right-container"><small class="label pull-right bg-red">Offline</small></span>
		                </a>
        		    </li-->
					<?php
					$site_url = parse_url(site_url());
					if($site_url['host'] == 'localhost'){?>
					<li<?php echo (isset($activemenu) && $activemenu == 'sinkronisasi' && 
					isset($uri[3]) && $uri[3] == 'nilai' 
					) ?  ' class="active"' : ''; ?>>
						<a href="<?php echo site_url('admin/sinkronisasi/nilai'); ?>">
                	    <i class="fa fa-upload"></i> <span>Kirim Nilai</span><span class="pull-right-container"><small class="label pull-right bg-red">Offline</small>
		                </a>
        		    </li>
					<?php } ?>
				</ul>
			</li>
			<li class="treeview <?php echo (isset($activemenu) && $activemenu == 'config') ?  'active' : ''; ?>">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span>Konfigurasi</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
                </a>
                <ul class="treeview-menu">
					<li<?php echo (isset($activemenu) && $activemenu == 'config' && isset($uri[3]) && $uri[3] == 'general') ?  ' class="active"' : ''; ?>>
						<a href="<?php echo site_url('admin/config/general'); ?>">
                	    <i class="fa fa-exchange"></i> <span>Konfigurasi Umum</span>
		                </a>
        		    </li>
					<!--li<?php echo (isset($activemenu) && $activemenu == 'config' && isset($uri[3]) && $uri[3] == 'backup') ?  ' class="active"' : ''; ?>>
        		        <a href="<?php echo site_url('admin/config/backup'); ?>">
                	    <i class="fa fa-exchange"></i> <span>Backup &amp; Restore</span>
		                </a>
        		    </li-->
					<li<?php echo (isset($activemenu) && $activemenu == 'config' && isset($uri[3]) && $uri[3] == 'users' || 
					isset($uri[3]) && $uri[3] == 'edit') ?  ' class="active"' : ''; ?>>
        		        <a href="<?php echo site_url('admin/config/users'); ?>">
                	    <i class="fa fa-user"></i> <span>Hak Akses Pengguna</span>
		                </a>
        		    </li>
				</ul>
			</li>
			<li class="treeview <?php echo (isset($activemenu) && $activemenu == 'referensi') ?  'active' : ''; ?>">
                <a href="#">
					<i class="fa fa-list"></i> 
					<span>Referensi</span>
					<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
					<li class="treeview <?php echo (isset($activemenu) && $activemenu == 'referensi' && isset($uri[2]) && $uri[2] == 'data_guru') ?  'active' : ''; ?>">
						<a href="#">
							<i class="fa fa-hand-o-right"></i> 
							<span>Referensi GTK</span>
							<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
						</a>
						<ul class="treeview-menu">
							<li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
							isset($uri[2]) && $uri[2] == 'data_guru'
							&& 
							!isset($uri[3])
							) ?  ' class="active"' : ''; ?>>
								<a href="<?php echo site_url('admin/data_guru'); ?>">
								<i class="fa fa-graduation-cap"></i> <span>Referensi Guru</span>
								</a>
							</li>
							<li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
							isset($uri[2]) && $uri[2] == 'data_guru'
							&& 
							isset($uri[3]) && $uri[3] == 'tendik'
							) ?  ' class="active"' : ''; ?>>
								<a href="<?php echo site_url('admin/data_guru/tendik'); ?>">
								<i class="fa fa-graduation-cap"></i> <span>Referensi Tendik</span>
								</a>
							</li>
							<li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
							isset($uri[2]) && $uri[2] == 'data_guru'
							&& 
							isset($uri[3]) && $uri[3] == 'instruktur'
							|| 
							isset($uri[3]) && $uri[3] == 'edit'
							||
							isset($uri[3]) && $uri[3] == 'tambah_instruktur'
							) ?  ' class="active"' : ''; ?>>
								<a href="<?php echo site_url('admin/data_guru/instruktur'); ?>">
								<i class="fa fa-graduation-cap"></i> <span>Referensi Instruktur</span>
								</a>
							</li>
						</ul>
					</li>
					<!--li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
					isset($uri[2]) && $uri[2] == 'data_guru'
					|| 
					isset($uri[3]) && $uri[3] == 'tambah_guru'
					) ?  ' class="active"' : ''; ?>>
        		        <a href="<?php echo site_url('admin/data_guru'); ?>">
                	    <i class="fa fa-hand-o-right"></i> <span>Referensi Guru</span>
		                </a>
        		    </li-->
					<li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
					isset($uri[2]) && $uri[2] == 'rombel'
					|| 
					isset($uri[3]) && $uri[3] == 'tambah_rombel'
					) ?  ' class="active"' : ''; ?>>
        		        <a href="<?php echo site_url('admin/rombel'); ?>">
                	    <i class="fa fa-hand-o-right"></i> <span>Referensi Rombel</span>
		                </a>
        		    </li>
					<!--li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
					isset($uri[2]) && $uri[2] == 'data_siswa'
					|| 
					isset($uri[3]) && $uri[3] == 'tambah_siswa'
					) ?  ' class="active"' : ''; ?>>
        		        <a href="<?php echo site_url('admin/data_siswa'); ?>">
                	    <i class="fa fa-hand-o-right"></i> <span>Referensi Peserta Didik</span>
		                </a>
        		    </li-->
					<li class="treeview <?php echo (isset($activemenu) && $activemenu == 'referensi' && isset($uri[2]) && $uri[2] == 'data_siswa') ?  'active' : ''; ?>">
						<a href="#">
							<i class="fa fa-hand-o-right"></i> 
							<span>Referensi Peserta Didik</span>
							<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
						</a>
						<ul class="treeview-menu">
							<li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
							isset($uri[2]) && $uri[2] == 'data_siswa'
							&& 
							!isset($uri[3])
							) ?  ' class="active"' : ''; ?>>
								<a href="<?php echo site_url('admin/data_siswa'); ?>">
								<i class="fa fa-users"></i> <span>Peserta Didik Aktif</span>
								</a>
							</li>
							<li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
							isset($uri[2]) && $uri[2] == 'data_siswa'
							&& 
							isset($uri[3]) && $uri[3] == 'non_aktif'
							) ?  ' class="active"' : ''; ?>>
								<a href="<?php echo site_url('admin/data_siswa/non_aktif'); ?>">
								<i class="fa fa-users"></i> <span><span class="text-red"><strong>Peserta Didik Keluar</strong></span></span>
								</a>
							</li>
						</ul>
					</li>
					<li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
					isset($uri[3]) && $uri[3] == 'mata_pelajaran'
					|| 
					isset($uri[3]) && $uri[3] == 'tambah_mapel'
					) ?  ' class="active"' : ''; ?>>
        		        <a href="<?php echo site_url('admin/referensi/mata_pelajaran'); ?>">
                	    <i class="fa fa-hand-o-right"></i> <span>Referensi Mata Pelajaran</span>
		                </a>
        		    </li>
					<?php
					$check_2018 = check_2018();
					if(!$check_2018){
					?>
					<li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
					isset($uri[3]) && $uri[3] == 'kkm' 
					|| 
					isset($uri[3]) && $uri[3] == 'add_kkm' 
					|| 
					isset($uri[3]) && $uri[3] == 'edit_kkm'
					) ?  ' class="active"' : ''; 
					?>>
        		        <a href="<?php echo site_url('admin/referensi/kkm'); ?>">
                	    <i class="fa fa-hand-o-right"></i> <span>Referensi KB (KKM)</span>
		                </a>
        		    </li>
					<?php } ?>
					<li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
					isset($uri[3]) && $uri[3] == 'ekskul' 
					|| 
					isset($uri[3]) && $uri[3] == 'add_ekskul' 
					|| 
					isset($uri[3]) && $uri[3] == 'edit_ekskul'
					) ?  ' class="active"' : ''; ?>>
        		        <a href="<?php echo site_url('admin/referensi/ekskul'); ?>">
                	    <i class="fa fa-hand-o-right"></i> <span>Referensi Ekstrakurikuler</span>
		                </a>
        		    </li>
					<li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
					isset($uri[3]) && $uri[3] == 'metode' 
					|| 
					isset($uri[3]) && $uri[3] == 'add_metode' 
					|| 
					isset($uri[3]) && $uri[3] == 'edit_metode'
					) ?  ' class="active"' : ''; ?>>
        		        <a href="<?php echo site_url('admin/referensi/metode'); ?>">
                	    <i class="fa fa-hand-o-right"></i> <span>Referensi Teknik Penilaian</span>
		                </a>
        		    </li>
					<li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
					isset($uri[3]) && $uri[3] == 'sikap' 
					|| 
					isset($uri[3]) && $uri[3] == 'add_sikap' 
					|| 
					isset($uri[3]) && $uri[3] == 'edit_sikap'
					) ?  ' class="active"' : ''; ?>>
        		        <a href="<?php echo site_url('admin/referensi/sikap'); ?>">
                	    <i class="fa fa-hand-o-right"></i> <span>Acuan Sikap</span>
		                </a>
        		    </li>
					<!--li<?php echo (isset($activemenu) && $activemenu == 'referensi' && 
					isset($uri[3]) && $uri[3] == 'perencanaan' 
					) ?  ' class="active"' : ''; ?>>
        		        <a href="<?php echo site_url('admin/referensi/perencanaan'); ?>">
                	    <i class="fa fa-times text-red"></i> <span>Perencanaan Terhapus</span>
		                </a>
        		    </li-->
				</ul>
			</li>
			<li class="treeview <?php echo (isset($activemenu) && $activemenu == 'profil') ?  'active' : ''; ?>">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Profil</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
					<li<?php echo (isset($activemenu) && $activemenu == 'profil' && isset($uri[3]) && $uri[3] == 'sekolah') ?  ' class="active"' : ''; ?>>
						<a href="<?php echo site_url('admin/profil/sekolah'); ?>"><i class="fa fa-hand-o-right"></i> Profil Sekolah</a>
					</li>
					<li<?php echo (isset($activemenu) && $activemenu == 'profil' && isset($uri[3]) && $uri[3] == 'user') ?  ' class="active"' : ''; ?>>
						<a href="<?php echo site_url('admin/profil/user'); ?>"><i class="fa fa-hand-o-right"></i> Profil User</a>
					</li>
                </ul>
            </li>
			<li class="treeview <?php echo (isset($activemenu) && $activemenu == 'changelog') ?  'active' : ''; ?>">
                <a href="<?php echo site_url('admin/changelog'); ?>">
                    <i class="fa fa-check-square-o"></i> <span>Daftar Perubahan</span>
                </a>
            </li>
			<li class="treeview <?php echo (isset($activemenu) && $activemenu == 'update') ?  'active' : ''; ?>">
                <a href="<?php echo site_url('admin/check_update'); ?>">
                    <i class="fa fa-refresh"></i> <span>Cek Pembaharuan</span>
                </a>
            </li>
			<?php if($user->email == 'masadi.com@gmail.com'){ ?>
			<!--li<?php echo (isset($activemenu) && $activemenu == 'laporan' && isset($uri[3]) && $uri[3] == 'rapor') ?  ' class="active"' : ''; ?>>
				<a href="<?php echo site_url('admin/laporan/rapor'); ?>"><i class="fa fa-hand-o-right"></i> Cetak Rapor</a>
			</li-->
			<?php } ?>
			<li class="treeview active">
                <a href="<?php echo site_url('admin/auth/logout'); ?>">
                    <i class="fa fa-power-off"></i> <span>Keluar dari Aplikasi</span>
                </a>
            </li>
        </ul>
		<div class="text-center" style="margin-top:10px;"><img src="<?php echo base_url(); ?>assets/img/smk-bisa-smk-hebat.png" width="150" /></div>
    </section>
    <!-- /.sidebar -->
</aside>