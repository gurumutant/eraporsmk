<?php if($all_siswa){
$settings 	= $this->settings->get(1);
?>
<div class="table-responsive no-padding">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="20%">Nama Siswa</th>
				<th width="40%">Deskripsi Pengetahuan</th>
				<th width="40%">Deskripsi Keterampilan</th>
			</tr>
			</thead>
			<tbody>
			<?php
			foreach($all_siswa['data'] as $key=>$siswa){
				$siswa_id = $siswa->siswa->id;
				$nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $id_mapel, $siswa_id);
				$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $id_mapel, $siswa_id);
				$kkm = get_kkm($ajaran_id, $rombel_id, $id_mapel);
				$class_pengetahuan = ($nilai_pengetahuan_value >= $kkm) ? 'bg-green' : 'bg-red';
				$class_keterampilan = ($nilai_keterampilan_value >= $kkm) ? 'bg-green' : 'bg-red';
				if(!$nilai_pengetahuan_value){
					$nilai_pengetahuan_value = '<a title="Tampilkan nilai" href="'.site_url('admin/ajax/generate_nilai/1/'.$ajaran_id.'/'.$rombel_id.'/'.$id_mapel.'/'.$siswa->siswa->id).'" class="btn generate_nilai"><i class="fa fa-search"></i></a>';
				}
				if(!$nilai_keterampilan_value){
					$nilai_keterampilan_value = '<a title="Tampilkan nilai" href="'.site_url('admin/ajax/generate_nilai/2/'.$ajaran_id.'/'.$rombel_id.'/'.$id_mapel.'/'.$siswa->siswa->id).'" class="btn generate_nilai"><i class="fa fa-search"></i></a>';
				}
				$deskripsi_mapel = $this->deskripsi_mata_pelajaran->find("semester_id = $ajaran_id and rombongan_belajar_id = $rombel_id and mata_pelajaran_id = $id_mapel and siswa_id = $siswa_id");
					if($deskripsi_mapel){
						$tombol = 1;
						$desc_id = $deskripsi_mapel->id;
						$deskripsi_pengetahuan_value = $deskripsi_mapel->deskripsi_pengetahuan;
						$deskripsi_keterampilan_value = $deskripsi_mapel->deskripsi_keterampilan;
					} else {
						$tombol = 0;
						$deskripsi_pengetahuan_value = '';
						$deskripsi_keterampilan_value = '';
					}
				?>
				<input type="hidden" name="siswa_id[]" value="<?php echo $siswa->siswa->id; ?>" />
				<tr>
					<td>
						<div><?php echo get_nama_siswa($siswa_id); ?></div>
						<div class="pull-left">Nilai Akhir : <br />
						<span class="nilai_pengetahuan label <?php echo $class_pengetahuan; ?>">P : <?php echo $nilai_pengetahuan_value; ?></span><br />

						<span class="nilai_keterampilan label <?php echo $class_keterampilan; ?>">K : <?php echo $nilai_keterampilan_value; ?></span>
						</div>
						<a title="Tampilkan sugesti" href="<?php echo site_url('admin/ajax/get_desc/'.$ajaran_id.'/'.$rombel_id.'/'.$id_mapel.'/'.$siswa->siswa->id); ?>" class="btn btn-success pull-right get_desc"><i class="fa fa-lightbulb-o"></i></a>&nbsp;&nbsp;
						<?php if($tombol){?>
						<a title="Hapus deskripsi" href="<?php echo site_url('admin/ajax/delete_desc/'.$desc_id); ?>" class="btn btn-danger pull-right delete_desc"><i class="fa fa-trash"></i></a>&nbsp;&nbsp;
						<?php } ?>
					</td>
					<td>
					<?php //echo $ajaran_id.'=>'. 1 .'=>'.$rombel_id.'=>'.$id_mapel.'=>'.$siswa_id; ?>
					<textarea name="deskripsi_pengetahuan[]" class="editor" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $deskripsi_pengetahuan_value; ?></textarea>
					</td>
					<td>
					<?php //echo $ajaran_id.'=>'. 2 .'=>'.$rombel_id.'=>'.$id_mapel.'=>'.$siswa_id; ?>
					<textarea name="deskripsi_keterampilan[]" class="editor" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $deskripsi_keterampilan_value; ?></textarea>
					</td>
				<?php
			}
			?>
			</tbody>
			</table>
			</div>
		<?php
		} else {
		?>
			<h4>Belum ada siswa di kelas terpilih</h4>
			<script>
			$('button.simpan').remove();
			</script>
		<?php } ?>
<script>
$('a.get_desc').bind('click',function(e) {
	var desc = '';
	e.preventDefault();
	var ini = $(this).parents('tr');
	var url = $(this).attr('href');
	$.get(url).done(function(response) {
		var data = $.parseJSON(response);
		if(data.result.length == 2){
			desc = '<tr><th width="50%">Pengetahuan</th><th width="50%">Keterampilan</th></tr><tr><td class="text-left" style="font-size:14px;">'+data.result[0]+'</td><td class="text-left" style="font-size:14px;">'+data.result[1]+'</td></tr>';
		} else {
			desc = '<tr><td>'+data.result[0]+'</td></tr>';
		}
		swal({
			type: 'info',
			html:
			'<table class="table table-bordered">' +
			desc +
			'</table>',
			width: 800,
			padding: 20,
			showCloseButton: false,
			showCancelButton: false,
			showConfirmButton: false,
		}).done();
	});
});
$('a.delete_desc').bind('click',function(e) {
	e.preventDefault();
	var tombol = $(this);
	var ini = $(this).parents('tr');
	var url = $(this).attr('href');
	$.get(url).done(function(response) {
		var induk = ini.children();
		var data = $.parseJSON(response);
		swal({title:data.title,type:data.type,html:data.html}).then(function() {
			if(data.type == 'success'){
				var satu = $(induk[1]).find('textarea').val('');
				var dua = $(induk[2]).find('textarea').val('');
				tombol.remove();
			}
		}).done();
	});
});
$('a.generate_nilai').bind('click',function(e) {
	e.preventDefault();
	var tombol = $(this);
	var ini = $(this).parents('tr');
	var url = $(this).attr('href');
	$.get(url).done(function(response) {
		console.log(response);
		var induk = ini.children();
		var data = $.parseJSON(response);
		if(data.kompetensi_id == 1){
			$(ini).find('.nilai_pengetahuan').removeClass('bg-red').addClass(data.class);
			$(ini).find('.nilai_pengetahuan').text('P : '+data.nilai);
		} else {
			$(ini).find('.nilai_keterampilan').removeClass('bg-red').addClass(data.class);
			$(ini).find('.nilai_keterampilan').text('K : '+data.nilai);
		}
		tombol.remove();
	});
});
</script>