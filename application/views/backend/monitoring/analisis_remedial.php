<?php
$html = '';
$settings 	= $this->settings->get(1);
$data_siswa = get_siswa_by_rombel($rombel_id, NULL, $start, $rows);
//$html .= 'ini aspek'.$aspek.'<br />';
//$html .= 'ini kompetensi_id'.$kompetensi_id;
$this->db->select('kd_id, id_kompetensi');
$this->db->from('kd_nilai as a');
$this->db->join('rencana_penilaian as b', 'a.rencana_penilaian_id = b.rencana_penilaian_id');
$this->db->where('b.semester_id', $ajaran_id);
$this->db->where('b.mata_pelajaran_id', $id_mapel);
$this->db->where('b.rombongan_belajar_id', $rombel_id);
$this->db->where('b.kompetensi_id', $kompetensi_id);
$this->db->order_by('kd_id', 'asc');
$this->db->group_by('kd_id, id_kompetensi');
$query = $this->db->get();
$get_kd_nilai = $query->result();
//$get_kd_nilai = $this->kd_nilai->find_all("rencana_penilaian_id IN (SELECT rencana_penilaian_id FROM rencana_penilaian WHERE semester_id = $ajaran_id and kompetensi_id = $kompetensi_id and rombongan_belajar_id = '$rombel_id' and mata_pelajaran_id = $id_mapel) GROUP BY kd_id", 'kd_id');
		$kkm = get_kkm($ajaran_id, $rombel_id, $id_mapel);
		$html .= '<div class="table-responsive no-padding">';
		$html .= '<table class="table table-bordered table-hover">';
		$html .= '<thead>';
		$html .= '<tr>';
		$html .= '<th rowspan="3" style="vertical-align: middle;">Nama Peserta Didik</th>';
		$html .= '<th class="text-center" colspan="'.( count($get_kd_nilai) * 2 ).'">Kompetensi Dasar</th>';
		$html .= '<th rowspan="3" style="vertical-align: middle;" class="text-center">Rerata Akhir</th>';
		$html .= '<th rowspan="3" style="vertical-align: middle;" class="text-center">Rerata Remedial</th>';
		$html .= '</tr>';
		$html .= '<tr>';
		foreach($get_kd_nilai as $kd_nilai){
			//$get_kd_nilai = $this->kd_nilai->get($p_tinggi->kd_nilai_id);
			$kd = $this->kompetensi_dasar->get($kd_nilai->kd_id);
			$kompetensi_dasar = ($kd->kompetensi_dasar_alias) ? $kd->kompetensi_dasar_alias : $kd->kompetensi_dasar;
			$html .= '<th colspan="2" class="text-center"><a href="javacript:void(0)" class="tooltip-left" title="'.strip_tags($kompetensi_dasar).'">&nbsp;&nbsp;&nbsp;'.$kd->id_kompetensi.'&nbsp;&nbsp;&nbsp;</a></th>';
		}
		$html .= '</tr>';
		$html .= '<tr>';
		foreach($get_kd_nilai as $kd_nilai){
			$html .= '<th class="text-center bg-gray disabled color-palette">A</th>';
			$html .= '<th class="text-center">R</th>';
		}
		$html .= '</tr>';
		$html .= '</thead>';
		$html .= '<tbody>';
		$no=0;
		foreach($data_siswa['data'] as $siswa){
			$siswa_id = $siswa->siswa_id;
			$html .= '<tr>';
			$html .= '<td>';
			$html .= strtoupper($siswa->nama);
			$html .= '</td>';
			$nilai_rerata_asli = 0;
			$nilai_rerata_remedial = 0;
			if($get_kd_nilai){
				foreach($get_kd_nilai as $key=>$kd_nilai){
					$nilai_asli = get_nilai_siswa_by_kd($kd_nilai->kd_id, $ajaran_id, $kompetensi_id, $rombel_id, $id_mapel, $siswa_id, 'asli');
					$nilai_remedial = get_nilai_siswa_by_kd($kd_nilai->kd_id, $ajaran_id, $kompetensi_id, $rombel_id, $id_mapel, $siswa_id, 'remedial');
					$nilai_asli = ($nilai_asli) ? number_format($nilai_asli,0) : 0;
					$nilai_remedial = ($nilai_remedial) ? number_format($nilai_remedial[$key],0) : 0;
					$html .= '<td class="text-center"><strong>';
					$html .= $nilai_asli;
					$html .= '</strong></td>';
					$html .= '</td>';
					$html .= '<td class="text-center">';
					$html .= $nilai_remedial;
					$html .= '</td>';
					$nilai_rerata_asli += $nilai_asli;
					$nilai_rerata_remedial += $nilai_remedial;
				}
			} else {
				$html .= '<td class="text-center"><strong>-</strong></td>';
			}
			$rerata_akhir = ($nilai_rerata_asli) ? number_format($nilai_rerata_asli / count($get_kd_nilai) ,0) : 0;
			$rerata_remedial = ($nilai_rerata_remedial) ? number_format($nilai_rerata_remedial / count($get_kd_nilai) ,0) : 0;
			if($kkm > $rerata_akhir){
				$bg_nilai_asli = ' text-red';
			} else {
				$bg_nilai_asli = ' text-green';
			}
			if($kkm > $rerata_remedial){
				$bg_nilai_remedial = ' text-red';
			} else {
				$bg_nilai_remedial = ' text-green';
			}
			$html .= '<td class="text-center bg-gray disabled color-palette'.$bg_nilai_asli.'"><strong>';
			$html .= $rerata_akhir;
			$html .= '</strong></td>';
			$html .= '<td class="text-center bg-gray disabled color-palette'.$bg_nilai_remedial.'"><strong>';
			$html .= $rerata_remedial;
			$html .= '</strong></td>';
			$html .= '</tr>';
		}
		$html .= '<input type="hidden" id="get_all_kd" value="'.count($get_kd_nilai).'" />';
		$html .= '</tbody>';
		$html .= '</table>';
		$html .= '</div>';
		$html .= '<div class="row">';
		$html .= '<div class="col-xs-6">Menampilkan '.$data_siswa['dari'].' sampai '.$data_siswa['ke'].' dari total '.$data_siswa['count'].' data</div>';
		$html .= '<div class="col-xs-6">';
		$html .= '<div class="dataTables_paginate paging_bootstrap">';
		$html .= '<ul class="pagination">';
		$start = ($start) ? $start : 1;
		for($i=1;$i<=$data_siswa['page'];$i++){
			if($i == $start){
				$class= ' class="active"';
			} else {
				$class = '';
			}
			$html .= '<li'.$class.'><a class="next_page" data-aksi="'.site_url('admin/ajax/get_analisis_remedial').'" href="'.site_url('admin/ajax/get_next_siswa/'.$rombel_id.'/'.$i).'/'.$ajaran_id.'/'.$id_mapel.'/'.$kelas.'/'.$aspek.'">'.$i.'</a></li>';
		}
		//$html .='<li class="prev disabled"><a href="#">&laquo; Sebelumnya</a></li><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li class="next"><a href="#">Selanjutnya &raquo;</a></li>
		$html .= '</ul>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= link_tag('assets/css/tooltip-viewport.css', 'stylesheet', 'text/css');
		$html .= '<script src="'.base_url().'assets/js/tooltip-viewport.js"></script>';
		$html .= '<script src="'.base_url().'assets/js/remedial.js"></script>';
		$html .= '<script src="'.base_url().'assets/js/delete_remed.js"></script>';
		echo $html;
?>