<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
        <?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<div class="box box-info">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_1" data-toggle="tab">Pengetahuan</a></li>
			<li><a href="#tab_2" data-toggle="tab">Keterampilan</a></li>
		</ul>
	</div>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1">
			<div class="box-body">
				<table id="datatable" class="table table-bordered table-striped table-hover">
					<thead>
					<tr>
						<th style="width: 10%">Kompetensi Penilaian</th>
						<th style="width: 20%">Nama Metode</th>
						<th style="width: 8%" class="text-center">Tindakan</th>
					</tr>
					</thead>
					<tbody>
					</tbody>    
				</table>
			</div><!-- /.box-body -->
		</div>
		<div class="tab-pane" id="tab_2">
			<?php echo form_open('admin/referensi/simpan');?>
			<input type="hidden" name="query" id="query" value="bobot_metode" />
			<div class="box-body">
				<table class="table table-bordered table-striped table-hover">
					<thead>
					<tr>
						<th style="width: 20%">Nama Metode</th>
						<th style="width: 8%" class="text-center">Bobot</th>
					</tr>
					</thead>
					<tbody>
					<?php
					$all_keterampilan = $this->teknik_penilaian->find_all("kompetensi_id = 2", '*','kompetensi_id desc');
					foreach($all_keterampilan as $keterampilan){
					?>
						<tr>
							<td><?php echo $keterampilan->nama; ?></td>
							<td><input type="text" name="bobot[<?php echo $keterampilan->teknik_penilaian_id; ?>]" class="form-control input-sm" value="<?php echo $keterampilan->bobot; ?>" /></td>
						</tr>
					<?php } ?>
					</tbody>    
				</table>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-success simpan">Simpan</button>
			</div>
            <?php echo form_close();  ?>
		</div>
	</div>
</div><!-- /.box -->
</div>
</div>