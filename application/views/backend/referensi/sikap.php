<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
        <?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<div class="box box-info">
    <div class="box-body">
		<?php if($check_2018){?>
        <table class="table table-bordered">
			<thead>
				<th width="20%" class="text-center">Integritas</th>
				<th width="20%" class="text-center">Religius</th>
				<th width="20%" class="text-center">Nasionalis</th>
				<th width="20%" class="text-center">Mandiri</th>
				<th width="20%" class="text-center">Gotong-royong</th>
			</thead>
			<tbody>
				<tr>
					<td>
					<ul style="padding-left:10px;">
						<li>Kesetiaan</li>
						<li>Antikorupsi</li>
						<li>Keteladanan</li>
						<li>Keadilan</li>
						<li>Menghargai martabat manusia</li>
					</ul>
					</td>
					<td>
					<ul style="padding-left:10px;">
						<li>Melindungi yang kecil dan tersisih</li>
						<li>Taat beribadah</li>
						<li>Menjalankan ajaran agama</li>
						<li>Menjauhi larangan agama</li>
					</ul>
					</td>
					<td>
					<ul style="padding-left:10px;">
						<li>Rela berkorban</li>
						<li>Taat hukum</li>
						<li>Unggul</li>
						<li>Disiplin</li>
						<li>Berprestasi</li>
						<li>Cinta damai</li>
					</ul>
					</td>
					<td>
					<ul style="padding-left:10px;">
						<li>Tangguh</li>
						<li>Kerja keras</li>
						<li>Kreatif</li>
						<li>Keberanian</li>
						<li>Pembelajar</li>
						<li>Daya juang</li>
						<li>Berwawasan informasi dan teknologi</li>
					</ul>
					</td>
					<td>
					<ul style="padding-left:10px;">
						<li>Musyawarah</li>
						<li>Tolong-menolong</li>
						<li>Kerelawanan</li>
						<li>Solidaritas</li>
						<li>Antidiskriminasi</li>
					</ul>
					</td>
				</tr>
			</tbody>
		</table>
        <?php } else { ?>
        <table class="table table-bordered table-striped table-hover" style="display:none;">
            <thead>
            <tr>
				<th style="width: 10px;" class="text-center">No.</th>
                <th style="width: 100%">Nama Sikap</th>
            </tr>
            </thead>
			<tbody>
				<?php
				$all_sikap = $this->sikap->get_all(); 
				$i=1;
				foreach($all_sikap as $sikap){
				?>
				<tr>
					<td class="text-center"><?=$i;?></td>
					<td><?php echo $sikap->butir_sikap; ?></td>
				</tr>
				<?php $i++; } ?>
			</tbody>      
        </table>
		<?php } ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>