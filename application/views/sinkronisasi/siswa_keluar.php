<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="row">
<!-- left column -->
<div class="col-md-12">
<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title">Sinkronisasi <?php echo $page_title.' ('.$total_rows.' => '.$inserted. ')'; ?></h3>
	</div>
    <div class="box-body">
	<div class="text-center">
		<?php //echo $pagination; ?>
	</div>
	<table class="table table-bordered table-striped table-hover">
            <thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Nama Siswa</th>
					<th class="text-center">Rombongan Belajar</th>
					<th class="text-center">Status Siswa</th>
					<th class="text-center">Status Anggota Rombel</th>
					<th class="text-center">Status User</th>
	            </tr>
            </thead>
			<tbody>
			<?php
				$no = $this->uri->segment('4') + 1;
				foreach($dapodik as $data){
					//test($data);
					$find_siswa = $this->siswa->find("siswa_id_dapodik = '$data->peserta_didik_id' AND active = 1");
					$siswa_id 			= 0;
					$anggota_rombel_id 	= 0;
					$user_id 			= 0;
					$rombongan_belajar_id = 0;
					$result_1 			= 'Abaikan';
					$result_2 			= 'Abaikan';
					$result_3 			= 'Abaikan';
					$class 				= 'table-success"';
					if($find_siswa){
						$class = 'table-danger"';
						$find_anggota_rombel = $this->anggota_rombel->find("siswa_id = '$find_siswa->siswa_id' AND semester_id = $ajaran->id");
						$rombongan_belajar_id = $find_anggota_rombel->rombongan_belajar_id;
						$siswa_id = $find_siswa->siswa_id;
						if($find_anggota_rombel){
							$result_1 = 'Terhapus';
							$anggota_rombel_id 	= $find_anggota_rombel->anggota_rombel_id;
						}
						$find_user = $this->user->get($find_siswa->user_id);
						if($find_user){
							$result_3 = 'Non Aktif';
							$user_id = $find_user->user_id;
						}
						$result_2 = 'Terhapus';
						$nama_siswa = get_nama_siswa($siswa_id);
						$nama_rombongan_belajar = get_nama_rombel($rombongan_belajar_id);
					} else {
						$nama_siswa = $data->nama_siswa;
						$nama_rombongan_belajar = $data->nama_rombongan_belajar;
						$find_siswa_insert = $this->siswa->find("siswa_id_dapodik = '$data->peserta_didik_id'");
						if(!$find_siswa_insert){
							$result_1 			= 'Terhapus';
							$result_2 			= 'Terhapus';
							$result_3 			= 'Terhapus';
							$class 				= 'table-danger"';
							$insert_siswa = array(
								'siswa_id' 			=> gen_uuid(),
								'sekolah_id'		=> $loggeduser->sekolah_id,
								'nama' 				=> $data->nama_siswa,
								'no_induk' 			=> ($data->nipd) ? $data->nipd : 0,
								'nisn' 				=> ($data->nisn) ? $data->nisn : GenerateNISN(),
								'jenis_kelamin' 	=> ($data->jenis_kelamin) ? $data->jenis_kelamin : 0,
								'tempat_lahir' 		=> ($data->tempat_lahir) ? $data->tempat_lahir : 0,
								'tanggal_lahir' 	=> $data->tanggal_lahir,
								'agama_id' 			=> ($data->agama_id) ? $data->agama_id : 0,
								'status' 			=> 'Anak Kandung',
								'anak_ke' 			=> ($data->anak_keberapa) ? $data->anak_keberapa : 0,
								'alamat' 			=> ($data->alamat_jalan) ? $data->alamat_jalan : 0,
								'rt' 				=> ($data->rt) ? $data->rt : 0,
								'rw' 				=> ($data->rw) ? $data->rw : 0,
								'desa_kelurahan' 	=> ($data->desa_kelurahan) ? $data->desa_kelurahan : 0,
								'kecamatan' 		=> 0,
								'kode_pos' 			=> ($data->kode_pos) ? $data->kode_pos : 0,
								'no_telp' 			=> ($data->nomor_telepon_seluler) ? $data->nomor_telepon_seluler : 0,
								'sekolah_asal' 		=> ($data->sekolah_asal) ? $data->sekolah_asal : 0,
								'diterima_kelas' 	=> 0,
								'diterima' 			=> ($data->tanggal_masuk_sekolah) ? $data->tanggal_masuk_sekolah : 0,
								'email' 			=> ($data->email) ? $data->email : GenerateEmail().'@erapor-smk.net',
								'nama_ayah' 		=> ($data->nama_ayah) ? $data->nama_ayah : 0,
								'nama_ibu' 			=> ($data->nama_ibu_kandung) ? $data->nama_ibu_kandung : 0,
								'kerja_ayah' 		=> ($data->pekerjaan_id_ayah) ? $data->pekerjaan_id_ayah : 0,
								'kerja_ibu' 		=> ($data->pekerjaan_id_ibu) ? $data->pekerjaan_id_ibu : 0,
								'nama_wali' 		=> ($data->nama_wali) ? $data->nama_wali : 0,
								'alamat_wali' 		=> ($data->alamat_jalan) ? $data->alamat_jalan : 0,
								'telp_wali' 		=> ($data->nomor_telepon_seluler) ? $data->nomor_telepon_seluler : 0,
								'kerja_wali' 		=> ($data->pekerjaan_id_wali) ? $data->pekerjaan_id_wali : 0,
								'photo' 			=> 0,
								'active' 			=> 0,
								//'password_dapo' 	=> md5(12345678),
								//'petugas' 			=> $loggeduser->username,
								'siswa_id_dapodik' 	=> $data->peserta_didik_id,
							);
							$password = 12345678;
							$additional_data = array(
								"sekolah_id"		=> $loggeduser->sekolah_id,
								"nisn"				=> $insert_siswa['nisn'],
								'password_dapo'		=> md5($password),
								'active' 			=> 0,
								'created_at'		=> date('Y-m-d H:i:s'),
								'updated_at'		=> date('Y-m-d H:i:s'),
							);
							$attributes_insert_anggota_rombel = array(
								'anggota_rombel_id' 		=> gen_uuid(),
								'semester_id' 				=> $ajaran->id, 
								'rombongan_belajar_id' 		=> $data->rombongan_belajar_id, 
								'siswa_id' 					=> $insert_siswa['siswa_id'],
								'anggota_rombel_id_dapodik'	=> $data->anggota_rombel_id,
								'deleted_at'				=> date('Y-m-d H:i:s'),
							);
							$this->anggota_rombel->insert($attributes_insert_anggota_rombel);
							$group = array('4');
							$user_id = $this->ion_auth->register(trim($data->nama_siswa), $password, $insert_siswa['email'], $additional_data, $group);
							$insert_siswa['user_id'] = $user_id;
							$this->siswa->insert($insert_siswa);
							//test($insert_siswa);
							//test($additional_data);
							//test($attributes_insert_anggota_rombel);
						}
					}
			?>
				<tr class="<?php echo $class; ?>">
					<td class="text-center"><?php echo $no++; ?></td>
					<td><?php echo $nama_siswa; ?></td>
					<td class="text-center"><?php echo $nama_rombongan_belajar; ?></td>
					<td class="text-center"><?php echo $result_1; ?></td>
					<td class="text-center"><?php echo $result_2; ?></td>
					<td class="text-center"><?php echo $result_3; ?></td>
				</tr>
			<?php
			if($siswa_id){
				$this->siswa->update($siswa_id, array('active' => 0));
			}
			if($anggota_rombel_id){
				$this->anggota_rombel->delete($anggota_rombel_id);
			}
			if($user_id){
				$this->user->update($user_id, array('active' => 0));
			}
			} ?>
			</tbody>
		</table>
    </div><!-- /.box-body -->
	<div class="box-footer text-center">
		<?php echo $pagination; ?>
	</div>
</div><!-- /.box -->
</div>
<script>
$(document).ready(function(){
	$('body').mouseover(function(){
		$(this).css({cursor: 'wait'});
	});
	var cari = $('body').find('.next');
	if(cari.length>0){
		var cari_a = $(cari).find('a');
		var url = $(cari_a).attr('href');
		window.location.replace(url);
	} else {
		window.location.replace('<?php echo site_url('admin/sinkronisasi'); ?>');
	}
})
</script>