<?php
$ajaran = get_ta();
$tahun = $ajaran->tahun;
$smt = $ajaran->semester;
$tahun = substr($tahun, 0,4); // returns "d"
$semester_id = $tahun.$smt;
$tahun_ajaran_id = substr($ajaran->tahun,0,4);?>
<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
<?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<div class="box box-info">
    <div class="box-body">
	<?php
		if($response && !$response->post_login){ ?>
			<div class="callout callout-danger lead">Anda terhubung ke server direktorat.<br /><?php echo $response->message; ?></div>
		<?php
		} elseif($response->post_login){
		$id_sekolah_dapodik = $response->sekolah_id;
	?>
		<table class="table table-bordered table-striped table-hover">
            <thead>
				<tr>
					<th class="text-center">Data</th>
					<th class="text-center">Status</th>
					<th class="text-center">Jml Data Dapodik</th>
					<th class="text-center">Jml Data Erapor</th>
					<th class="text-center">Jml Data Sudah Tersinkronisasi</th>
					<th class="text-center">Aksi</th>
	            </tr>
            </thead>
			<tbody>
			<?php
			$sekolah_erapor = $this->sekolah->find_all("sekolah_id = '$id_sekolah_dapodik'");
			$sekolah_sinkron = $this->sekolah->get("$id_sekolah_dapodik");
			$sekolah_erapor = count($sekolah_erapor);
			$sekolah_sinkron = count($sekolah_sinkron);
			$jurusan = $this->jurusan_sp->find_all("sekolah_id = '$id_sekolah_dapodik'");
			if(!$jurusan){
				$sekolah_sinkron = 0;
				$sekolah_erapor = 0;
			}
			$ptk_terdaftar = $response->ptk_terdaftar;
			$rombongan_belajar = $response->rombongan_belajar;
			$registrasi_peserta_didik = $response->registrasi_peserta_didik;
			$pembelajaran_dapodik = $response->pembelajaran_dapodik;
			$ekskul_dapodik = isset($response->ekskul_dapodik) ? $response->ekskul_dapodik : 0;
			// ================== erapor =============== //
			$guru_erapor = $this->guru_terdaftar->find_all("sekolah_id = '$id_sekolah_dapodik' AND semester_id = $ajaran->id AND guru_id IN (SELECT guru_id FROM ref_guru WHERE guru_id_dapodik IS NOT NULL)");
			$guru_sinkron = $this->guru_terdaftar->find_all("sekolah_id = '$id_sekolah_dapodik' AND status = 1 AND semester_id = $ajaran->id AND guru_id IN (SELECT guru_id FROM ref_guru WHERE guru_id_dapodik IS NOT NULL)");
			$rombel_erapor = $this->rombongan_belajar->find_all("sekolah_id = '$id_sekolah_dapodik' AND semester_id = $ajaran->id");
			$rombel_sinkron = $this->rombongan_belajar->find_all("sekolah_id = '$id_sekolah_dapodik' AND semester_id = $ajaran->id AND rombel_id_dapodik IS NOT NULL");
			$siswa_erapor = $this->anggota_rombel->find_all("sekolah_id = '$id_sekolah_dapodik' AND semester_id = $ajaran->id AND rombongan_belajar_id IN(SELECT rombongan_belajar_id FROM rombongan_belajar WHERE sekolah_id = '$id_sekolah_dapodik' AND semester_id = $ajaran->id)");
			$siswa_sinkron = $this->anggota_rombel->find_all("sekolah_id = '$id_sekolah_dapodik' AND semester_id = $ajaran->id AND anggota_rombel_id_dapodik IS NOT NULL AND rombongan_belajar_id IN(SELECT rombongan_belajar_id FROM rombongan_belajar WHERE sekolah_id = '$id_sekolah_dapodik' AND semester_id = $ajaran->id)");
			$siswa_keluar_dapodik = $response->siswa_keluar_dapodik;
			$siswa_keluar_erapor = $this->siswa->find_count("sekolah_id = '$id_sekolah_dapodik' AND active = 0 AND siswa_id IN (SELECT siswa_id FROM anggota_rombel WHERE semester_id = $ajaran->id AND deleted_at IS NOT NULL)");
			$this->db->select('*');
			$this->db->from('ref_siswa');
			$this->db->join('anggota_rombel', 'anggota_rombel.siswa_id = ref_siswa.siswa_id');
			$this->db->join('users', 'users.user_id = ref_siswa.user_id');
			$this->db->where('ref_siswa.active', 0);
			$this->db->where("anggota_rombel.deleted_at IS NOT NULL");
			$this->db->where('users.active', 0);
			$this->db->where("ref_siswa.sekolah_id", $id_sekolah_dapodik);
			$siswa_keluar_sinkron = $this->db->count_all_results();
			$pembelajaran_erapor = $this->pembelajaran->find_all("sekolah_id = '$id_sekolah_dapodik' AND semester_id = $ajaran->id");
			$pembelajaran_sinkron = $this->pembelajaran->find_all("sekolah_id = '$id_sekolah_dapodik' AND semester_id = $ajaran->id AND pembelajaran_id IS NOT NULL");
			$jurusan_dapodik = 0;
			$jurusan_erapor = 0;
			$jurusan_sinkron = 0;
			$mata_pelajaran_dapodik = 0;
			$mata_pelajaran_erapor = 0;
			$mata_pelajaran_sinkron = 0;
			$ekskul_erapor	= $this->ekstrakurikuler->find_count("sekolah_id = '$id_sekolah_dapodik' AND semester_id = $ajaran->id");
			$ekskul_sinkron	= $this->ekstrakurikuler->find_count("sekolah_id = '$id_sekolah_dapodik' AND semester_id = $ajaran->id AND is_dapodik = 1");
			$data = array(
				0 => 
					array(
						'nama' => 'Sekolah',
						'link' => 'sekolah',
						'get_dapodik' => 1,
						'get_erapor' => $sekolah_erapor,
						'get_sinkron' => $sekolah_sinkron,
						'class' => 'count_sekolah',
					), 
				1 => 
					array(
						'nama' => 'PTK',
						'link' => 'guru',
						'get_dapodik' => $ptk_terdaftar,
						'get_erapor' => count($guru_erapor),
						'get_sinkron' => count($guru_sinkron),
						'class' => 'count_guru',
					), 
				2 => 
					array(
						'nama' => 'Rombongan Belajar',
						'link' => 'rombongan_belajar',
						'get_dapodik' => $rombongan_belajar,
						'get_erapor' => count($rombel_erapor),
						'get_sinkron' => count($rombel_sinkron),
						'class' => 'count_rombel',
					), 
				3 => 
					array(
						'nama' => 'Peserta Didik Aktif',
						'link' => 'siswa_aktif',
						'get_dapodik' => $registrasi_peserta_didik,
						'get_erapor' => count($siswa_erapor),
						'get_sinkron' => count($siswa_sinkron),
						'class' => 'count_siswa',
					), 
				4 => 
					array(
						'nama' => 'Peserta Didik Keluar',
						'link' => 'siswa_keluar',
						'get_dapodik' => $siswa_keluar_dapodik,
						'get_erapor' => $siswa_keluar_erapor,
						'get_sinkron' => $siswa_keluar_sinkron,
						'class' => 'count_siswa',
					), 
				5 => 
					array(
						'nama' => 'Pembelajaran',
						'link' => 'pembelajaran',
						'get_dapodik' => $pembelajaran_dapodik,
						'get_erapor' => count($pembelajaran_erapor),
						'get_sinkron' => count($pembelajaran_sinkron),
						'class' => 'count_pembelajaran',
					),
				6 => 
					array(
						'nama' => 'Ekstrakurikuler',
						'link' => 'ekskul',
						'get_dapodik' => $ekskul_dapodik,
						'get_erapor' => $ekskul_erapor,
						'get_sinkron' => $ekskul_sinkron,
						'class' => 'count_ekskul',
					),
				7 => 
					array(
						'nama' => 'Jurusan',
						'link' => 'jurusan',
						'get_dapodik' => isset($response->jurusan) ? $response->jurusan : 617,
						'get_erapor' => $this->jurusan->count_all(),
						'get_sinkron' => $this->jurusan->count_all(),
						'class' => 'count_ekskul',
					),
				8 => 
					array(
						'nama' => 'Kurikulum',
						'link' => 'kurikulum',
						'get_dapodik' => isset($response->kurikulum) ? $response->kurikulum : 581,
						'get_erapor' => $this->kurikulum->count_all(),
						'get_sinkron' => $this->kurikulum->count_all(),
						'class' => 'count_ekskul',
					),
				9 => 
					array(
						'nama' => 'Mata Pelajaran',
						'link' => 'mata_pelajaran',
						'get_dapodik' => isset($response->mata_pelajaran) ? $response->mata_pelajaran : 3405,
						'get_erapor' => $this->mata_pelajaran->count_all(),
						'get_sinkron' => $this->mata_pelajaran->count_all(),
						'class' => 'count_ekskul',
					),
				10 => 
					array(
						'nama' => 'Mata Pelajaran Kurikulum',
						'link' => 'mapel_kur',
						'get_dapodik' => isset($response->mata_pelajaran_kurikulum) ? $response->mata_pelajaran_kurikulum : 51329,
						'get_erapor' => $this->mata_pelajaran_kurikulum->count_all(),
						'get_sinkron' => $this->mata_pelajaran_kurikulum->count_all(),
						'class' => 'count_ekskul',
					),
			);
			$i=0;
			//echo $response->mata_pelajaran_kurikulum;
			foreach($data as $d){
				if($d['get_sinkron']){
					$status = 'Lengkap';
					$btn = 'btn-danger';
					$text = 'Update';
					if($d['get_dapodik'] > $d['get_sinkron']){
						$status = 'Kurang';
						$btn = 'btn-warning';
						$text = 'Sinkron Ulang';
					}
				} else {
					$status = 'Belum';
					$btn = 'btn-success';
					$text = 'Sinkron';
				}
				if($d['link'] == 'mata_pelajaran' || $d['link'] == 'jurusan'){
					$id_sekolah_dapodik = '';
				}
			?>
				<tr>
					<td><?php echo $d['nama']; ?></td>
					<td class="text-center"><?php echo $status; ?></td>
					<td class="text-center"><?php echo $d['get_dapodik']; ?></td>
					<td class="text-center"><?php echo $d['get_erapor']; ?></td>
					<td class="text-center <?php echo $d['class']; ?>"><?php echo $d['get_sinkron']; ?></td>
					<td class="text-center"><a href="<?php echo site_url('admin/sinkronisasi/'.$d['link']); ?>" class="<?php echo $d['class']; ?> btn <?php echo $btn; ?> btn-block"><?php echo $text; ?></a></td>
				</tr>
			<?php 
			$i++;
			} ?>
			</tbody>
		</table>
		<div class="progress active" style="display:none;">
			<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
			</div>
		</div>
		<div id="result" class="callout callout-danger lead" style="display:none;"></div>
		<?php } else { ?>
		<div class="callout callout-danger lead">Anda tidak terhubung ke server direktorat.<br />Pastikan PC/Laptop Anda terhubung ke internet!</div>
		<?php } ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>
<script>
/*
var index = 0;
var jumlah = 0;
var siswa_dapodik = <?php echo count($siswa_sinkron); ?>;
function DoAjaxProgressSiswa(data,length) {
	$('#spinner').show();
	$.ajax({
		url: '<?php echo site_url('admin/sinkronisasi/proses'); ?>',
		type: 'post',
		data: {length:length,parameter:JSON.stringify(data.result[index]),siswa_dapodik:siswa_dapodik},
		success: function(response){
			var result = $.parseJSON(response);
			$('.progress-bar').css('width', result.persen+'%').attr('aria-valuenow', result.persen);
			$('.count_siswa').html(result.jumlah);
			$('#result').html(result.text);
			jumlah = result.jumlah;
			if(length == jumlah){
				window.location.replace('<?php echo site_url('admin/sinkronisasi'); ?>');
			}
		}
	});
	index++;
}
function DoAjaxProgressCall(data,length){
	setInterval( function() {
		DoAjaxProgressSiswa(data,length);
	}, 500 );
}
$('a.count_siswa').bind('click',function(e) {
	e.preventDefault();
	var url = $(this).attr('href');
	$.get(url).done(function(response) {
		$('.progress').show();
		$('#result').show();
		var data = $.parseJSON(response);
		var length = data.result.length;
		DoAjaxProgressCall(data,length);
	});
	return false;
});
*/
</script>