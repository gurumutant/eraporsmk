<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="row">
<!-- left column -->
<div class="col-md-12">
<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title">Sinkronisasi <?php echo $page_title; ?></h3>
	</div>
    <div class="box-body">
	<table class="table table-bordered table-striped table-hover">
	<!--table class="table table-bordered table-striped table-hover" style="display:none;"-->
            <thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Kurikulum</th>
					<th class="text-center">Mata Pelajaran</th>
					<th class="text-center">Tingkat Pendidikan</th>
					<th class="text-center">status</th>
	            </tr>
            </thead>
			<tbody>
			<?php
				$no = $this->uri->segment('4') + 1;
				if($dapodik){
					foreach($dapodik as $data){
						$data->created_at = $data->create_date;
						$data->updated_at = $data->last_update;
						$data->deleted_at = $data->expired_date;
						unset($data->create_date, $data->last_update, $data->expired_date);
						//test($data);
						//echo $data->kurikulum_id.'=>'.$data->mata_pelajaran_id.'=>'.$data->tingkat_pendidikan_id;
						//$find = $this->mata_pelajaran_kurikulum->find("kurikulum_id = $data->kurikulum_id AND mata_pelajaran_id = $data->mata_pelajaran_id AND tingkat_pendidikan_id = $data->tingkat_pendidikan_id");
						$this->db->select('*');
						$this->db->from('mata_pelajaran_kurikulum');
						$this->db->where('kurikulum_id', $data->kurikulum_id);
						$this->db->where('mata_pelajaran_id', $data->mata_pelajaran_id);
						$this->db->where('tingkat_pendidikan_id', $data->tingkat_pendidikan_id);
						$query = $this->db->get();
						$find = $query->row();
						//test($find);
						$this->db->select('*');
						$this->db->from('ref_kurikulum');
						$this->db->where('kurikulum_id', $data->kurikulum_id);
						$query = $this->db->get();
						$get_kurikulum = $query->row();
						$this->db->select('*');
						$this->db->from('mata_pelajaran');
						$this->db->where('mata_pelajaran_id', $data->mata_pelajaran_id);
						$query = $this->db->get();
						$get_nama_mapel = $query->row();
						if($find){
							$this->db->where('kurikulum_id', $find->kurikulum_id);
							$this->db->where('mata_pelajaran_id', $find->mata_pelajaran_id);
							$this->db->where('tingkat_pendidikan_id', $find->tingkat_pendidikan_id);
							$this->db->update('mata_pelajaran_kurikulum', $data);
							$result = 'update';
							$class = 'table-danger"';
						} else {
							//$find_mata_pelajaran = $this->mata_pelajaran->get($data->mata_pelajaran_id);
							//$find_kurikulum = $this->kurikulum->get($data->kurikulum_id);
							//if($find_mata_pelajaran && $find_kurikulum){
							if($get_nama_mapel && $get_kurikulum){
								//$this->mata_pelajaran_kurikulum->insert($data);
								$this->db->insert('mata_pelajaran_kurikulum', $data);
								$result = 'insert';
								$class = 'table-success"';
							} else {
								$result = 'ignore';
								$class = 'table-warning"';
							}
						}
				?>
					<tr class="<?php echo $class; ?>">
						<td class="text-center"><?php echo $no++; ?></td>
						<td><?php echo ($get_kurikulum) ? $get_kurikulum->nama_kurikulum : 'Kurikulum tidak ditemukan('.$data->kurikulum_id.')'; ?></td>
						<td><?php echo ($get_nama_mapel) ? $get_nama_mapel->nama.'('.$data->mata_pelajaran_id.')' : '('.$data->mata_pelajaran_id.')'; ?></td>
						<td class="text-center"><?php echo $data->tingkat_pendidikan_id; ?></td>
						<td class="text-center"><?php echo $result; ?></td>
					</tr>
				<?php
				//}
				//break; 
				} 
			} else { ?>
					<tr class="table-danger">
						<td colspan="5" class="text-center">Tidak ada data untuk ditampilkan</td>
					</tr>
			<?php } ?>
			</tbody>
		</table>
    </div><!-- /.box-body -->
	<div class="box-footer text-center">
		<?php echo $pagination; ?>
	</div>
</div><!-- /.box -->
</div>
<script>
$(document).ready(function(){
	$('body').mouseover(function(){
		$(this).css({cursor: 'wait'});
	});
	var cari = $('body').find('.next');
	if(cari.length>0){
		var cari_a = $(cari).find('a');
		var url = $(cari_a).attr('href');
		window.location.replace(url);
	} else {
		window.location.replace('<?php echo site_url('admin/sinkronisasi'); ?>');
	}
})
</script>