<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="row">
<!-- left column -->
<div class="col-md-12">
<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title">Sinkronisasi <?php echo $page_title.' ('.$total_rows.' => '.$inserted. ')'; ?></h3>
	</div>
    <div class="box-body">
	<div class="text-center">
		<?php //echo $pagination; ?>
	</div>
	<table class="table table-bordered table-striped table-hover">
            <thead>
				<tr>
					<th class="text-center" style="vertical-align: middle;">No</th>
					<th class="text-center">nama</th>
					<th class="text-center">jenis_kelamin</th>
					<th class="text-center">tempat_lahir</th>
					<th class="text-center">tanggal_lahir</th>
					<th class="text-center">nik</th>
					<th class="text-center">nuptk</th>
					<th class="text-center">email</th>
					<th class="text-center">status</th>
	            </tr>
            </thead>
			<tbody>
			<?php
				$no = $this->uri->segment('4') + 1;
				//$sekolah = $this->sekolah->find_by_sekolah_id_dapodik($loggeduser->sekolah_id);
				//$sekolah_id = ($sekolah) ? $sekolah->sekolah_id : 0;
				foreach($dapodik as $data){					
					$data->nuptk = str_replace(' ','',$data->nuptk);
					$data_sync = array(
						'ptk_id'	=> $data->ptk_id,
					);
					$response = post_sync('rwy_pend_formal', $data_sync);
					$find_gelar = ($response) ? $response->rwy_pend_formal : '';
					$data_guru = $this->guru->find_by_guru_id_dapodik($data->ptk_id);
					$data->nuptk = str_replace('-','',$data->nuptk);
					$data->nuptk = str_replace(' ','',$data->nuptk);
					if($data_guru){
						$data->nuptk = ($data_guru->nuptk) ? $data_guru->nuptk : GenerateID();
					} else {
						$data->nuptk = ($data->nuptk) ? $data->nuptk : GenerateID();
					}
					/*$nama_ptk 		= addslashes($data->nama);
					if($data->nuptk && is_numeric($data->nuptk) && strlen($data->nuptk) > 10){
						$data->nuptk = $data->nuptk;
						$data_guru = $this->guru->find("nama = '$nama_ptk' AND nuptk = '$data->nuptk' AND tanggal_lahir = '$data->tanggal_lahir'");
					} else {
						$data->nuptk = GenerateID();
						$data_guru = $this->guru->find("nama = '$nama_ptk' AND tanggal_lahir = '$data->tanggal_lahir'");
					}*/
					$data->email = ($data->email) ? $data->email : GenerateEmail().'@erapor-smk.net';
					if($data->email == $loggeduser->email){
						$data->email = GenerateEmail().'@erapor-smk.net';
					}
					$data->email = strtolower($data->email);
					$query = $this->db->get_where('mst_wilayah', array('kode_wilayah' => $data->kode_wilayah));
					$kecamatan = $query->row();
					$password = 12345678;
					$additional_data = array(
						'sekolah_id'	=> $loggeduser->sekolah_id,
						'nuptk'			=> $data->nuptk,
						'password_dapo'	=> md5($password),
						'created_at'	=> date('Y-m-d H:i:s'),
						'updated_at'	=> date('Y-m-d H:i:s'),
					);
					if($data_guru){
						$result = 'update';
						$class = 'table-danger"';
						$update_guru = array(
							//'guru_id'				=> $guru_id,
							//'sekolah_id' 			=> $sekolah_id,
							'sekolah_id'			=> $loggeduser->sekolah_id,
							//'user_id' 				=> $user_id,
							'nama' 					=> $data->nama,
							'nuptk' 				=> $data->nuptk,
							'nip' 					=> $data->nip,
							'nik' 					=> $data->nik,
							'jenis_kelamin' 		=> $data->jenis_kelamin,
							'tempat_lahir' 			=> $data->tempat_lahir,
							'tanggal_lahir' 		=> $data->tanggal_lahir,
							'status_kepegawaian_id'	=> $data->status_kepegawaian_id,
							'jenis_ptk_id' 			=> $data->jenis_ptk_id,
							'agama_id' 				=> $data->agama_id,
							'alamat' 				=> $data->alamat_jalan,
							'rt' 					=> ($data->rt) ? $data->rt : 0,
							'rw' 					=> ($data->rw) ? $data->rw : 0,
							'desa_kelurahan' 		=> $data->desa_kelurahan,
							'kecamatan' 			=> $kecamatan->nama,
							'kode_pos'				=> ($data->kode_pos) ? $data->kode_pos : 0,
							'no_hp'					=> ($data->no_hp) ? $data->no_hp : 0,
							'email' 				=> $data->email,
							'photo' 				=> '',
							'guru_id_dapodik' 		=> $data->ptk_id,
						);
						$this->guru->update($data_guru->guru_id, $update_guru);
						$update_user = array(
							'sekolah_id'	=> $loggeduser->sekolah_id,
							'guru_id'		=> $data_guru->guru_id,
							'email' 		=> $data->email,
							'nuptk'			=> $data->nuptk,
						);
						$this->user->update($data_guru->user_id, $update_user);
						$find_guru_aktif = $this->guru_terdaftar->find("guru_id = '$data_guru->guru_id' and semester_id = $ajaran->id");
						if($find_guru_aktif){
							$update_guru_aktif = array(
								'status' => 1
							);
							$this->guru_terdaftar->update($find_guru_aktif->guru_terdaftar_id, $update_guru_aktif);
						} else {
							$attributes = array('guru_terdaftar_id' => gen_uuid(), 'semester_id' => $ajaran->id, 'guru_id' => $data_guru->guru_id, 'status' => 1);			
							$guru_aktif = $this->guru_terdaftar->insert($attributes);
						}
						if($find_gelar){
							$find_gelar = array_unique($find_gelar, SORT_REGULAR);
							foreach($find_gelar as $gelar){
								if($gelar->gelar_akademik_id){
									$find_gelar_ptk = $this->gelar_ptk->find("ptk_id = '$data->ptk_id' AND gelar_akademik_id = $gelar->gelar_akademik_id");
									if($find_gelar_ptk){
										$this->gelar_ptk->delete($find_gelar_ptk->gelar_ptk_id);
									}
									$this->gelar_ptk->insert(array('gelar_ptk_id' => gen_uuid(), 'gelar_akademik_id' => $gelar->gelar_akademik_id, 'ptk_id' => $data->ptk_id, 'guru_id' => $data_guru->guru_id));
								}
							}
						}
					} else {
						//$find_user = $this->user->find_by_username($data->nama);
						$find_user = $this->user->find_by_email($data->email);
						if(!$find_user){
							$guru_id = gen_uuid();
							$group = array('3');
							$user_id = $this->ion_auth->register($data->nama, $password, $data->email, $additional_data, $group);
							if($user_id){
								$insert_guru = array(
									'guru_id'				=> $guru_id,
									//'sekolah_id' 			=> $sekolah_id,
									'sekolah_id'			=> $loggeduser->sekolah_id,
									'user_id' 				=> $user_id,
									'nama' 					=> $data->nama,
									'nuptk' 				=> $data->nuptk,
									'nip' 					=> $data->nip,
									'nik' 					=> $data->nik,
									'jenis_kelamin' 		=> $data->jenis_kelamin,
									'tempat_lahir' 			=> $data->tempat_lahir,
									'tanggal_lahir' 		=> $data->tanggal_lahir,
									'status_kepegawaian_id'	=> $data->status_kepegawaian_id,
									'jenis_ptk_id' 			=> $data->jenis_ptk_id,
									'agama_id' 				=> $data->agama_id,
									'alamat' 				=> $data->alamat_jalan,
									'rt' 					=> ($data->rt) ? $data->rt : 0,
									'rw' 					=> ($data->rw) ? $data->rw : 0,
									'desa_kelurahan' 		=> $data->desa_kelurahan,
									'kecamatan' 			=> $kecamatan->nama,
									'kode_pos'				=> ($data->kode_pos) ? $data->kode_pos : 0,
									'no_hp'					=> ($data->no_hp) ? $data->no_hp : 0,
									'email' 				=> $data->email,
									'photo' 				=> '',
									'guru_id_dapodik' 		=> $data->ptk_id,
								);
								$dataguru = $this->guru->insert($insert_guru);
								$find_guru_aktif = $this->guru_terdaftar->find("guru_id = '$guru_id' and semester_id = $ajaran->id");
								if($find_guru_aktif){
									$update_guru_aktif = array(
										'status' => 1
									);
									$this->guru_terdaftar->update($find_guru_aktif->guru_terdaftar_id, $update_guru_aktif);
								} else {
									$attributes = array('guru_terdaftar_id' => gen_uuid(), 'semester_id' => $ajaran->id, 'guru_id' => $guru_id, 'status' => 1);			
									$guru_aktif = $this->guru_terdaftar->insert($attributes);
								}
								$this->user->update($user_id, array('guru_id' => $guru_id));
								if($find_gelar){
									$find_gelar = array_unique($find_gelar, SORT_REGULAR);
									foreach($find_gelar as $gelar){
										if($gelar->gelar_akademik_id){
											$find_gelar_ptk = $this->gelar_ptk->find("ptk_id = '$data->ptk_id' AND gelar_akademik_id = $gelar->gelar_akademik_id");
											if(!$find_gelar_ptk){
												$this->gelar_ptk->insert(array('gelar_ptk_id' => gen_uuid(), 'gelar_akademik_id' => $gelar->gelar_akademik_id, 'ptk_id' => $data->ptk_id, 'guru_id' => $guru_id));
											}
										}
									}
								}
							}
							$result = 'insert';
							$class = 'table-success"';
						} else {
							$result = 'update';
							$class = 'table-danger"';
						}
					}
			?>
				<tr class="<?php echo $class; ?>">
					<td class="text-center"><?php echo $no++; ?></td>
					<td><?php echo $data->nama; ?></td>
					<td><?php echo $data->jenis_kelamin; ?></td>
					<td><?php echo $data->tempat_lahir; ?></td>
					<td><?php echo $data->tanggal_lahir; ?></td>
					<td><?php echo $data->nik; ?></td>
					<td><?php echo $data->nuptk; ?></td>
					<td><?php echo $data->email; ?></td>
					<td><?php echo $result; ?></td>
				</tr>
			<?php
			} ?>
			</tbody>
		</table>
    </div><!-- /.box-body -->
	<div class="box-footer text-center">
		<?php echo $pagination; ?>
	</div>
</div><!-- /.box -->
</div>
<script>
$(document).ready(function(){
	$('body').mouseover(function(){
		$(this).css({cursor: 'progress'});
	});
	var cari = $('body').find('.next');
	if(cari.length>0){
		var cari_a = $(cari).find('a');
		var url = $(cari_a).attr('href');
		window.location.replace(url);
	} else {
		window.location.replace('<?php echo site_url('admin/sinkronisasi'); ?>');
	}
})
</script>