<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="row">
<!-- left column -->
<div class="col-md-12">
<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title">Sinkronisasi <?php echo $page_title.' ('.$total_rows.' => '.$inserted. ')'; ?></h3>
	</div>
    <div class="box-body">
	<div class="text-center">
		<?php //echo $pagination; ?>
	</div>
	<table class="table table-bordered table-striped table-hover">
            <thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Nama</th>
					<th class="text-center">Tanggal Lahir</th>
					<th class="text-center">No Induk</th>
					<th class="text-center">NISN</th>
					<th class="text-center">status</th>
	            </tr>
            </thead>
			<tbody>
			<?php
				$no = $this->uri->segment('4') + 1;
				foreach($dapodik as $data){
					$data_sync = array(
						'peserta_didik_id'	=> $data->peserta_didik_id,
						'sekolah_id'		=> $loggeduser->sekolah_id,
					);
					$response = post_sync('diterima_kelas', $data_sync);
					$find_diterima_kelas = isset($response->data->nama) ? $response->data->nama : '-';
					$data->nisn = ($data->nisn) ? $data->nisn : GenerateNISN();
					$data->email = ($data->email) ? $data->email : GenerateEmail().'@erapor-smk.net';
					$data->email = ($data->email != $sekolah->email) ? $data->email : GenerateEmail().'@erapor-smk.net';
					$data->email = strtolower($data->email);
					//$sekolah = $this->sekolah->find_by_sekolah_id_dapodik($loggeduser->sekolah_id);
					//$sekolah_id = ($sekolah) ? $sekolah->sekolah_id : 0;
					$insert_siswa = array(
						//"sekolah_id"		=> $sekolah_id,
						'sekolah_id'		=> $loggeduser->sekolah_id,
						'nama' 				=> $data->nama_siswa,
						'no_induk' 			=> ($data->nipd) ? $data->nipd : 0,
						'nisn' 				=> $data->nisn,
						'jenis_kelamin' 	=> ($data->jenis_kelamin) ? $data->jenis_kelamin : 0,
						'tempat_lahir' 		=> ($data->tempat_lahir) ? $data->tempat_lahir : 0,
						'tanggal_lahir' 	=> $data->tanggal_lahir,
						'agama_id' 			=> ($data->agama_id) ? $data->agama_id : 0,
						'status' 			=> 'Anak Kandung',
						'anak_ke' 			=> ($data->anak_keberapa) ? $data->anak_keberapa : 0,
						'alamat' 			=> ($data->alamat_jalan) ? $data->alamat_jalan : 0,
						'rt' 				=> ($data->rt) ? $data->rt : 0,
						'rw' 				=> ($data->rw) ? $data->rw : 0,
						'desa_kelurahan' 	=> ($data->desa_kelurahan) ? $data->desa_kelurahan : 0,
						'kecamatan' 		=> ($data->kecamatan) ? $data->kecamatan : 0,
						'kode_pos' 			=> ($data->kode_pos) ? $data->kode_pos : 0,
						'no_telp' 			=> ($data->nomor_telepon_seluler) ? $data->nomor_telepon_seluler : 0,
						'sekolah_asal' 		=> ($data->sekolah_asal) ? $data->sekolah_asal : 0,
						'diterima_kelas' 	=> ($find_diterima_kelas) ? $find_diterima_kelas : 0,
						'diterima' 			=> ($data->tanggal_masuk_sekolah) ? $data->tanggal_masuk_sekolah : 0,
						//'email' 			=> $data->email,
						'email' 			=> ($data->email) ? $data->email : GenerateEmail().'@erapor-smk.net',
						'nama_ayah' 		=> ($data->nama_ayah) ? $data->nama_ayah : 0,
						'nama_ibu' 			=> ($data->nama_ibu_kandung) ? $data->nama_ibu_kandung : 0,
						'kerja_ayah' 		=> ($data->pekerjaan_id_ayah) ? $data->pekerjaan_id_ayah : 0,
						'kerja_ibu' 		=> ($data->pekerjaan_id_ibu) ? $data->pekerjaan_id_ibu : 0,
						'nama_wali' 		=> ($data->nama_wali) ? $data->nama_wali : 0,
						'alamat_wali' 		=> ($data->alamat_jalan) ? $data->alamat_jalan : 0,
						'telp_wali' 		=> ($data->nomor_telepon_seluler) ? $data->nomor_telepon_seluler : 0,
						'kerja_wali' 		=> ($data->pekerjaan_id_wali) ? $data->pekerjaan_id_wali : 0,
						'photo' 			=> 0,
						'active' 			=> 1,
						//'password_dapo' 	=> md5(12345678),
						//'petugas' 			=> $loggeduser->username,
						'siswa_id_dapodik' 	=> $data->peserta_didik_id,
					);
					$password = 12345678;
					$additional_data = array(
						"sekolah_id"		=> $loggeduser->sekolah_id,
						"nisn"				=> $insert_siswa['nisn'],
						'password_dapo'		=> md5($password),
						'created_at'		=> date('Y-m-d H:i:s'),
						'updated_at'		=> date('Y-m-d H:i:s'),
					);
					$find_rombel = $this->rombongan_belajar->find_by_rombel_id_dapodik($data->rombongan_belajar_id);
					$rombongan_belajar_id = ($find_rombel) ? $find_rombel->rombongan_belajar_id : gen_uuid();
					$find_data_siswa = $this->siswa->find_by_siswa_id_dapodik($data->peserta_didik_id);
					if($find_data_siswa){
						$update_user = array(
							'sekolah_id'	=> $loggeduser->sekolah_id,
							'nisn'			=> $data->nisn,
							'email'			=> $data->email,
						);
						$find_user = $this->user->find_by_siswa_id($find_data_siswa->siswa_id);
						if(!$find_user){
							$group = array('4');
							$user_id = $this->ion_auth->register(trim($data->nama_siswa), $password, $insert_siswa['email'], $additional_data, $group);
						} else {
							$this->ion_auth->update($find_data_siswa->user_id, $update_user);
						}
						$insert_siswa['user_id'] = $find_data_siswa->user_id;
						$this->siswa->update($find_data_siswa->siswa_id, $insert_siswa);
						$attributes_update_anggota_rombel = array(
							'semester_id' 				=> $ajaran->id, 
							'rombongan_belajar_id' 		=> $rombongan_belajar_id, 
							'siswa_id' 					=> $find_data_siswa->siswa_id,
							'anggota_rombel_id_dapodik'	=> $data->anggota_rombel_id,
						);
						$find_anggota_rombel = $this->anggota_rombel->find("semester_id = $ajaran->id AND siswa_id = '$find_data_siswa->siswa_id'");
						if($find_anggota_rombel){
							$this->anggota_rombel->update($find_anggota_rombel->anggota_rombel_id, $attributes_update_anggota_rombel);
							$result = 'update';
							$class = 'table-danger"';
						} else {
							$attributes_update_anggota_rombel['anggota_rombel_id'] = gen_uuid();
							$this->anggota_rombel->insert($attributes_update_anggota_rombel);
							$result = 'insert';
							$class = 'table-warning"';
						}
					} else {
						$group = array('4');
						$user_id = $this->ion_auth->register(trim($data->nama_siswa), $password, $insert_siswa['email'], $additional_data, $group);		
						if($user_id){
							$insert_siswa['user_id'] = $user_id;
							$insert_siswa['siswa_id'] = gen_uuid();
							$datasiswa = $this->siswa->insert($insert_siswa);
							$attributes_insert_anggota_rombel = array(
								'semester_id' 				=> $ajaran->id, 
								'rombongan_belajar_id' 		=> $rombongan_belajar_id, 
								'siswa_id' 					=> $insert_siswa['siswa_id'],
								'anggota_rombel_id_dapodik'	=> $data->anggota_rombel_id,
							);
							$find_anggota_rombel = $this->anggota_rombel->find("semester_id = $ajaran->id AND anggota_rombel_id_dapodik = '$data->anggota_rombel_id'");
							if($find_anggota_rombel){
								$this->anggota_rombel->update($find_anggota_rombel->anggota_rombel_id, $attributes_insert_anggota_rombel);
							} else {
								$attributes_insert_anggota_rombel['anggota_rombel_id'] = gen_uuid();
								$this->anggota_rombel->insert($attributes_insert_anggota_rombel);
							}
							//$anggota = $this->anggota_rombel->insert($attributes_insert_anggota_rombel);
							$this->user->update($user_id, array('siswa_id' => $insert_siswa['siswa_id']));
							}
						$result = 'insert';
						$class = 'table-success"';
					}
					//if(!$find_data_siswa){
			?>
				<tr class="<?php echo $class; ?>">
					<td class="text-center"><?php echo $no++; ?></td>
					<td><?php echo $data->nama_siswa; ?></td>
					<td class="text-center"><?php echo $data->tanggal_lahir; ?></td>
					<td class="text-center"><?php echo $data->nipd; ?></td>
					<td class="text-center"><?php echo $data->nisn; ?></td>
					<td class="text-center"><?php echo $result; ?></td>
				</tr>
			<?php
			//}
			//break; 
			} ?>
			</tbody>
		</table>
    </div><!-- /.box-body -->
	<div class="box-footer text-center">
		<?php echo $pagination; ?>
	</div>
</div><!-- /.box -->
</div>
<script>
$(document).ready(function(){
	$('body').mouseover(function(){
		$(this).css({cursor: 'wait'});
	});
	var cari = $('body').find('.next');
	if(cari.length>0){
		var cari_a = $(cari).find('a');
		var url = $(cari_a).attr('href');
		window.location.replace(url);
	} else {
		window.location.replace('<?php echo site_url('admin/sinkronisasi'); ?>');
	}
})
</script>