<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//$session = $this->session->userdata();
ini_set('display_errors', 0);
?><!DOCTYPE html>
<html>
<head>
  <title>Install eRaporSMK</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/css/ionicons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
</head>
<body class="hold-transition login-page">
		<div class="login-box">
        <div class="login-logo">
	<img src="<?php echo base_url(); ?>assets/img/logo.png" alt="logo" class="text-center" width="100" />
	<!--a href="<?php echo base_url(); ?>"><b>e-Rapor SMK</b></a-->
</div>
<div class="login-box-body">
	<p class="login-box-msg"><a href="<?php echo base_url(); ?>"><b>Install e-Rapor SMK</b></a></p>
		<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
    	<?php echo isset($error) ? error_msg($error) : ''; ?>
    	<?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
        <form action="<?php echo site_url('install'); ?>" method="post">
        <div class="form-group has-feedback">
			<input type="text" name="hostname" class="form-control" placeholder="Hostname (e.g localhost)"/>
			<span class="glyphicon glyphicon-ok form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="text" name="username" class="form-control" placeholder="Database Username"/>
			<span class="glyphicon glyphicon-ok form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="text" name="password" class="form-control" placeholder="Database Password"/>
			<span class="glyphicon glyphicon-ok form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="text" name="database" class="form-control" placeholder="Database Name"/>
			<span class="glyphicon glyphicon-ok form-control-feedback"></span>
		</div>
		<button type="submit" class="btn btn-primary btn-block btn-flat">Install</button>
            </form>
</div>		</div>
        <!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>    </body>
</html>