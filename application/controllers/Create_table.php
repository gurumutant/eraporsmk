<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Create_table extends CI_Controller {
	public function __construct() {
		parent::__construct();
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit', '-1'); 
	}
	public function index($table){
		if($table == 'mata_pelajaran'){
			$sql = "CREATE TABLE mata_pelajaran (
					mata_pelajaran_id integer NOT NULL,
					nama varchar(80) NOT NULL,
					pilihan_sekolah numeric(1,0) NOT NULL,
					pilihan_buku numeric(1,0) NOT NULL,
					pilihan_kepengawasan numeric(1,0) NOT NULL,
					pilihan_evaluasi numeric(1,0) NOT NULL,
					jurusan_id varchar(25),
					created_at datetime NOT NULL,
					updated_at datetime NOT NULL,
					deleted_at datetime,
					last_sync datetime NOT NULL,
					CONSTRAINT mata_pelajaran_pkey PRIMARY KEY (mata_pelajaran_id),
					CONSTRAINT mata_pelajaran_jurusan_id_fkey FOREIGN KEY (jurusan_id)
						REFERENCES ref_jurusan (jurusan_id) MATCH SIMPLE
						ON UPDATE NO ACTION ON DELETE NO ACTION
				)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='referensi mata_pelajaran'";
			$this->db->query($sql);
			$this->db->select('*');
			$this->db->from('mata_pelajaran');
			$this->db->where('mata_pelajaran_id',1);
			$query = $this->db->get();
			$result = $query->row();
			if(!$result){
				include_once APPPATH."/migrations/referensi/mata_pelajaran.php";
				$this->db->insert_batch('mata_pelajaran', $mata_pelajaran);
			}
			redirect('');
		} elseif($table == 'mst_wilayah'){
			$sql = "CREATE TABLE mst_wilayah (
				kode_wilayah varchar(8) NOT NULL,
				nama varchar(60) NOT NULL,
				id_level_wilayah smallint NOT NULL,
				mst_kode_wilayah varchar(8),
				negara_id varchar(2) NOT NULL,
				asal_wilayah varchar(8),
				kode_bps varchar(7),
				kode_dagri varchar(7),
				kode_keu varchar(10),
				created_at datetime NOT NULL,
				updated_at datetime NOT NULL,
				deleted_at datetime,
				last_sync datetime NOT NULL,
				CONSTRAINT mst_wilayah_pkey PRIMARY KEY (kode_wilayah),
				CONSTRAINT mst_wilayah_id_level_wilayah_fkey FOREIGN KEY (id_level_wilayah)
					REFERENCES level_wilayah (id_level_wilayah) MATCH SIMPLE
					ON UPDATE NO ACTION ON DELETE NO ACTION,
				CONSTRAINT mst_wilayah_mst_kode_wilayah_fkey FOREIGN KEY (mst_kode_wilayah)
					REFERENCES mst_wilayah (kode_wilayah) MATCH SIMPLE
					ON UPDATE NO ACTION ON DELETE NO ACTION,
				CONSTRAINT mst_wilayah_negara_id_fkey FOREIGN KEY (negara_id)
					REFERENCES negara (negara_id) MATCH SIMPLE
					ON UPDATE NO ACTION ON DELETE NO ACTION
				)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='referensi mst_wilayah'";
			$this->db->query($sql);
			$kurikulum = array('000000', '030817AN', '050911AA', '060713AB', '072605AK', '111011AN', '150918AD', '196204AG', '241601AI', '270721');
			foreach($kurikulum as $key => $kur){
				$this->db->select('*');
				$this->db->from('mst_wilayah');
				$this->db->where('kode_wilayah',$kur);
				$query = $this->db->get();
				$result = $query->row();
				if(!$result){
					include_once APPPATH."/migrations/referensi/mst_wilayah($key).php";
					$this->db->insert_batch('mst_wilayah', $mst_wilayah);
				}
				sleep(1);
			}
			redirect('admin/sinkronisasi');
		} elseif($table == 'mata_pelajaran_kurikulum'){
			$sql = "CREATE TABLE mata_pelajaran_kurikulum (
					kurikulum_id smallint NOT NULL,
					mata_pelajaran_id integer NOT NULL,
					tingkat_pendidikan_id numeric(2,0) NOT NULL,
					jumlah_jam numeric(2,0) NOT NULL,
					jumlah_jam_maksimum numeric(2,0) NOT NULL,
					wajib numeric(1,0) NOT NULL,
					sks numeric(2,0) NOT NULL,
					a_peminatan numeric(1,0) NOT NULL,
					area_kompetensi varchar(1) NOT NULL,
					gmp_id varchar(36),
					created_at datetime NOT NULL,
					updated_at datetime NOT NULL,
					deleted_at datetime,
					last_sync datetime NOT NULL,
					CONSTRAINT mata_pelajaran_kurikulum_pkey PRIMARY KEY (kurikulum_id, mata_pelajaran_id, tingkat_pendidikan_id),
					CONSTRAINT mata_pelajaran_kurikulum_kurikulum_id_fkey FOREIGN KEY (kurikulum_id)
						REFERENCES ref_kurikulum (kurikulum_id) MATCH SIMPLE
						ON UPDATE NO ACTION ON DELETE NO ACTION,
					CONSTRAINT mata_pelajaran_kurikulum_mata_pelajaran_id_fkey FOREIGN KEY (mata_pelajaran_id)
						REFERENCES mata_pelajaran (mata_pelajaran_id) MATCH SIMPLE
						ON UPDATE NO ACTION ON DELETE NO ACTION,
					CONSTRAINT mata_pelajaran_kurikulum_tingkat_pendidikan_id_fkey FOREIGN KEY (tingkat_pendidikan_id)
						REFERENCES ref_tingkat_pendidikan (tingkat_pendidikan_id) MATCH SIMPLE
						ON UPDATE NO ACTION ON DELETE NO ACTION
				)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='referensi mata_pelajaran_kurikulum'";
			$this->db->query($sql);
			$kurikulum = array(
				1 => array(
					'kurikulum_id' 			=> 1,
					'mata_pelajaran_id'		=> 1100,
					'tingkat_pendidikan_id'	=> 1
				),
				2 => array(
					'kurikulum_id' 			=> 175,
					'mata_pelajaran_id'		=> 401231000,
					'tingkat_pendidikan_id'	=> 11
				),
				3 => array(
					'kurikulum_id' 			=> 555,
					'mata_pelajaran_id'		=> 100015000,
					'tingkat_pendidikan_id'	=> 10
				),
				4 => array(
					'kurikulum_id' 			=> 142,
					'mata_pelajaran_id'		=> 300311900,
					'tingkat_pendidikan_id'	=> 10
				),
				5 => array(
					'kurikulum_id' 			=> 356,
					'mata_pelajaran_id'		=> 300311900,
					'tingkat_pendidikan_id'	=> 11
				),
			);
			foreach($kurikulum as $key => $kur){
				$this->db->select('*');
				$this->db->from('mata_pelajaran_kurikulum');
				$this->db->where('kurikulum_id',$kur['kurikulum_id']);
				$this->db->where('mata_pelajaran_id',$kur['mata_pelajaran_id']);
				$this->db->where('tingkat_pendidikan_id',$kur['tingkat_pendidikan_id']);
				$query = $this->db->get();
				$result = $query->row();
				if(!$result){
					include_once APPPATH."/migrations/referensi/mata_pelajaran_kurikulum($key).php";
					$this->db->insert_batch('mata_pelajaran_kurikulum', $mata_pelajaran_kurikulum);
				}
				sleep(1);
			}
			redirect('admin/dashboard');
		} elseif($table == 'menu'){
			$this->load->dbforge();
			$fields = array(
				'id' => array(
					'type' => 'INT',
					'constraint' => 11,
					'unsigned' => TRUE,
					'auto_increment' => TRUE
				),
				'group_id' => array(
					'type' => 'INT',
					'constraint' => 11,
					'default' => NULL
				),
				'judul_menu' => array(
					'type' => 'VARCHAR',
					'constraint' => 50,
					'default' => NULL
				),
				'link' => array(
					'type' => 'VARCHAR',
					'constraint' => 50,
					'default' => NULL
				),
				'icon' => array(
					'type' => 'VARCHAR',
					'constraint' => 20,
					'default' => NULL
				),
				'is_main_menu' => array(
					'type' => 'INT',
					'constraint' => 11,
					'default' => NULL
				),
			);
			$this->dbforge->add_field($fields);
			$this->dbforge->add_key('id', TRUE);
			$this->dbforge->create_table('menu',TRUE);
			redirect(''); 
		}
	}
}
