<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Install extends CI_Controller {
	//public function __construct(){
		//parent::__construct();
	//}
	// redirect if needed, otherwise display the user list
	// create a new user
	public function index(){
		$this->load->library('form_validation');
        $this->form_validation->set_rules('hostname', 'Hostname', 'required');
        $this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('database', 'database', 'required');
		if ($this->form_validation->run() == true){
            $data_sync = array(
				'hostname'	=> $this->input->post('hostname'),
				'username'	=> $this->input->post('username'),
				'password'	=> $this->input->post('password'),
				'database'	=> $this->input->post('database'),
			);
			$response = $this->database_install($data_sync);
			//test($response);
        } else {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			if(isset($response)){
				if($response->post_login){
					$this->data['success'] = (validation_errors()) ? validation_errors() : $response->message;
				} else {
					$this->data['error'] = (validation_errors()) ? validation_errors() : $response->message;
				}
			} else {
				$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			}
            $this->data['hostname'] = array(
                'name'  => 'hostname',
                'id'    => 'hostname',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('hostname'),
            );
            $this->data['username'] = array(
                'name'  => 'username',
                'id'    => 'username',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('username'),
            );
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['database'] = array(
                'name'  => 'database',
                'id'    => 'database',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('database'),
            );
			$this->load->view('install_form', $this->data);
            //$this->template->title('eRapor SMK Bisa')
	        //->set_layout($this->auth_tpl)
	        //->set('page_title', 'Register')
	        //->build($this->auth_folder.'/register', $this->data);
        }
    }
	public function database_install($data) {
		$this->load->dbutil();
		$this->load->dbforge();
		$hostname = $data['hostname'];
		$username = $data['username'];
		$password = $data['password'];
		$database = $data['database'];
		// Config path
		//include_once APPPATH."/migrations/referensi/mata_pelajaran_kurikulum($key).php";
		$template_path 	= 'application/config/database_default.php';
		$output_path 	= 'application/config/database.php';
		// Open the file
		//echo $template_path;
		$database_dapodik = file_get_contents($template_path);
		//echo $database_dapodik;
		//test($data);
		//die();
		if($password == 0){
			$password = '';
		}
		$new  = str_replace("%HOSTNAME%",$hostname,$database_dapodik);
		$new  = str_replace("%USERNAME%",$username,$new);
		$new  = str_replace("%PASSWORD%",$password,$new);
		$new  = str_replace("%DATABASE%",$database,$new);
		// Write the new database.php file
		$handle = fopen($output_path,'w+');

		// Chmod the file, in case the user forgot
		@chmod($output_path,0755);

		// Verify file permissions
		if(is_writable($output_path)) {
			// Write the file
			if(fwrite($handle,$new)) {
				//return true;
				if (!$this->dbutil->database_exists($database)){
					$this->create_database($data);
				} else {
					redirect(site_url());
				}
				/*if($this->ion_auth->logged_in()){
					redirect('admin/auth/logout');
				} else {
					redirect('admin/auth/');
				}*/
				//redirect('admin/dashboard');
			} else {
				//return false;
				echo 'The database configuration file could not be written, please chmod application/config/database.php file to 777';
			}

		} else {
			//return false;
			echo 'The database configuration file could not be written, please chmod application/config/database.php file to 777';
		}
	}
	function create_database($data){
		$response  = array();
		// Connect to the database
		$mysqli = new mysqli($data['hostname'],$data['username'],$data['password'],'');
		// Check for errors
		if($mysqli->connect_errno){
			echo 'Failed to connect to MySQL : '. $mysqli->connect_error;
		} else  if(!$mysqli->query("CREATE DATABASE IF NOT EXISTS ".$data['database'])){
			echo "Database Error : Database <b>".$data['database']."</b> does not exist and could not be created. Please create the Database manually and retry installing.";
		} else {
			redirect(site_url());
		}
		// Close the connection
		$mysqli->close();
	}
	public function write_autoload(){
		// Config path
		//$template_path 	= 'config/autoload.php';
		//$output_path 	= '../application/config/autoload.php';
		$template_path 	= 'application/config/autoload_default.php';
		$output_path 	= 'application/config/autoload.php';
		// Open the file
		$config_file = file_get_contents($template_path);
		// Write the new index.php file
		$handle = fopen($output_path,'w+');
		// Chmod the file, in case the user forgot
		@chmod($output_path,0755);
		// Verify file permissions
		if(is_writable($output_path)) {
			// Write the file
			if(fwrite($handle,$config_file)) {
				redirect(site_url());
			} else {
				echo "The autoload file could not be written, please chmod install/config/autoload.php file to 777";
			}
		} else {
			echo "The autoload file could not be written, please chmod install/config/autoload.php file to 777";
		}	
	}
	function write_htaccess(){
		// Config path
		$template_path 	= 'assets/files/.htaccess';
		$output_path 	= '.htaccess';
		// Open the file
		$config_file = file_get_contents($template_path);
		$root = $_SERVER["REQUEST_URI"];
		$parse = parse_url($root);
		$subfolder = str_replace('index.php/install/write_htaccess','',$parse['path']);
		$new  = str_replace("%rewrite_base%","RewriteBase ".$subfolder, $config_file);
		// Write the new index.php file
		$handle = fopen($output_path,'w+');
		// Chmod the file, in case the user forgot
		@chmod($output_path,0755);
		// Verify file permissions
		if(is_writable($output_path)) {
			// Write the file
			if(fwrite($handle,$new)) {
				redirect(site_url());
			} else {
				echo "The htaccess file could not be written, please chmod assets/files/.htaccess file to 777";
			}
		} else {
			echo "The htaccess file could not be written, please chmod .htaccess file to 777";
		}	
	}
	function update_core(){
		// Config path
		//$template_path 	= 'config/autoload.php';
		//$output_path 	= '../application/config/autoload.php';
		$template_path 	= 'assets/temp/MY_Controller.php';
		$output_path 	= 'application/core/MY_Controller.php';
		// Open the file
		$config_file = file_get_contents($template_path);
		// Write the new index.php file
		$handle = fopen($output_path,'w+');
		// Chmod the file, in case the user forgot
		@chmod($output_path,0755);
		// Verify file permissions
		if(is_writable($output_path)) {
			// Write the file
			if(fwrite($handle,$config_file)) {
				redirect(site_url());
			} else {
				echo "The autoload file could not be written, please chmod assets/temp/MY_Controller.php file to 777";
			}
		} else {
			echo "The autoload file could not be written, please chmod application/core/MY_Controller.php file to 777";
		}	
	}
}
