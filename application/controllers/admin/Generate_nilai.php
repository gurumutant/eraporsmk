<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Generate_nilai extends Backend_Controller {
	protected $activemenu = 'generate_nilai';
	public function __construct(){
		parent::__construct();
		$this->template->set('activemenu', $this->activemenu);
		$this->load->model('nilai_akhir_model','nilai_akhir');
	}

	public function index(){
		$user = $this->ion_auth->user()->row();
		$sekolah = $this->sekolah->find_by_sekolah_id_dapodik($user->sekolah_id);
		$sekolah_id = ($sekolah) ? $sekolah->id : 0;
		$this->template->title('Administrator Panel : Generate Nilai')
        ->set_layout($this->admin_tpl)
        ->set('page_title', 'Generate Nilai Akhir')
        ->set('sekolah_id', $sekolah_id)
        ->build($this->admin_folder.'/tools/generate_nilai');
		//->build($this->admin_folder.'/perbaikan');
	}
	public function list_generate_nilai($kompetensi = NULL, $tingkat = NULL, $rombel = NULL){
		$semester = get_ta();
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah_id = get_sekolah_id($loggeduser->sekolah_id);
		$search = "";
		$start = 0;
		$rows = 25;
		$ajaran = get_ta();
		// get search value (if any)
		if (isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$search = $_GET['sSearch'];
		}

		// limit
		$start = get_start();
		$rows = get_rows();
		//$where = "rombongan_belajar_id IN (SELECT id FROM rombongan_belajar WHERE sekolah_id = $sekolah_id AND semester_id = $semester->id AND deleted_at IS NULL)";
		$where = "rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE sekolah_id = '$sekolah_id' AND semester_id = $semester->id AND deleted_at IS NULL) AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM nilai) AND mata_pelajaran_id IN (SELECT mata_pelajaran_id FROM nilai)";
		$search_from = "mata_pelajaran_id IN (SELECT mata_pelajaran_id FROM pembelajaran WHERE $where AND nama_mata_pelajaran LIKE '%$search%') OR guru_id IN (SELECT id FROM ref_guru WHERE nama LIKE '%$search%')";
		if($kompetensi && !$tingkat && !$rombel){
			$set_query = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $kompetensi) AND";
		} elseif($kompetensi && $tingkat && !$rombel){
			$set_query = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $kompetensi AND tingkat = $tingkat) AND";
		} elseif($kompetensi && $tingkat && $rombel){
			$set_query = "AND rombongan_belajar_id = '$rombel' AND";
		} else {
			$set_query = 'AND';
		}
		$query = $this->pembelajaran->find_all("$where $set_query ($search_from)", '*','rombongan_belajar_id asc, mata_pelajaran_id asc', $start, $rows);
		$filter = $this->pembelajaran->find_count("$where $set_query ($search_from)");
		$iFilteredTotal = count($query);
		
		$iTotal= $filter;
	    
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iTotal,
	        "aaData" => array()
	    );

	    // get result after running query and put it in array
		$i=$start;
		//$barang = $query->result();
	    foreach ($query as $temp) {
			//test($temp);
			//die();
			$class_pengetahuan = 'btn-success btn_generate';
			$class_keterampilan = 'btn-success btn_generate';
			$status_pengetahuan = 'Proses';
			$status_keterampilan = 'Proses';
			$nilai_akhir_pengetahuan = $this->nilai_akhir->find("semester_id = $ajaran->id AND kompetensi_id = 1 AND rombongan_belajar_id = '$temp->rombongan_belajar_id' AND mata_pelajaran_id = $temp->mata_pelajaran_id");
			$nilai_akhir_keterampilan = $this->nilai_akhir->find("semester_id = $ajaran->id AND kompetensi_id = 2 AND rombongan_belajar_id = '$temp->rombongan_belajar_id' AND mata_pelajaran_id = $temp->mata_pelajaran_id");
			if($nilai_akhir_pengetahuan){
				$class_pengetahuan = 'btn-danger';
				$status_pengetahuan = 'Update';
			}
			if($nilai_akhir_keterampilan){
				$class_keterampilan = 'btn-danger';
				$status_keterampilan = 'Update';
			}
			$record = array();
			$record[] = $temp->nama_mata_pelajaran.' ('.$temp->mata_pelajaran_id.')';
			$record[] = get_nama_guru($temp->guru_id);
			$record[] = '<div class="text-center">'.get_nama_rombel($temp->rombongan_belajar_id).'</div>';
			$record[] = '<div class="text-center"><a href="'.site_url('admin/generate_nilai/proses/1/'.$temp->rombongan_belajar_id.'/'.$temp->mata_pelajaran_id.'/'.$temp->pembelajaran_id).'" class="btn btn-sm '.$class_pengetahuan.' btn-sm"><i class="fa fa-check-square-o"></i> '.$status_pengetahuan.' Nilai Pengetahuan</a></div>';
			$record[] = '<div class="text-center"><a href="'.site_url('admin/generate_nilai/proses/2/'.$temp->rombongan_belajar_id.'/'.$temp->mata_pelajaran_id.'/'.$temp->pembelajaran_id).'" class="btn btn-sm '.$class_keterampilan.' btn-sm"><i class="fa fa-check-square-o"></i> '.$status_keterampilan.' Nilai Keterampilan</a></div>';
			$output['aaData'][] = $record;
		}
		if($kompetensi && $tingkat){
			$get_all_rombel = $this->rombongan_belajar->find_all("jurusan_id = $kompetensi AND semester_id = $ajaran->id AND sekolah_id_dapodik = '$loggeduser->sekolah_id' AND tingkat = $tingkat");
			//Datarombel::find_all_by_kurikulum_id_and_tingkat_and_ajaran_id($jurusan, $tingkat, $ajaran->id);
			foreach($get_all_rombel as $allrombel){
				$all_rombel= array();
				$all_rombel['value'] = $allrombel->id;
				$all_rombel['text'] = $allrombel->nama;
				$output['rombel'][] = $all_rombel;
			}
		}
		// format it to JSON, this output will be displayed in datatable
		echo json_encode($output);
	}
	public function proses($kompetensi_id, $rombongan_belajar_id, $mata_pelajaran_id, $pembelajaran_id){
		$ajaran = get_ta();
		$get_siswa = get_siswa_by_rombel($rombongan_belajar_id);
		$a=0;
		$i=0;
		foreach($get_siswa['data'] as $siswa){
			$nilai_siswa = get_nilai_siswa($ajaran->id, $kompetensi_id, $rombongan_belajar_id, $mata_pelajaran_id, $siswa->siswa_id);
			$find_nilai_akhir = $this->nilai_akhir->find("semester_id = $ajaran->id AND kompetensi_id = $kompetensi_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $mata_pelajaran_id AND siswa_id = '$siswa->siswa_id'");
			if($nilai_siswa){
				if($find_nilai_akhir){
					$a++;
					$find_nilai_akhir = $this->nilai_akhir->update($find_nilai_akhir->nilai_akhir_id, array('nilai' => $nilai_siswa, 'pembelajaran_id' => $pembelajaran_id));
				} else {
					$i++;
					$insert_nilai_akhir = array(
						'nilai_akhir_id'		=> gen_uuid(),
						'semester_id'			=> $ajaran->id,
						'kompetensi_id'			=> $kompetensi_id,
						'rombongan_belajar_id'	=> $rombongan_belajar_id,
						'mata_pelajaran_id'		=> $mata_pelajaran_id,
						'siswa_id'				=> $siswa->siswa_id,
						'nilai'					=> $nilai_siswa,
						'pembelajaran_id'		=> $pembelajaran_id,
					);
					$this->nilai_akhir->insert($insert_nilai_akhir);
				}
			} else {
				if($find_nilai_akhir){
					$this->nilai_akhir->update($find_nilai_akhir->nilai_akhir_id, array('nilai' => 0, 'pembelajaran_id' => $pembelajaran_id));
				}
			}
		}
		$this->session->set_flashdata('success', "$i siswa berhasil disimpan. $a siswa berhasil diperbaharui");
		redirect("admin/generate_nilai", 'refresh');
	}
	public function proses_walas($kompetensi_id, $rombongan_belajar_id, $mata_pelajaran_id, $pembelajaran_id){
		$ajaran = get_ta();
		$get_siswa = get_siswa_by_rombel($rombongan_belajar_id);
		$a=0;
		$i=0;
		foreach($get_siswa['data'] as $siswa){
			$nilai_siswa = get_nilai_siswa($ajaran->id, $kompetensi_id, $rombongan_belajar_id, $mata_pelajaran_id, $siswa->siswa_id);
			$find_nilai_akhir = $this->nilai_akhir->find("semester_id = $ajaran->id AND kompetensi_id = $kompetensi_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $mata_pelajaran_id AND siswa_id = '$siswa->siswa_id'");
			if($nilai_siswa){
				if($find_nilai_akhir){
					$a++;
					$find_nilai_akhir = $this->nilai_akhir->update($find_nilai_akhir->nilai_akhir_id, array('nilai' => $nilai_siswa, 'pembelajaran_id' => $pembelajaran_id));
				} else {
					$i++;
					$insert_nilai_akhir = array(
						'nilai_akhir_id'		=> gen_uuid(),
						'semester_id'			=> $ajaran->id,
						'kompetensi_id'			=> $kompetensi_id,
						'rombongan_belajar_id'	=> $rombongan_belajar_id,
						'mata_pelajaran_id'		=> $mata_pelajaran_id,
						'siswa_id'				=> $siswa->siswa_id,
						'nilai'					=> $nilai_siswa,
						'pembelajaran_id'		=> $pembelajaran_id,
					);
					$this->nilai_akhir->insert($insert_nilai_akhir);
				}
			} else {
				if($find_nilai_akhir){
					$this->nilai_akhir->update($find_nilai_akhir->nilai_akhir_id, array('nilai' => 0, 'pembelajaran_id' => $pembelajaran_id));
				}
			}
		}
		//$this->session->set_flashdata('success', "$i siswa berhasil disimpan. $a siswa berhasil diperbaharui");
		//redirect("admin/dashboard#pembelajaran", 'refresh');
		$status['type'] = 'success';
		$status['text'] = "$i siswa berhasil disimpan. $a siswa berhasil diperbaharui";
		$status['title'] = 'Generate Nilai Selesai!';
		$status['redirect'] = site_url("admin/dashboard");
		echo json_encode($status);
	}
	public function proses_guru($kompetensi_id, $rombongan_belajar_id, $mata_pelajaran_id, $pembelajaran_id){
		$ajaran = get_ta();
		$get_siswa = get_siswa_by_rombel($rombongan_belajar_id);
		$a=0;
		$i=0;
		/*
		foreach($get_siswa['data'] as $siswa){
			$nilai_siswa = get_nilai_siswa($ajaran->id, $kompetensi_id, $rombongan_belajar_id, $mata_pelajaran_id, $siswa->siswa_id);
			$find_nilai_akhir = $this->nilai_akhir->find("semester_id = $ajaran->id AND kompetensi_id = $kompetensi_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $mata_pelajaran_id AND siswa_id = '$siswa->siswa_id'");
			if($nilai_siswa){
				if($find_nilai_akhir){
					$a++;
					$find_nilai_akhir = $this->nilai_akhir->update($find_nilai_akhir->nilai_akhir_id, array('nilai' => $nilai_siswa, 'pembelajaran_id' => $pembelajaran_id));
				} else {
					$i++;
					$insert_nilai_akhir = array(
						'nilai_akhir_id'		=> gen_uuid(),
						'semester_id'			=> $ajaran->id,
						'kompetensi_id'			=> $kompetensi_id,
						'rombongan_belajar_id'	=> $rombongan_belajar_id,
						'mata_pelajaran_id'		=> $mata_pelajaran_id,
						'siswa_id'				=> $siswa->siswa_id,
						'nilai'					=> $nilai_siswa,
						'pembelajaran_id'		=> $pembelajaran_id,
					);
					$this->nilai_akhir->insert($insert_nilai_akhir);
				}
			} else {
				if($find_nilai_akhir){
					$this->nilai_akhir->update($find_nilai_akhir->nilai_akhir_id, array('nilai' => 0, 'pembelajaran_id' => $pembelajaran_id));
				}
			}
		}
		*/
		foreach($get_siswa['data'] as $siswa){
			$nilai_siswa = get_nilai_siswa($ajaran->id, $kompetensi_id, $rombongan_belajar_id, $mata_pelajaran_id, $siswa->siswa_id);
			if($nilai_siswa){
				$find_nilai_akhir = $this->nilai_akhir->find("semester_id = $ajaran->id AND kompetensi_id = $kompetensi_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $mata_pelajaran_id AND siswa_id = '$siswa->siswa_id'");
				if($find_nilai_akhir){
					$a++;
					$find_nilai_akhir = $this->nilai_akhir->update($find_nilai_akhir->nilai_akhir_id, array('nilai' => $nilai_siswa, 'pembelajaran_id' => $pembelajaran_id));
				} else {
					$i++;
					$insert_nilai_akhir = array(
						'nilai_akhir_id'		=> gen_uuid(),
						'semester_id'			=> $ajaran->id,
						'kompetensi_id'			=> $kompetensi_id,
						'rombongan_belajar_id'	=> $rombongan_belajar_id,
						'mata_pelajaran_id'		=> $mata_pelajaran_id,
						'siswa_id'				=> $siswa->siswa_id,
						'nilai'					=> $nilai_siswa,
						'pembelajaran_id'		=> $pembelajaran_id,
					);
					$this->nilai_akhir->insert($insert_nilai_akhir);
				}
			}
		}
		$status['type'] = 'success';
		$status['text'] = "$i siswa berhasil disimpan. $a siswa berhasil diperbaharui";
		$status['title'] = 'Generate Nilai Selesai!';
		$status['redirect'] = site_url("admin/dashboard");
		echo json_encode($status);
		//$this->session->set_flashdata('success', "$i siswa berhasil disimpan. $a siswa berhasil diperbaharui");
		//redirect("admin/dashboard", 'refresh');
	}
}
