<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Laporan extends Backend_Controller {
	protected $activemenu = 'laporan';
	public function __construct() {
		parent::__construct();
		$this->template->set('activemenu', $this->activemenu);
	}
	public function index(){
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Laporan')
		->build($this->admin_folder.'/perbaikan');
	}
	public function catatan_wali_kelas(){
		$ajaran = get_ta();
		$loggeduser = $this->ion_auth->user()->row();
		$nama_group = get_jabatan($loggeduser->id);
		$find_akses = get_akses($loggeduser->id);
		//test($find_akses);
		//test($nama_group);
		if($find_akses['name'] === 'siswa'){
			//$siswa = $this->siswa->get($loggeduser->siswa_id);
			//Datasiswa::find_by_id($loggeduser->data_siswa_id);
			$data['ajaran']	= $ajaran->tahun.' Semester '.$ajaran->semester;
			$data['data'] = $this->catatan_wali->find("semester_id = $ajaran->id AND siswa_id = '$loggeduser->siswa_id'");
			//Catatanwali::find_by_ajaran_id_and_siswa_id($ajaran->id, $siswa->id);
			$this->template->title('Administrator Panel')
			->set_layout($this->admin_tpl)
			->set('page_title', 'Catatan Wali Kelas')
			->build($this->admin_folder.'/laporan/catatan_siswa',$data);
		} else {
			if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
				$rombongan_belajar = $this->rombongan_belajar->find("semester_id = $ajaran->id AND sekolah_id = '$loggeduser->sekolah_id' AND guru_id = '$loggeduser->guru_id'");
				$rombongan_belajar_id = ($rombongan_belajar) ? $rombongan_belajar->rombongan_belajar_id : gen_uuid();
				$file = 'add_catatan_wali';
				$pilih_rombel = '';
				//$pilih_rombel = '<a href="'.site_url('admin/laporan/add_catatan_wali_kelas').'" class="btn btn-success" style="float:right;"><i class="fa fa-plus-circle"></i> Tambah Data</a>';
			} else {
				$rombongan_belajar_id = gen_uuid();
				$pilih_rombel = '';
				//$pilih_rombel = '<a href="'.site_url('admin/laporan/add_catatan_wali_kelas').'" class="btn btn-success" style="float:right;"><i class="fa fa-plus-circle"></i> Tambah Data</a>';
				$file = 'list_deskripsi_antar_mapel';
			}
			$check_2018 = check_2018();
			if($check_2018){
				$page_title = 'Catatan Akademik';
			} else {
				$page_title = 'Catatan Wali Kelas';
			}
			$this->template->title('Administrator Panel')
			->set_layout($this->admin_tpl)
			->set('page_title', $page_title)
			->set('guru_id', $loggeduser->guru_id)
			->set('pilih_rombel', $pilih_rombel)
			->set('check_2018', $check_2018)
			->set('sekolah_id', $loggeduser->sekolah_id)
			->set('rombongan_belajar_id', $rombongan_belajar_id)
			->set('semester_id', $ajaran->id)
			->set('form_action', 'admin/laporan/simpan_catatan_wali')
			->build($this->admin_folder.'/laporan/'.$file);
		}
	}
	public function add_catatan_wali_kelas(){
		$ajaran = get_ta();
		$loggeduser = $this->ion_auth->user()->row();
		$admin_group = array(1,2,3,5,6);
		hak_akses($admin_group);
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('form_action', 'admin/laporan/simpan_catatan_wali')
		->set('page_title', 'Tambah Data Catatan Wali Kelas')
		->set('guru_id', $loggeduser->guru_id)
		->build($this->admin_folder.'/laporan/add_catatan_wali');
	}
	public function deskripsi_sikap(){
		$file = 'list_deskripsi_sikap';
		$admin_group = array(3);
		hak_akses($admin_group);
		$loggeduser = $this->ion_auth->user()->row();
		$nama_group = get_jabatan($loggeduser->id);
		$find_akses = get_akses($loggeduser->id);
		if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
			$file = 'deskripsi_sikap';
		}
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('form_action', 'admin/laporan/simpan_deskripsi_sikap')
		->set('page_title', 'Deskripsi Sikap')
		->set('sekolah_id', $loggeduser->sekolah_id)
		->build($this->admin_folder.'/laporan/'.$file);
	}
	public function simpan_deskripsi_sikap(){
		$ajaran_id 						= $this->input->post('ajaran_id');
		$rombel_id 						= $this->input->post('rombel_id');
		$siswa_id 						= $this->input->post('siswa_id');
		$uraian_deskripsi_spiritual 	= $this->input->post('uraian_deskripsi_spiritual');
		$uraian_deskripsi_sosial 		= $this->input->post('uraian_deskripsi_sosial');
		$predikat_spiritual 			= $this->input->post('predikat_spiritual');
		$predikat_sosial		 		= $this->input->post('predikat_sosial');
		foreach($siswa_id as $k=>$siswa){
			$find = $this->deskripsi_sikap->find("semester_id = $ajaran_id AND rombongan_belajar_id = '$rombel_id' AND siswa_id = '$siswa'");
			//Deskripsisikap::find_by_ajaran_id_and_rombel_id_and_siswa_id($ajaran_id,$rombel_id,$siswa);
			if($find){
				$deskripsi_sikap_update = array(
					'uraian_deskripsi_spiritual' 	=> $uraian_deskripsi_spiritual[$k],
					'uraian_deskripsi_sosial' 		=> $uraian_deskripsi_sosial[$k],
					'predikat_spiritual'			=> $predikat_spiritual[$k],
					'predikat_sosial'				=> $predikat_sosial[$k],
				);
				$this->deskripsi_sikap->update($find->deskripsi_sikap_id, $deskripsi_sikap_update);
				$this->session->set_flashdata('success', 'Berhasil memperbaharui data deskripsi sikap');
			} else {
				if($uraian_deskripsi_spiritual[$k]){
					$deskripsi_sikap_insert = array(
						'deskripsi_sikap_id'			=> gen_uuid(),
						'semester_id'					=> $ajaran_id,
						'rombongan_belajar_id'			=> $rombel_id,
						'siswa_id' 						=> $siswa,
						'uraian_deskripsi_spiritual' 	=> $uraian_deskripsi_spiritual[$k],
						'uraian_deskripsi_sosial' 		=> $uraian_deskripsi_sosial[$k],
						'predikat_spiritual'			=> $predikat_spiritual[$k],
						'predikat_sosial'				=> $predikat_sosial[$k],
					);
					//test($deskripsi_sikap_insert);
					$this->deskripsi_sikap->insert($deskripsi_sikap_insert);
				}
				$this->session->set_flashdata('success', 'Berhasil menambah data deskripsi sikap');
			}
		}
		redirect('admin/laporan/deskripsi_sikap');
	}
	public function absensi(){
		$admin_group = array(1,2,3,5,6);
		hak_akses($admin_group);
		$loggeduser = $this->ion_auth->user()->row();
		$nama_group = get_jabatan($loggeduser->id);
		$find_akses = get_akses($loggeduser->id);
		if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
			$file = 'add_absensi';
		} else {
			$file = 'list_absensi';
		}
		$data['tingkat_pendidikan'] = $this->tingkat_pendidikan->get_all();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('form_action', 'admin/laporan/simpan_absensi')
		->set('page_title', 'Data Kehadiran Peserta Didik')
		->set('sekolah_id', $loggeduser->sekolah_id)
		->build($this->admin_folder.'/laporan/'.$file,$data);
	}
	public function ekstrakurikuler(){
		$admin_group = array(1,2,3,5,6);
		$ajaran = get_ta();
		hak_akses($admin_group);
		$loggeduser = $this->ion_auth->user()->row();
		$nama_group = get_jabatan($loggeduser->id);
		$find_akses = get_akses($loggeduser->id);
		$guru_id = $find_akses['id'][0];
		$pilih_rombel = '';
		if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
			$find_ekskul = $this->ekstrakurikuler->find("guru_id = '$guru_id' AND semester_id = $ajaran->id");
			if($find_ekskul){
				$pilih_rombel = '<a href="'.site_url('admin/asesmen/ekstrakurikuler').'" class="btn btn-success" style="float:right;"><i class="fa fa-plus-circle"></i> Tambah Nilai Ekstrakurikuler</a>';
				$file = 'form_ekstrakurikuler';
			} else {
				//$file = 'form_ekstrakurikuler';
				$file = 'list_ekstrakurikuler';
			}
		} else {
			$file = 'list_ekstrakurikuler';
		}
		$file = 'list_ekstrakurikuler';
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('form_action', 'admin/laporan/simpan_ekstrakurikuler')
		->set('page_title', 'Ekstrakurikuler')
		->set('pilih_rombel', $pilih_rombel)
		->set('sekolah_id', $loggeduser->sekolah_id)
		->build($this->admin_folder.'/laporan/'.$file);
	}
	public function prakerin(){
		$admin_group = array(1,2,3,5,6);
		hak_akses($admin_group);
		$loggeduser = $this->ion_auth->user()->row();
		$nama_group = get_jabatan($loggeduser->id);
		$find_akses = get_akses($loggeduser->id);
		if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
			$file = 'prakerin';
		} else {
			$file = 'list_prakerin';
		}
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('form_action', 'admin/laporan/simpan_prakerin')
		->set('page_title', 'Praktik Kerja Lapangan')
		->set('sekolah_id', $loggeduser->sekolah_id)
		->build($this->admin_folder.'/laporan/'.$file);
	}
	public function prestasi(){
		$admin_group = array(1,2,3,5,6);
		hak_akses($admin_group);
		$loggeduser = $this->ion_auth->user()->row();
		$nama_group = get_jabatan($loggeduser->id);
		$find_akses = get_akses($loggeduser->id);
		if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
			$file = 'prestasi';
		} else {
			$file = 'list_prestasi';
		}
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('form_action', 'admin/laporan/simpan_prestasi')
		->set('page_title', 'Prestasi Peserta Didik')
		->set('sekolah_id', $loggeduser->sekolah_id)
		->build($this->admin_folder.'/laporan/'.$file);
	}
	public function rapor(){
		$ajaran = get_ta();
		$admin_group = array(1,2,3,5,6);
		hak_akses($admin_group);
		$loggeduser = $this->ion_auth->user()->row();
		$nama_group = get_jabatan($loggeduser->id);
		$find_akses = get_akses($loggeduser->id);
		$data = '';
		if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
			$guru_id = $find_akses['id'][0];
			$rombel = $this->rombongan_belajar->find("guru_id = '$guru_id' AND semester_id = $ajaran->id");
			//Datarombel::find_by_guru_id_and_ajaran_id($find_akses['id'], $ajaran->id);
			$rombel_id = ($rombel) ? $rombel->rombongan_belajar_id : 0;
			$kurikulum_id = isset($rombel->kurikulum_id) ? $rombel->kurikulum_id : 0;
			$ajaran = get_ta();
			$data['query'] = 'wali';
			$data['rombel_id'] = $rombel_id;
			$data['ajaran_id'] = $ajaran->id;
			$nama_kompetensi = get_kurikulum($kurikulum_id);
			if (strpos($nama_kompetensi, 'REV') !== false) {
				$data['nama_kompetensi'] = 2017;
			} elseif (strpos($nama_kompetensi, 'SMK KTSP') !== false) {
				$data['nama_kompetensi'] = 'ktsp';
			} else {
				$data['nama_kompetensi'] = 2013;
			}
			$folder = '/cetak/';
		} else {
			$data['query'] = 'waka';
			//$data['ajarans'] = Ajaran::all();
			//$data['rombels'] = Datarombel::find('all', array('group' => 'tingkat','order'=>'tingkat ASC'));
			$folder = '/laporan/';
		}
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Cetak Rapor')
		->build($this->admin_folder.$folder.'rapor',$data);
		//->build($this->admin_folder.'/cetak/'.$file,$data);
	}
	public function legger(){
		$ajaran = get_ta();
		$admin_group = array(1,2,3,5,6);
		hak_akses($admin_group);
		$loggeduser = $this->ion_auth->user()->row();
		$nama_group = get_jabatan($loggeduser->id);
		$find_akses = get_akses($loggeduser->id);
		$data = '';
		if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
			$guru_id = $find_akses['id'][0];
			$rombel = $this->rombongan_belajar->find("guru_id = '$guru_id' AND semester_id = $ajaran->id");
			//Datarombel::find_by_guru_id_and_ajaran_id($guru_id,$ajaran->id);
			$rombel_id = ($rombel) ? $rombel->rombongan_belajar_id : 0;
			$kurikulum_id = isset($rombel->kurikulum_id) ? $rombel->kurikulum_id : 0;
			$data['query'] = 'wali';
			$data['rombel_id'] = $rombel_id;
			$data['ajaran_id'] = $ajaran->id;
			//check_2018
			$data['loggeduser'] = $this->ion_auth->user()->row();
			$data['nama_rombel'] = $this->rombongan_belajar->get($rombel_id);
			$data['data_siswa'] = get_siswa_by_rombel($rombel_id);
			$data['kompetensi_id'] = 1;
			$data['data_mapel'] = $this->pembelajaran->with('mata_pelajaran')->find_all(" semester_id =  $ajaran->id AND  rombongan_belajar_id = '$rombel_id' AND kelompok_id IS NOT NULL", '*','kelompok_id ASC, no_urut ASC');
			$kompetensi = get_kurikulum($kurikulum_id);			
			//Datakurikulum::find_by_kurikulum_id($kurikulum_id);
			$nama_kompetensi = isset($kompetensi->nama_kurikulum) ? $kompetensi->nama_kurikulum : 0;
			if (strpos($nama_kompetensi, 'REV') !== false) {
				$data['nama_kompetensi'] = 2017;
			} elseif (strpos($nama_kompetensi, 'SMK KTSP') !== false) {
				$data['nama_kompetensi'] = 'ktsp';
			} else {
				$data['nama_kompetensi'] = 2013;
			}
			$folder = '/cetak/';
			$check_2018 = check_2018();
			if($check_2018){
				$file = 'legger_2018';
			} else {
				$file = 'legger';
			}
		} else {
			$folder = '/laporan/';
			$file = 'legger';
		}
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Cetak Legger')
		->build($this->admin_folder.$folder.$file,$data);
	}
	public function list_catatan_wali_kelas($jurusan = NULL, $tingkat = NULL, $rombel = NULL){
		$ajaran = get_ta();
		$search = "";
		$start = 0;
		$rows = 25;
		// get search value (if any)
		if (isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$search = $_GET['sSearch'];
		}

		// limit
		$start = get_start();
		$rows = get_rows();
		$join = '';
		if($jurusan && $tingkat == NULL && $rombel == NULL){
			$join = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan)";
		}elseif($jurusan && $tingkat && $rombel == NULL){
			$join = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan) AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE tingkat = $tingkat)";
		} elseif($jurusan && $tingkat && $rombel){
			$join = "AND rombongan_belajar_id = '$rombel'";
		}
		$where = "(rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE semester_id = $ajaran->id AND nama LIKE '%$search%') OR siswa_id IN (SELECT siswa_id FROM ref_siswa WHERE nama LIKE '%$search%') OR uraian_deskripsi LIKE '%$search%')";
		$query = $this->catatan_wali->find_all("semester_id = $ajaran->id $join AND ($where)", '*','rombongan_belajar_id ASC', $start, $rows);
		$filter = $this->catatan_wali->find_count("semester_id = $ajaran->id $join AND ($where)", '*','rombongan_belajar_id ASC');
		$iFilteredTotal = count($query);
		
		$iTotal= $filter;
	    
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iTotal,
	        "aaData" => array()
	    );

	    // get result after running query and put it in array
		$i=$start;
		//$barang = $query->result();
	    foreach ($query as $temp) {
			//test($temp);
			$record = array();
            $tombol_aktif = '';
			$rombel = $this->rombongan_belajar->get($temp->rombongan_belajar_id);
			if($temp->uraian_deskripsi){
				$uraian_deskripsi = limit_words($temp->catatan_wali_id,$temp->uraian_deskripsi,10);
			} else {
				$uraian_deskripsi = $temp->uraian_deskripsi;
			}
			if (strpos(get_kurikulum($rombel->kurikulum_id), 'REV') !== false) {
				$kur = 2017;
			} elseif (strpos(get_kurikulum($rombel->kurikulum_id), '2013') !== false) {
				$kur = 2013;
			} else {
				$kur = 'ktsp';
			}
			$record[] = '<div class="text-center">'.get_nama_rombel($temp->rombongan_belajar_id).'</div>';
			$record[] = get_wali_kelas($temp->rombongan_belajar_id);
			$record[] = get_nama_siswa($temp->siswa_id);
            $record[] = $uraian_deskripsi;
            $record[] = '<div class="text-center"><a href="'.site_url('admin/laporan/review_rapor/'.$kur.'/'.$ajaran->id.'/'.$temp->rombongan_belajar_id.'/'.$temp->siswa_id).'" class="btn btn-info btn-sm">Pratinjau Rapor</a></div>';
			$output['aaData'][] = $record;
		}
		if($jurusan && $tingkat){
			$get_all_rombel = $this->rombongan_belajar->find_all("jurusan_id = $jurusan AND tingkat = $tingkat AND semester_id = $ajaran->id");
			foreach($get_all_rombel as $allrombel){
				$all_rombel= array();
				$all_rombel['value'] = $allrombel->rombongan_belajar_id;
				$all_rombel['text'] = $allrombel->nama;
				$output['rombel'][] = $all_rombel;
			}
		}
		// format it to JSON, this output will be displayed in datatable
		echo json_encode($output);
	}
	public function list_absensi($jurusan = NULL, $tingkat = NULL, $rombel = NULL){
		$loggeduser = $this->ion_auth->user()->row();
		$ajaran = get_ta();
		$search = "";
		$start = 0;
		$rows = 25;
		// get search value (if any)
		if (isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$search = $_GET['sSearch'];
		}

		// limit
		$start = get_start();
		$rows = get_rows();
		$join = "";
		if($jurusan && $tingkat == NULL && $rombel == NULL){
			$join = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan)";
		}elseif($jurusan && $tingkat && $rombel == NULL){
			$join = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan) AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE tingkat = $tingkat)";
		} elseif($jurusan && $tingkat && $rombel){
			$join = "AND rombongan_belajar_id = '$rombel'";
		}
		$where = "(rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE semester_id = $ajaran->id AND nama LIKE '%$search%') OR siswa_id IN (SELECT siswa_id FROM ref_siswa WHERE nama LIKE '%$search%'))";
		$query = $this->absen->find_all("semester_id = $ajaran->id $join AND ($where)", '*','rombongan_belajar_id ASC', $start, $rows);
		$filter = $this->absen->find_count("semester_id = $ajaran->id $join AND ($where)", '*','rombongan_belajar_id ASC');
		$iFilteredTotal = count($query);
		
		$iTotal= $filter;
	    
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iTotal,
	        "aaData" => array()
	    );

	    // get result after running query and put it in array
		$i=$start;
		//$barang = $query->result();
	    foreach ($query as $temp) {
			$rombel = $this->rombongan_belajar->get($temp->rombongan_belajar_id);
			if (strpos(get_kurikulum($rombel->kurikulum_id), 'REV') !== false) {
				$kur = 2017;
			} elseif (strpos(get_kurikulum($rombel->kurikulum_id), '2013') !== false) {
				$kur = 2013;
			} else {
				$kur = 'ktsp';
			}
			$record = array();
			$record[] = '<div class="text-center">'.get_nama_rombel($temp->rombongan_belajar_id).'</div>';
			$record[] = get_nama_siswa($temp->siswa_id);
            $record[] = '<div class="text-center">'.$temp->sakit.'</div>';
            $record[] = '<div class="text-center">'.$temp->izin.'</div>';
            $record[] = '<div class="text-center">'.$temp->alpa.'</div>';
            $record[] = '<div class="text-center"><a href="'.site_url('admin/laporan/review_rapor/'.$kur.'/'.$ajaran->id.'/'.$temp->rombongan_belajar_id.'/'.$temp->siswa_id).'" class="btn btn-info btn-sm">Pratinjau Rapor</a></div>';
			$output['aaData'][] = $record;
		}
		// format it to JSON, this output will be displayed in datatable
		if($jurusan && $tingkat){
			$get_all_rombel = $this->rombongan_belajar->find_all("jurusan_id = $jurusan AND tingkat = $tingkat AND semester_id = $ajaran->id");
			foreach($get_all_rombel as $allrombel){
				$all_rombel= array();
				$all_rombel['value'] = $allrombel->rombongan_belajar_id;
				$all_rombel['text'] = $allrombel->nama;
				$output['rombel'][] = $all_rombel;
			}
		}
		echo json_encode($output);
	}
	public function list_ekstrakurikuler($jurusan = NULL, $tingkat = NULL, $rombel = NULL){
		$loggeduser = $this->ion_auth->user()->row();
		$ajaran = get_ta();
		$search = "";
		$start = 0;
		$rows = 25;
		// get search value (if any)
		if (isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$search = $_GET['sSearch'];
		}
		// limit
		$start = get_start();
		$rows = get_rows();
		$join = "";
		$nama_group = get_jabatan($loggeduser->id);
		$find_akses = get_akses($loggeduser->id);
		if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
			$walas = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE semester_id = $ajaran->id AND guru_id = '$loggeduser->guru_id')";
		} else {
			$walas = "";
		}
		if($jurusan && $tingkat == NULL && $rombel == NULL){
			$join = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan)";
		}elseif($jurusan && $tingkat && $rombel == NULL){
			$join = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan) AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE tingkat = $tingkat)";
		} elseif($jurusan && $tingkat && $rombel){
			$join = "AND rombongan_belajar_id = '$rombel'";
		}
		$where = "(rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE semester_id = $ajaran->id AND nama LIKE '%$search%') OR siswa_id IN (SELECT siswa_id FROM ref_siswa WHERE nama LIKE '%$search%'))";
		$query = $this->nilai_ekstrakurikuler->find_all("sekolah_id = '$loggeduser->sekolah_id' AND semester_id = $ajaran->id $walas $join AND $where", '*','rombongan_belajar_id ASC', $start, $rows);
		$filter = $this->nilai_ekstrakurikuler->find_count("sekolah_id = '$loggeduser->sekolah_id' AND semester_id = $ajaran->id $walas $join AND $where", '*','rombongan_belajar_id ASC');
		$iFilteredTotal = count($query);
		
		$iTotal= $filter;
	    
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iTotal,
	        "aaData" => array()
	    );

	    // get result after running query and put it in array
		$i=$start;
		//$barang = $query->result();
	    foreach ($query as $temp) {
			$rombel = $this->rombongan_belajar->get($temp->rombongan_belajar_id);
			if (strpos(get_kurikulum($rombel->kurikulum_id), 'REV') !== false) {
				$kur = 2017;
			} elseif (strpos(get_kurikulum($rombel->kurikulum_id), '2013') !== false) {
				$kur = 2013;
			} else {
				$kur = 'ktsp';
			}
			$record = array();
            $tombol_aktif = '';
			$record[] = get_nama_rombel($temp->rombongan_belajar_id);
			$record[] = get_nama_siswa($temp->siswa_id);
            $record[] = get_nama_ekskul($temp->ekstrakurikuler_id);
            $record[] = get_nilai_ekskul($temp->nilai);
            $record[] = $temp->deskripsi_ekskul;
            $record[] = '<div class="text-center"><a href="'.site_url('admin/laporan/review_rapor/'.$kur.'/'.$ajaran->id.'/'.$temp->rombongan_belajar_id.'/'.$temp->siswa_id).'" class="btn btn-info btn-sm">Pratinjau Rapor</a></div>';
			$output['aaData'][] = $record;
		}
		// format it to JSON, this output will be displayed in datatable
		if($jurusan && $tingkat){
			$get_all_rombel = $this->rombongan_belajar->find_all("jurusan_id = $jurusan AND tingkat = $tingkat AND semester_id = $ajaran->id");
			foreach($get_all_rombel as $allrombel){
				$all_rombel= array();
				$all_rombel['value'] = $allrombel->rombongan_belajar_id;
				$all_rombel['text'] = $allrombel->nama;
				$output['rombel'][] = $all_rombel;
			}
		}
		echo json_encode($output);
	}
	public function list_prakerin($jurusan = NULL, $tingkat = NULL, $rombel = NULL){
		$loggeduser = $this->ion_auth->user()->row();
		$ajaran = get_ta();
		$search = "";
		$start = 0;
		$rows = 25;
		// get search value (if any)
		if (isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$search = $_GET['sSearch'];
		}
		// limit
		$start = get_start();
		$rows = get_rows();
		$join = "";
		if($jurusan && $tingkat == NULL && $rombel == NULL){
			$join = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan)";
		}elseif($jurusan && $tingkat && $rombel == NULL){
			$join = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan) AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE tingkat = $tingkat)";
		} elseif($jurusan && $tingkat && $rombel){
			$join = "AND rombongan_belajar_id = '$rombel'";
		}
		$where = "(rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE semester_id = $ajaran->id AND nama LIKE '%$search%') OR siswa_id IN (SELECT siswa_id FROM ref_siswa WHERE nama LIKE '%$search%') OR mitra_prakerin LIKE '%$search%' OR lokasi_prakerin LIKE '%$search%')";
		$query = $this->prakerin->find_all("semester_id = $ajaran->id $join AND ($where)", '*','rombongan_belajar_id ASC', $start, $rows);
		$filter = $this->prakerin->find_count("semester_id = $ajaran->id $join AND ($where)", '*','rombongan_belajar_id ASC');
		$iFilteredTotal = count($query);
		
		$iTotal= $filter;
	    
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iTotal,
	        "aaData" => array()
	    );

	    // get result after running query and put it in array
		$i=$start;
		//$barang = $query->result();
	    foreach ($query as $temp) {
			$rombel = $this->rombongan_belajar->get($temp->rombongan_belajar_id);
			if (strpos(get_kurikulum($rombel->kurikulum_id), 'REV') !== false) {
				$kur = 2017;
			} elseif (strpos(get_kurikulum($rombel->kurikulum_id), '2013') !== false) {
				$kur = 2013;
			} else {
				$kur = 'ktsp';
			}
			$record = array();
            $tombol_aktif = '';
			$record[] = get_nama_rombel($temp->rombongan_belajar_id);
			$record[] = get_nama_siswa($temp->siswa_id);
            $record[] = $temp->mitra_prakerin;
            $record[] = $temp->lokasi_prakerin;
            $record[] = $temp->lama_prakerin;
			$record[] = $temp->keterangan_prakerin;
            $record[] = '<div class="text-center"><a href="'.site_url('admin/laporan/review_rapor/'.$kur.'/'.$ajaran->id.'/'.$temp->rombongan_belajar_id.'/'.$temp->siswa_id).'" class="btn btn-info btn-sm">Pratinjau Rapor</a></div>';
			$output['aaData'][] = $record;
		}
		// format it to JSON, this output will be displayed in datatable
		if($jurusan && $tingkat){
			$get_all_rombel = $this->rombongan_belajar->find_all("jurusan_id = $jurusan AND tingkat = $tingkat AND semester_id = $ajaran->id");
			foreach($get_all_rombel as $allrombel){
				$all_rombel= array();
				$all_rombel['value'] = $allrombel->rombongan_belajar_id;
				$all_rombel['text'] = $allrombel->nama;
				$output['rombel'][] = $all_rombel;
			}
		}
		echo json_encode($output);
	}
	public function list_prestasi($jurusan = NULL, $tingkat = NULL, $rombel = NULL){
		$loggeduser = $this->ion_auth->user()->row();
		$ajaran = get_ta();
		$search = "";
		$start = 0;
		$rows = 25;
		// get search value (if any)
		if (isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$search = $_GET['sSearch'];
		}
		// limit
		$start = get_start();
		$rows = get_rows();
		$join = "";
		if($jurusan && $tingkat == NULL && $rombel == NULL){
			$join = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan)";
		}elseif($jurusan && $tingkat && $rombel == NULL){
			$join = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan) AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE tingkat = $tingkat)";
		} elseif($jurusan && $tingkat && $rombel){
			$join = "AND rombongan_belajar_id = '$rombel'";
		}
		$where = "(rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE semester_id = $ajaran->id AND nama LIKE '%$search%') OR siswa_id IN (SELECT siswa_id FROM ref_siswa WHERE nama LIKE '%$search%') OR jenis_prestasi LIKE '%$search%' OR keterangan_prestasi LIKE '%$search%')";
		$query = $this->prestasi->find_all("semester_id = $ajaran->id $join AND ($where)", '*','rombongan_belajar_id ASC', $start, $rows);
		$filter = $this->prestasi->find_count("semester_id = $ajaran->id $join AND ($where)", '*','rombongan_belajar_id ASC');
		$iFilteredTotal = count($query);
		
		$iTotal= $filter;
	    
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iTotal,
	        "aaData" => array()
	    );

	    // get result after running query and put it in array
		$i=$start;
		//$barang = $query->result();
	    foreach ($query as $temp) {
			$rombel = $this->rombongan_belajar->get($temp->rombongan_belajar_id);
			if (strpos(get_kurikulum($rombel->kurikulum_id), 'REV') !== false) {
				$kur = 2017;
			} elseif (strpos(get_kurikulum($rombel->kurikulum_id), '2013') !== false) {
				$kur = 2013;
			} else {
				$kur = 'ktsp';
			}
			$record = array();
            $tombol_aktif = '';
			$record[] = get_nama_rombel($temp->rombongan_belajar_id);
			$record[] = get_nama_siswa($temp->siswa_id);
            $record[] = $temp->jenis_prestasi;
			$record[] = $temp->keterangan_prestasi;
            $record[] = '<div class="text-center"><a href="'.site_url('admin/laporan/review_rapor/'.$kur.'/'.$ajaran->id.'/'.$temp->rombongan_belajar_id.'/'.$temp->siswa_id).'" class="btn btn-info btn-sm">Pratinjau Rapor</a></div>';
			$output['aaData'][] = $record;
		}
		// format it to JSON, this output will be displayed in datatable
		if($jurusan && $tingkat){
			$get_all_rombel = $this->rombongan_belajar->find_all("jurusan_id = $jurusan AND tingkat = $tingkat AND semester_id = $ajaran->id");
			foreach($get_all_rombel as $allrombel){
				$all_rombel= array();
				$all_rombel['value'] = $allrombel->rombongan_belajar_id;
				$all_rombel['text'] = $allrombel->nama;
				$output['rombel'][] = $all_rombel;
			}
		}
		echo json_encode($output);
	}
	public function view_deskripsi_antar_mapel($id){
		$deskripsi = $this->catatan_wali->get($id);
		$siswa = $this->siswa->get($deskripsi->siswa_id);
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Deskripsi Antar Mata Pelajaran '.$siswa->nama)
		->set('data', $deskripsi->uraian_deskripsi)
		->build($this->admin_folder.'/laporan/view_deskripsi_antar_mapel');
	}
	public function review_rapor($kur,$ajaran_id,$rombel_id,$siswa_id){
		$loggeduser = $this->ion_auth->user()->row();
		$data['sekolah_id'] = $loggeduser->sekolah_id;
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['siswa_id'] = $siswa_id;
		$data['kurikulum_id'] = $kur;
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Pratinjau Rapor')
		->build($this->admin_folder.'/laporan/review_rapor',$data);
	}
	public function review_desc($kur, $ajaran_id,$rombel_id,$siswa_id){
		$loggeduser = $this->ion_auth->user()->row();
		$data['sekolah_id'] = $loggeduser->sekolah_id;
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['siswa_id'] = $siswa_id;
		$data['kurikulum'] = $kur;
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Pratinjau Deskripsi')
		->build($this->admin_folder.'/laporan/review_desc',$data);
	}
	public function review_nilai($kur,$ajaran_id,$rombel_id,$siswa_id){
		$loggeduser = $this->ion_auth->user()->row();
		$data['sekolah_id'] = $loggeduser->sekolah_id;
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['siswa_id'] = $siswa_id;
		$data['kurikulum'] = $kur;
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Pratinjau Nilai')
		->build($this->admin_folder.'/laporan/rapor_nilai',$data);
	}
	public function simpan_catatan_wali(){
		$ajaran_id = $_POST['ajaran_id'];
		$rombel_id = $_POST['rombel_id'];
		$siswa_id = $_POST['siswa_id'];
		foreach($siswa_id as $key=>$siswa){
			$catatan_wali = $this->catatan_wali->find("semester_id = $ajaran_id AND rombongan_belajar_id = '$rombel_id' AND siswa_id = '$siswa'");
			//Catatanwali::find_by_ajaran_id_and_rombel_id_and_siswa_id($ajaran_id,$rombel_id,$siswa);
			if($catatan_wali){
				$catatan_wali_update = array(
					'uraian_deskripsi' => $_POST['uraian_deskripsi'][$key]
				);
				$this->catatan_wali->update($catatan_wali->catatan_wali_id, $catatan_wali_update);
				$this->session->set_flashdata('success', 'Berhasil memperbaharui deskripsi antar mata pelajaran');
			} else {
				$new_catatan_wali = array(
					'catatan_wali_id'		=> gen_uuid(),
					'semester_id'			=> $ajaran_id,
					'rombongan_belajar_id'	=> $rombel_id,
					'siswa_id'				=> $siswa,
					'uraian_deskripsi' 		=> $_POST['uraian_deskripsi'][$key],
				);
				$this->catatan_wali->insert($new_catatan_wali);
				$this->session->set_flashdata('success', 'Berhasil menambah deskripsi antar mata pelajaran');
			}
		}
		redirect('admin/laporan/catatan_wali_kelas');
	}
	public function simpan_absensi(){
		$loggeduser = $this->ion_auth->user()->row();
		$ajaran_id = $_POST['ajaran_id'];
		$rombel_id = $_POST['rombel_id'];
		$siswa_id = $_POST['siswa_id'];
		foreach($siswa_id as $key=>$siswa){
			array_walk($_POST['sakit'], 'check_numeric','laporan/absensi');
			array_walk($_POST['izin'], 'check_numeric','laporan/absensi');
			array_walk($_POST['alpa'], 'check_numeric','laporan/absensi');
			$absen = $this->absen->find("semester_id = $ajaran_id AND rombongan_belajar_id = '$rombel_id' AND siswa_id = '$siswa'");
			//Absen::find_by_ajaran_id_and_rombel_id_and_siswa_id($ajaran_id,$rombel_id,$siswa);
			if($absen){
				$absen_update = array(
					'sakit' => ($_POST['sakit'][$key]) ? $_POST['sakit'][$key] : 0,
					'izin' 	=> ($_POST['izin'][$key]) ? $_POST['izin'][$key] : 0,
					'alpa'	=> ($_POST['alpa'][$key]) ? $_POST['alpa'][$key] : 0,
				);
				$this->absen->update($absen->absen_id, $absen_update);
				$this->session->set_flashdata('success', 'Berhasil memperbaharui data absensi');
			} else {
				$new_absen = array(
					'absen_id'				=> gen_uuid(),
					'sekolah_id'			=> $loggeduser->sekolah_id,
					'semester_id'			=> $ajaran_id,
					'rombongan_belajar_id'	=> $rombel_id,
					'siswa_id'				=> $siswa,
					'sakit'					=> ($_POST['sakit'][$key]) ? $_POST['sakit'][$key] : 0,
					'izin'					=> ($_POST['izin'][$key]) ? $_POST['izin'][$key] : 0,
					'alpa'					=> ($_POST['alpa'][$key]) ? $_POST['alpa'][$key] : 0,
				);
				$this->absen->insert($new_absen);
				$this->session->set_flashdata('success', 'Berhasil menambah data absensi');
			}
		}
		redirect('admin/laporan/absensi');
	}
	public function simpan_ekstrakurikuler(){
		$ajaran_id = $_POST['ajaran_id'];
		$rombel_id = $_POST['rombel_id'];
		$ekskul_id = $_POST['ekskul_id'];
		$siswa_id = $_POST['siswa_id'];
		foreach($siswa_id as $key=>$siswa){
			$nilai_ekskul = $this->nilai_ekstrakurikuler->find("semester_id = $ajaran_id AND ekstrakurikuler_id = '$ekskul_id' AND rombongan_belajar_id = '$rombel_id' AND siswa_id = '$siswa'");
			//Nilaiekskul::find_by_ajaran_id_and_ekskul_id_and_rombel_id_and_siswa_id($ajaran_id,$ekskul_id,$rombel_id,$siswa);
			if($nilai_ekskul){
				$nilai_ekskul_update = array(
					'nilai' => $_POST['nilai'][$key],
					'deskripsi_ekskul' 	=> $_POST['deskripsi_ekskul'][$key],
				);
				$this->nilai_ekstrakurikuler->update($nilai_ekskul->nilai_ekstrakurikuler_id, $nilai_ekskul_update);
				$this->session->set_flashdata('success', 'Berhasil memperbaharui nilai ekstrakurikuler');
			} else {
				$new_ekskul= array(
					'nilai_ekstrakurikuler_id'	=> gen_uuid(),
					'semester_id'				=> $ajaran_id,
					'ekstrakurikuler_id' 		=> $ekskul_id,
					'rombongan_belajar_id'		=> $rombel_id,
					'siswa_id'					=> $siswa,
					'nilai' 					=> $_POST['nilai'][$key],
					'deskripsi_ekskul'			=> $_POST['deskripsi_ekskul'][$key],
				);
				$this->nilai_ekstrakurikuler->insert($new_ekskul);
				$this->session->set_flashdata('success', 'Berhasil menambah nilai ekstrakurikuler');
			}
		}
		redirect('admin/laporan/ekstrakurikuler');
	}
	public function simpan_prakerin(){
		$ajaran_id = $_POST['ajaran_id'];
		$rombel_id = $_POST['rombel_id'];
		$siswa_id = $_POST['siswa_id'];
		$mitra_prakerin = $_POST['mitra_prakerin'];
		$lokasi_prakerin = $_POST['lokasi_prakerin'];
		$lama_prakerin = $_POST['lama_prakerin'];
		$keterangan_prakerin = $_POST['keterangan_prakerin'];
		$post_lama_prakerin = array($lama_prakerin);
		//array_walk($post_lama_prakerin, 'check_numeric','laporan/prakerin');
		$new_prakerin = array(
			'prakerin_id'			=> gen_uuid(),
			'semester_id'			=> $ajaran_id,
			'rombongan_belajar_id'	=> $rombel_id,
			'siswa_id'				=> $siswa_id,
			'mitra_prakerin'		=> $mitra_prakerin,
			'lokasi_prakerin'		=> $lokasi_prakerin,
			'lama_prakerin'			=> $lama_prakerin,
			'keterangan_prakerin'	=> $keterangan_prakerin,
		);
		$this->prakerin->insert($new_prakerin);
		$this->session->set_flashdata('success', 'Berhasil menambah data prakerin');
		redirect('admin/laporan/prakerin');
	}
	public function edit_prakerin($id){
		if($_POST){
			$prakerin = $this->prakerin->get($id);
			if($prakerin){
				$prakerin_update = array(
					'mitra_prakerin' => $_POST['mitra_prakerin'], 
					'lokasi_prakerin' => $_POST['lokasi_prakerin'], 
					'lama_prakerin' => $_POST['lama_prakerin'],
					'keterangan_prakerin' => $_POST['keterangan_prakerin']
				);
				$this->prakerin->update($id, $prakerin_update);
			}
			$ajaran_id = $_POST['ajaran_id'];
			$rombel_id = $_POST['rombel_id'];
			$siswa_id = $_POST['siswa_id'];
			$this->form_prakerin($ajaran_id,$rombel_id,$siswa_id);
		} else {
			$prakerin = $this->prakerin->get($id);
			$this->template->title('Administrator Panel : Edit Sikap')
			->set_layout($this->modal_tpl)
			->set('page_title', 'Edit Sikap')
			->set('prakerin', $prakerin)
			->set('modal_footer', '<a class="btn btn-primary" id="button_form" href="javascript:void(0);">Update</a>')
			->build($this->admin_folder.'/laporan/edit_prakerin');
		}
	}
	function form_prakerin($ajaran_id,$rombel_id,$siswa_id){
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['siswa_id'] = $siswa_id;
		$this->load->view('backend/laporan/add_prakerin', $data);
	}
	public function delete_prakerin($id){
		$prakerin = $this->prakerin->delete($id);
	}
	public function simpan_prestasi(){
		$loggeduser = $this->ion_auth->user()->row();
		$ajaran_id = $_POST['ajaran_id'];
		$rombel_id = $_POST['rombel_id'];
		$siswa_id = $_POST['siswa_id'];
		$jenis_prestasi = $_POST['jenis_prestasi'];
		$keterangan_prestasi = $_POST['keterangan_prestasi'];
		$new_prestasi = array(
			'prestasi_id'			=> gen_uuid(),
			//'sekolah_id'			=> $loggeduser->sekolah_id,
			'semester_id'			=> $ajaran_id,
			'rombongan_belajar_id'	=> $rombel_id,
			'siswa_id'				=> $siswa_id,
			'jenis_prestasi'		=> $jenis_prestasi,
			'keterangan_prestasi'	=> $keterangan_prestasi,
		);
		$this->prestasi->insert($new_prestasi);
		$this->session->set_flashdata('success', 'Berhasil menambah data prestasi siswa');
		redirect('admin/laporan/prestasi');
	}
	public function edit_prestasi($id){
		$loggeduser = $this->ion_auth->user()->row();
		$prestasi = $this->prestasi->get($id);
		if($_POST){
			$prestasi_update = array(
				//'sekolah_id'			=> $loggeduser->sekolah_id,
				'jenis_prestasi' 		=> $_POST['jenis_prestasi'], 
				'keterangan_prestasi' 	=> $_POST['keterangan_prestasi']
			);
			$this->prestasi->update($id, $prestasi_update);
			$ajaran_id = $_POST['ajaran_id'];
			$rombel_id = $_POST['rombel_id'];
			$siswa_id = $_POST['siswa_id'];
			$this->form_prestasi($ajaran_id,$rombel_id,$siswa_id);
		} else {
			$this->template->title('Administrator Panel : Edit Prestasi')
			->set_layout($this->modal_tpl)
			->set('page_title', 'Edit Prestasi')
			->set('prestasi', $prestasi)
			->set('modal_footer', '<a class="btn btn-primary" id="button_form" href="javascript:void(0);">Update</a>')
			->build($this->admin_folder.'/laporan/edit_prestasi');
		}
	}
	function form_prestasi($ajaran_id,$rombel_id,$siswa_id){
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['siswa_id'] = $siswa_id;
		$this->load->view('backend/laporan/add_prestasi', $data);
	}
	public function delete_prestasi($id){
		$prakerin = $this->prestasi->delete($id);
	}
	public function list_deskripsi_sikap(){
		$ajaran = get_ta();
		$search = "";
		$start = 0;
		$rows = 25;
		// get search value (if any)
		if (isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$search = $_GET['sSearch'];
		}

		// limit
		$start = get_start();
		$rows = get_rows();
		$where = "(rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE semester_id = $ajaran->id AND nama LIKE '%$search%') OR siswa_id IN (SELECT siswa_id FROM ref_siswa WHERE nama LIKE '%$search%') OR uraian_deskripsi_spiritual LIKE '%$search%' OR uraian_deskripsi_sosial LIKE '%$search%')";
		$query = $this->deskripsi_sikap->find_all("semester_id = $ajaran->id AND ($where)", '*','rombongan_belajar_id ASC', $start, $rows);
		//$this->catatan_wali->find_all("semester_id = $ajaran->id AND ($where)", '*','rombongan_belajar_id ASC', $start, $rows);
		$filter = $this->deskripsi_sikap->find_count("semester_id = $ajaran->id AND ($where)", '*','rombongan_belajar_id ASC');
		//$this->catatan_wali->find_count("semester_id = $ajaran->id AND ($where)", '*','rombongan_belajar_id ASC');
		$iFilteredTotal = count($query);
		
		$iTotal= $filter;
	    
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iTotal,
	        "aaData" => array()
	    );

	    // get result after running query and put it in array
		$i=$start;
		//$barang = $query->result();
	    foreach ($query as $temp) {
			$record = array();
            $tombol_aktif = '';
			//$rombel = $this->rombongan_belajar->get($temp->rombongan_belajar_id);
			/*if($temp->uraian_deskripsi_spiritual){
				$uraian_deskripsi_spiritual = limit_words($temp->id,$temp->uraian_deskripsi_spiritual,10);
				$uraian_deskripsi_sosial = limit_words($temp->id,$temp->uraian_deskripsi_sosial,10);
			} else {
				$uraian_deskripsi = $temp->uraian_deskripsi;
			}
			if (strpos(get_kurikulum($rombel->kurikulum_id), 'REV') !== false) {
				$kur = 2017;
			} elseif (strpos(get_kurikulum($rombel->kurikulum_id), '2013') !== false) {
				$kur = 2013;
			} else {
				$kur = 'ktsp';
			}*/
			$record[] = '<div class="text-center">'.get_nama_rombel($temp->rombongan_belajar_id).'</div>';
			$record[] = get_wali_kelas($temp->rombongan_belajar_id);
			$record[] = get_nama_siswa($temp->siswa_id);
            $record[] = $temp->uraian_deskripsi_spiritual;
			$record[] = $temp->uraian_deskripsi_sosial;
            //$record[] = '<div class="text-center"><a href="'.site_url('admin/laporan/review_rapor/'.$kur.'/'.$ajaran->id.'/'.$temp->rombongan_belajar_id.'/'.$temp->siswa_id).'" class="btn btn-info btn-sm">Pratinjau Rapor</a></div>';
			$output['aaData'][] = $record;
		}
		// format it to JSON, this output will be displayed in datatable
		echo json_encode($output);
	}
	public function naik_kelas(){
		$this->load->model('kenaikan_kelas_model', 'kenaikan_kelas');
		$data['ajaran'] = get_ta();
		$ajaran = $data['ajaran'];
		$loggeduser = $this->ion_auth->user()->row();
		$find_akses = get_akses($loggeduser->id);
		$guru_id = $find_akses['id'][0];
		$data['rombel'] = $this->rombongan_belajar->find("guru_id = '$guru_id' AND semester_id = $ajaran->id");
		$data['data_siswa'] = $this->anggota_rombel->find_all_by_rombongan_belajar_id($data['rombel']->rombongan_belajar_id);
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Kenaikan Kelas '.$data['rombel']->nama)
		->set('form_action', 'admin/laporan/simpan_naik_kelas')
		->build($this->admin_folder.'/laporan/naik_kelas', $data);
	}
	public function simpan_naik_kelas(){
		$this->load->model('kenaikan_kelas_model', 'kenaikan_kelas');
		//test($_POST);
		//die();
		$ajaran_id 			= $this->input->post('ajaran_id');
		$anggota_rombel_id	= $this->input->post('anggota_rombel_id');
		$status 			= $this->input->post('status');
		$ke_kelas 			= $this->input->post('ke_kelas');
		foreach($anggota_rombel_id as $k=>$anggota_rombel){
			$find = $this->kenaikan_kelas->find("semester_id = $ajaran_id AND anggota_rombel_id = '$anggota_rombel'");
			//Deskripsisikap::find_by_ajaran_id_and_rombel_id_and_siswa_id($ajaran_id,$rombel_id,$siswa);
			if($find){
				$kenaikan_kelas_update = array(
					'status' 	=> $status[$k],
					'ke_kelas' 	=> ($ke_kelas[$k]) ? $ke_kelas[$k] : NULL,
				);
				$this->kenaikan_kelas->update($find->kenaikan_kelas_id, $kenaikan_kelas_update);
				$this->session->set_flashdata('success', 'Berhasil memperbaharui data kenaikan kelas');
			} else {
				if($status[$k]){
					$kenaikan_kelas_insert = array(
						'kenaikan_kelas_id'	=> gen_uuid(),
						'semester_id'		=> $ajaran_id,
						'anggota_rombel_id'	=> $anggota_rombel,
						'status' 			=> $status[$k],
						'ke_kelas' 			=> ($ke_kelas[$k]) ? $ke_kelas[$k] : NULL,
					);
					$this->kenaikan_kelas->insert($kenaikan_kelas_insert);
				}
				$this->session->set_flashdata('success', 'Berhasil memproses kenaikan kelas');
			}
		}
		redirect('admin/laporan/naik_kelas');
	}
	public function ppk(){
		$check_2018 = check_2018();
		$loggeduser = $this->ion_auth->user()->row();
		$pilih_rombel = '';
		$find_akses = get_akses($loggeduser->id);
		$nama_group = get_jabatan($loggeduser->id);
		if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
			if($check_2018){
				$pilih_rombel = '<a href="'.site_url('admin/laporan/entry_ppk').'" class="btn btn-success" style="float:right;"><i class="fa fa-plus-circle"></i> Tambah Data</a>';
			} else {
				$pilih_rombel = '<a href="'.site_url('admin/laporan/add_ppk').'" class="btn btn-success" style="float:right;"><i class="fa fa-plus-circle"></i> Tambah Data</a>';
			}
		}
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('pilih_rombel', $pilih_rombel)
		->set('page_title', 'Penguatan Pendidikan Karakter')
		->build($this->admin_folder.'/laporan/list_ppk');
	}
	public function entry_ppk(){
		$loggeduser = $this->ion_auth->user()->row();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('siswa_ppk', 'Peserta Didik', 'required');
		$check_2018 = check_2018();
		if($check_2018){
			$this->form_validation->set_rules('deskripsi[1]', 'Religius', 'required');
			$this->form_validation->set_rules('deskripsi[7]', 'Mandiri', 'required');
			$this->form_validation->set_rules('deskripsi[19]', 'Integritas', 'required');
			$this->form_validation->set_rules('deskripsi[20]', 'Nasionalis', 'required');
			$this->form_validation->set_rules('deskripsi[21]', 'Gotong-royong', 'required');
		}
		$this->form_validation->set_rules('capaian', 'Catatan Perkembangan Karakter', 'required');
		if ($this->form_validation->run() == true){
			$semester_id = $this->input->post('semester_id');
			$siswa_id = $this->input->post('siswa_ppk');
			$capaian = $this->input->post('capaian');
			$all_sikap_id = $this->input->post('sikap_id');
			$deskripsi = $this->input->post('deskripsi');
			$data = array(
				'semester_id' 		=> $semester_id,
				'siswa_id' 			=> $siswa_id,
				'capaian' 			=> preg_replace('/(<br>)+$/', '', $capaian)
			);
			$find_ppk = $this->catatan_ppk->find("semester_id = $semester_id AND siswa_id = '$siswa_id'");
			if($find_ppk){
				$nama_siswa = get_nama_siswa($data['siswa_id']);
				$update = $this->catatan_ppk->update($find_ppk->catatan_ppk_id, $data);
				foreach($all_sikap_id as $key => $sikap_id){
					$insert_nilai_karakter = array(
						'catatan_ppk_id'	=> $find_ppk->catatan_ppk_id,
						'sekolah_id'		=> $loggeduser->sekolah_id,
						'semester_id'		=> $semester_id,
						'sikap_id'			=> $sikap_id,
						'deskripsi'			=> preg_replace('/(<br>)+$/', '', $deskripsi[$key]),
					);
					$find_nilai_karakter = $this->nilai_karakter->find("semester_id = $semester_id AND catatan_ppk_id = '$find_ppk->catatan_ppk_id' AND sikap_id = $sikap_id");
					if($find_nilai_karakter){
						$this->nilai_karakter->update($find_nilai_karakter->nilai_karakter_id, $insert_nilai_karakter);
					} else {
						$insert_nilai_karakter['nilai_karakter_id'] = gen_uuid();
						$insert_nilai_karakter['created_at']		= date('Y-m-d H:i:s');
						$insert_nilai_karakter['updated_at']		= date('Y-m-d H:i:s');
						$this->nilai_karakter->insert($insert_nilai_karakter);
					}
					//test($insert_nilai_karakter);
				}
				if($update){
					$this->session->set_flashdata('success', "Capaian PPK $nama_siswa berhasil diperbaharui");
				} else {
					$this->session->set_flashdata('error', "Capaian PPK $nama_siswa gagal diperbaharui. Silahkan coba beberapa saat lagi");
				}
			} else {
				$data['catatan_ppk_id'] = gen_uuid();
				if($this->catatan_ppk->insert($data)){
					foreach($all_sikap_id as $key => $sikap_id){
						$insert_nilai_karakter = array(
							'nilai_karakter_id'	=> gen_uuid(),
							'catatan_ppk_id'	=> $data['catatan_ppk_id'],
							'sekolah_id'		=> $loggeduser->sekolah_id,
							'semester_id'		=> $semester_id,
							'sikap_id'			=> $sikap_id,
							'deskripsi'			=> $deskripsi[$key],
							'created_at'		=> date('Y-m-d H:i:s'),
							'updated_at'		=> date('Y-m-d H:i:s')
						);
						$this->nilai_karakter->insert($insert_nilai_karakter);
						//test($insert_nilai_karakter);
					}
					$this->session->set_flashdata('success', 'Tambah capaian PPK berhasil disimpan');
				} else {
					$this->session->set_flashdata('error', 'Gagal menambah capaian PPK');
				}
			}
			//exit;
			redirect('admin/laporan/ppk', 'refresh');
		} else {
			$this->data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$this->data['siswa'] = $this->form_validation->set_value('siswa');
			$all_karakter = $this->sikap->find_all("sikap_id IN(1, 7, 19, 20, 21)");
			foreach($all_karakter as $karakter){
				$this->data['deskripsi'][$karakter->sikap_id] = array(
					'name'  => "deskripsi",
					'id'    => 'deskripsi',
					'type'  => 'text',
					'class' => "form-control required",
					'value' => $this->form_validation->set_value("deskripsi[$karakter->sikap_id]", ''),
				);
			}
			$this->data['capaian'] = $this->form_validation->set_value('capaian');
		}
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Tambah Data Penilaian Penguatan Pendidikan Karakter')
		->build($this->admin_folder.'/laporan/form_karakter', $this->data);
	}
	public function list_ppk(){
		$loggeduser = $this->ion_auth->user()->row();
		$nama_group = get_jabatan($loggeduser->id);
		$find_akses = get_akses($loggeduser->id);
		$ajaran = get_ta();
		$guru_id = $loggeduser->guru_id;
		//$guru_id = ($guru_id) ? $guru_id->id : 0;
		$cari_rombel = $this->rombongan_belajar->find("semester_id = $ajaran->id and guru_id = '$guru_id'");
		$id_rombel = ($cari_rombel) ? $cari_rombel->rombongan_belajar_id : 0;
		$where = '';
		if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
			$where = "WHERE rombongan_belajar_id = '$id_rombel'";
		}
		$search = "";
		$start = 0;
		$rows = 25;
		// get search value (if any)
		if (isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$search = $_GET['sSearch'];
		}

		// limit
		$start = get_start();
		$rows = get_rows();
		$search_form = "siswa_id IN(SELECT siswa_id FROM ref_siswa WHERE nama LIKE '%$search%') OR capaian LIKE '%$search%'";
		$query = $this->catatan_ppk->with('siswa')->find_all("semester_id =  $ajaran->id AND siswa_id IN(SELECT siswa_id FROM anggota_rombel $where) AND ($search_form)", '*','catatan_ppk_id desc', $start, $rows);
		$filter = $this->catatan_ppk->with('siswa')->find_count("semester_id =  $ajaran->id AND siswa_id IN(SELECT siswa_id FROM anggota_rombel $where) AND ($search_form)");
		$iFilteredTotal = count($query);
		$iTotal= $filter;
	    
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iTotal,
	        "aaData" => array()
	    );

	    // get result after running query and put it in array
		$i=$start;
		//$barang = $query->result();
		$check_2018 = check_2018();
	    foreach ($query as $temp) {
			if($check_2018){
				$link_edit = site_url('admin/laporan/edit_karakter/'.$temp->catatan_ppk_id);
			} else {
				$link_edit = site_url('admin/laporan/edit_ppk/'.$temp->catatan_ppk_id);
			}
			$record = array();
			$record[] = $temp->siswa->nama;
			$record[] = $temp->capaian;
			$record[] = '<div class="text-center"><div class="btn-group">
							<button type="button" class="btn btn-default btn-sm">Aksi</button>
                            <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu pull-right text-left" role="menu">
                                <!--li><a href="'.site_url('admin/asesmen/edit_ppk/'.$temp->catatan_ppk_id).'" class="toggle-modal"><i class="fa fa-eye"></i>Detil</a></li-->
								 <li><a href="'.$link_edit.'"><i class="fa fa-pencil"></i> Edit</a></li>
								 <li><a href="'.site_url('admin/laporan/hapus_ppk/'.$temp->catatan_ppk_id).'" class="confirm"><i class="fa fa-trash"></i> Hapus</a></li>
                            </ul>
                        </div></div>';
			$output['aaData'][] = $record;
		}
		// format it to JSON, this output will be displayed in datatable
		echo json_encode($output);
	}
	public function edit_karakter($id){
		$loggeduser = $this->ion_auth->user()->row();
		$catatan_ppk = $this->catatan_ppk->get($id);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('siswa_ppk', 'Peserta Didik', 'required');
		$this->form_validation->set_rules('deskripsi[1]', 'Religius', 'required');
		$this->form_validation->set_rules('deskripsi[7]', 'Mandiri', 'required');
		$this->form_validation->set_rules('deskripsi[19]', 'Integritas', 'required');
		$this->form_validation->set_rules('deskripsi[20]', 'Nasionalis', 'required');
		$this->form_validation->set_rules('deskripsi[21]', 'Gotong-royong', 'required');
		$this->form_validation->set_rules('capaian', 'Catatan Perkembangan Karakter', 'required');
		if ($this->form_validation->run() == true){
			$semester_id = $this->input->post('semester_id');
			$siswa_id = $this->input->post('siswa_ppk');
			$capaian = $this->input->post('capaian');
			$all_sikap_id = $this->input->post('sikap_id');
			$deskripsi = $this->input->post('deskripsi');
			$data = array(
				'semester_id' 		=> $semester_id,
				'siswa_id' 			=> $siswa_id,
				'capaian' 			=> preg_replace('/(<br>)+$/', '', $capaian)
			);
			if($this->catatan_ppk->update($id,$data)){
				foreach($all_sikap_id as $key => $sikap_id){
					$insert_nilai_karakter = array(
						'deskripsi'			=> preg_replace('/(<br>)+$/', '', $deskripsi[$key]),
					);
					$find_nilai_karakter = $this->nilai_karakter->find("semester_id = $semester_id AND catatan_ppk_id = '$id' AND sikap_id = $sikap_id");
					if($find_nilai_karakter){
						$this->nilai_karakter->update($find_nilai_karakter->nilai_karakter_id, $insert_nilai_karakter);
					}
				}
				$this->session->set_flashdata('success', 'Karakter berhasil diperbaharui');
			} else {
				$this->session->set_flashdata('error', 'Karakter gagal diperbaharui');
			}
			redirect('admin/laporan/ppk', 'refresh');
		} else {
			$this->data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$this->data['siswa'] = $this->form_validation->set_value('siswa', $catatan_ppk->siswa_id);
			$this->data['capaian'] = $this->form_validation->set_value('capaian', $catatan_ppk->capaian);
			//$this->data['ref_ppk_id'] = unserialize($catatan_ppk->ref_ppk_id);
			$all_karakter = $this->sikap->find_all("sikap_id IN(1, 7, 19, 20, 21)");
			foreach($all_karakter as $karakter){
				$find_karakter = $this->nilai_karakter->find("semester_id = $catatan_ppk->semester_id AND catatan_ppk_id = '$id' AND sikap_id = $karakter->sikap_id");
				$deskripsi = ($find_karakter) ? $find_karakter->deskripsi : '';
				$this->data['deskripsi'][$karakter->sikap_id] = array(
					'name'  => "deskripsi",
					'id'    => 'deskripsi',
					'type'  => 'text',
					'class' => "form-control required",
					'value' => $this->form_validation->set_value("deskripsi[$karakter->sikap_id]", $deskripsi),
				);
			}
			/*
			$this->data['tempat'] = array(
				'name'  => 'tempat',
				'id'    => 'tempat',
				'type'  => 'text',
				'class' => "form-control required",
				'value' => $this->form_validation->set_value('tempat', $catatan_ppk->tempat),
			);
			$this->data['penanggung_jawab'] = array(
				'name'  => 'penanggung_jawab',
				'id'    => 'penanggung_jawab',
				'type'  => 'text',
				'class' => "form-control required",
				'value' => $this->form_validation->set_value('penanggung_jawab', $catatan_ppk->penanggung_jawab),
			);
			$this->data['waktu'] = array(
				'name'  => 'waktu',
				'id'    => 'waktu',
				'type'  => 'text',
				'class' => "form-control required",
				'value' => $this->form_validation->set_value('waktu', $catatan_ppk->waktu),
			);
			$this->data['ppk_id'] = $this->form_validation->set_value('ppk_id', $catatan_ppk->ref_ppk_id);*/
		}
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Edit Data Penilaian Penguatan Pendidikan Karakter')
		->build($this->admin_folder.'/laporan/form_karakter', $this->data);
	}
	public function hapus_ppk($id){
		if($id){
			$catatan_ppk = $this->catatan_ppk->get($id);
			if(is_file(MEDIAFOLDER.$catatan_ppk->foto_1)){
				unlink(MEDIAFOLDER.$catatan_ppk->foto_1);
			}
			if(is_file(MEDIAFOLDER.$catatan_ppk->foto_2)){
				unlink(MEDIAFOLDER.$catatan_ppk->foto_2);
			}
			if(is_file(MEDIAFOLDER.$catatan_ppk->foto_3)){
				unlink(MEDIAFOLDER.$catatan_ppk->foto_3);
			}
			if($this->catatan_ppk->delete($id)){
				$output['title'] = 'Sukses';
				$output['text'] = 'Berhasil menghapus data PPK';
				$output['type'] = 'success';
			} else {
				$output['title'] = 'Gagal';
				$output['text'] = 'Gagal menghapus data PPK';
				$output['type'] = 'error';
			}
		} else {
			$output['title'] = 'Gagal';
			$output['text'] = 'Gagal menghapus data PPK';
			$output['type'] = 'error';
		}
		echo json_encode($output);
	}
	public function edit_ppk($id){
		$loggeduser = $this->ion_auth->user()->row();
		$catatan_ppk = $this->catatan_ppk->get($id);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('siswa_ppk', 'Peserta Didik', 'required');
		/*for($i=1; $i<=3; $i++){
			if (empty($_FILES['foto_'.$i]['name'])){
				$this->form_validation->set_rules('foto_'.$i, 'Foto '.$i, 'required');
			} else {
			//if(!empty($_FILES['foto_'.$i]['name'])){
				$upload_response = $this->upload_photo('foto_'.$i);
				if($upload_response['success']){
					$foto = 'foto_'.$i;
					$data['foto_'.$i]  = $upload_response['upload_data']['file_name'];
					if(is_file(MEDIAFOLDER.$catatan_ppk->$foto)){
						unlink(MEDIAFOLDER.$catatan_ppk->$foto);
					}
				}
				else{
					$this->session->set_flashdata('error', $upload_response['msg']);
				}
			}
		}*/
		if ($this->form_validation->run() == true){
			$semester_id = $this->input->post('semester_id');
			$siswa_id = $this->input->post('siswa_ppk');
			//$ref_ppk_id = serialize($this->input->post('ppk_id'));
			$capaian = $this->input->post('copy_editor');
			$capaian = find_img($capaian);
			$data = array(
				'semester_id' 		=> $semester_id,
				'siswa_id' 			=> $siswa_id,
				//'kegiatan' 			=> $this->input->post('kegiatan'),
				////'tempat' 			=> $this->input->post('tempat'),
				//'penanggung_jawab' 	=> $this->input->post('penanggung_jawab'),
				//'waktu' 			=> $this->input->post('waktu'),
				//'ref_ppk_id' 		=> $ref_ppk_id,
				'capaian' 			=> $capaian
			);
			if($this->catatan_ppk->update($id,$data)){
				$this->session->set_flashdata('success', 'Capaian PPK berhasil diperbaharui');
			} else {
				$this->session->set_flashdata('error', 'Gagal menambah capaian PPK');
			}
			redirect('admin/laporan/ppk', 'refresh');
		} else {
			$this->data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$this->data['siswa'] = $this->form_validation->set_value('siswa', $catatan_ppk->siswa_id);
			$this->data['capaian'] = $this->form_validation->set_value('capaian', $catatan_ppk->capaian);
			//$this->data['ref_ppk_id'] = unserialize($catatan_ppk->ref_ppk_id);
			/*$this->data['kegiatan'] = array(
				'name'  => 'kegiatan',
				'id'    => 'kegiatan',
				'type'  => 'text',
				'class' => "form-control required",
				'value' => $this->form_validation->set_value('kegiatan', $catatan_ppk->kegiatan),
			);
			$this->data['tempat'] = array(
				'name'  => 'tempat',
				'id'    => 'tempat',
				'type'  => 'text',
				'class' => "form-control required",
				'value' => $this->form_validation->set_value('tempat', $catatan_ppk->tempat),
			);
			$this->data['penanggung_jawab'] = array(
				'name'  => 'penanggung_jawab',
				'id'    => 'penanggung_jawab',
				'type'  => 'text',
				'class' => "form-control required",
				'value' => $this->form_validation->set_value('penanggung_jawab', $catatan_ppk->penanggung_jawab),
			);
			$this->data['waktu'] = array(
				'name'  => 'waktu',
				'id'    => 'waktu',
				'type'  => 'text',
				'class' => "form-control required",
				'value' => $this->form_validation->set_value('waktu', $catatan_ppk->waktu),
			);
			$this->data['ppk_id'] = $this->form_validation->set_value('ppk_id', $catatan_ppk->ref_ppk_id);*/
		}
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Edit Data Penilaian Penguatan Pendidikan Karakter')
		->build($this->admin_folder.'/laporan/form_ppk', $this->data);
	}
	public function add_ppk(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('siswa_ppk', 'Peserta Didik', 'required');
		if ($this->form_validation->run() == true){
			$semester_id = $this->input->post('semester_id');
			$siswa_id = $this->input->post('siswa_ppk');
			$capaian = $this->input->post('copy_editor');
			$capaian = find_img($capaian);
			$data = array(
				'semester_id' 		=> $semester_id,
				'siswa_id' 			=> $siswa_id,
				'capaian' 			=> $capaian
			);
			$find_ppk = $this->catatan_ppk->find("semester_id = $semester_id AND siswa_id = '$siswa_id'");
			if($find_ppk){
				$nama_siswa = get_nama_siswa($data['siswa_id']);
				$update = $this->catatan_ppk->update($find_ppk->catatan_ppk_id, $data);
				if($update){
					$this->session->set_flashdata('success', "Capaian PPK $nama_siswa berhasil diperbaharui");
				} else {
					$this->session->set_flashdata('error', "Capaian PPK $nama_siswa gagal diperbaharui. Silahkan coba beberapa saat lagi");
				}
			} else {
				$data['catatan_ppk_id'] = gen_uuid();
				if($this->catatan_ppk->insert($data)){
					$this->session->set_flashdata('success', 'Tambah capaian PPK berhasil disimpan');
				} else {
					$this->session->set_flashdata('error', 'Gagal menambah capaian PPK');
				}
			}
			redirect('admin/asesmen/ppk', 'refresh');
		} else {
			$this->data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$this->data['siswa'] = $this->form_validation->set_value('siswa');
			$this->data['capaian'] = $this->form_validation->set_value('capaian');
		}
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Tambah Data Penilaian Penguatan Pendidikan Karakter')
		->build($this->admin_folder.'/laporan/form_ppk', $this->data);
	}
	public function ukk(){
		$this->load->model('nilai_ukk_model', 'nilai_ukk');
		$loggeduser = $this->ion_auth->user()->row();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('guru_id', $loggeduser->guru_id)
		->set('page_title', 'Nilai Uji Kompetensi Keahlian')
		->set('form_action', 'admin/laporan/simpan_nilai_ukk')
		->build($this->admin_folder.'/laporan/list_ukk');
	}
	public function simpan_nilai_ukk(){
		$this->load->model('nilai_ukk_model', 'nilai_ukk');
		$ajaran_id = $this->input->post('ajaran_id');
		$rombongan_belajar_id = $this->input->post('rombel_id');
		$siswa_id = $this->input->post('siswa_id');
		$nilai_teori = $this->input->post('nilai_teori');
		$nilai_praktik = $this->input->post('nilai_praktik');
		$redirect = 'ukk';
		foreach($siswa_id as $k=>$siswa){
			array_walk($nilai_teori, 'check_great_than_one_fn',$redirect);
			array_walk($nilai_teori, 'check_numeric','laporan/'.$redirect);
			$nt = ($nilai_teori[$k]) ? $nilai_teori[$k] : 0;
			$np = ($nilai_praktik[$k]) ? $nilai_praktik[$k] : 0;
			$get_nilai_ukk = $this->nilai_ukk->find("semester_id = $ajaran_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND siswa_id = '$siswa'");
			$insert_nilai_ukk = array(
				'semester_id' 			=> $ajaran_id, 
				'rombongan_belajar_id'	=> $rombongan_belajar_id, 
				'siswa_id' 				=> $siswa,
				'nilai_teori'			=> $nt,
				'nilai_praktik'			=> $np
			);
			//test($insert_nilai_ukk);
			if($get_nilai_ukk){
				$this->nilai_ukk->update($get_nilai_ukk->nilai_ukk_id, $insert_nilai_ukk);
				$this->session->set_flashdata('success', 'Berhasil memperbaharui data nilai UKK');
			} else {
				$this->nilai_ukk->insert($insert_nilai_ukk);
				$this->session->set_flashdata('success', 'Berhasil menambah data nilai UKK');
			}
		}
		//test($_POST);
		redirect('admin/laporan/'.$redirect);
	}
}