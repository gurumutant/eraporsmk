<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Asesmen extends Backend_Controller {
	protected $activemenu = 'penilaian';
	public function __construct() {
		parent::__construct();
		$this->template->set('activemenu', $this->activemenu);
		$admin_group = array(1,2,3,5,6);
		hak_akses($admin_group);
	}
	public function index(){
		redirect('admin/asesmen/sikap');
	}
	public function sikap(){
		$check_2018 = check_2018();
		if($check_2018){
			$find = $this->sikap->get(19);
			if(!$find){
				$insert_sikap = array(
					array(
						'sikap_id' 		=> 19,
						'butir_sikap'	=> 'Integritas',
						'created_at'	=> date('Y-m-d H:i:s'),
						'updated_at'	=> date('Y-m-d H:i:s'),
					),
					array(
						'sikap_id' 		=> 20,
						'butir_sikap'	=> 'Nasionalis',
						'created_at'	=> date('Y-m-d H:i:s'),
						'updated_at'	=> date('Y-m-d H:i:s'),
					),
					array(
						'sikap_id' 		=> 21,
						'butir_sikap'	=> 'Gotong-royong',
						'created_at'	=> date('Y-m-d H:i:s'),
						'updated_at'	=> date('Y-m-d H:i:s'),
					),
				);
				$this->db->insert_batch('ref_sikap', $insert_sikap);
					
			}
		}
		$ajaran = get_ta();
		$loggeduser = $this->ion_auth->user()->row();
		$pilih_rombel = '<a href="'.site_url('admin/asesmen/add_sikap').'" class="btn btn-success" style="float:right;"><i class="fa fa-plus-circle"></i> Tambah Data</a>';
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('pilih_rombel', $pilih_rombel)
		->set('page_title', 'Tambah Data Penilaian Sikap')
		->set('form_action', 'admin/asesmen/simpan_sikap')
		->set('ajaran', $ajaran)
		->set('sekolah_id', $loggeduser->sekolah_id)
		->build($this->admin_folder.'/asesmen/list_sikap');
	}
	public function add_sikap(){
		$loggeduser = $this->ion_auth->user()->row();
		//$data['rombels'] = Datarombel::find('all', array('group' => 'tingkat','order'=>'tingkat ASC'));
		$check_2018 = check_2018();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Jurnal Sikap')
		->set('check_2018', $check_2018)
		->set('guru_id', $loggeduser->guru_id)
		->set('form_action', 'admin/asesmen/simpan_sikap')
		->build($this->admin_folder.'/asesmen/sikap');
	}
	public function pengetahuan(){
		//$data['rombels'] = Datarombel::find('all', array('group' => 'tingkat','order'=>'tingkat ASC'));
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Penilaian Pengetahuan')
		->set('form_action', 'admin/asesmen/simpan_nilai')
		->set('query', 'rencana_penilaian')
		->set('kompetensi_id', 1)
		->build($this->admin_folder.'/asesmen/form_asesmen');
	}
	public function keterampilan(){
		//$data['rombels'] = Datarombel::find('all', array('group' => 'tingkat','order'=>'tingkat ASC'));
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Penilaian Keterampilan')
		->set('form_action', 'admin/asesmen/simpan_nilai')
		->set('query', 'rencana_penilaian')
		->set('kompetensi_id', 2)
		->build($this->admin_folder.'/asesmen/form_asesmen');
	}
	public function remedial(){
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Penilaian Remedial')
		->set('form_action', 'admin/asesmen/simpan_remedial')
		->set('query', 'remedial')
		->set('kompetensi_id', 1)
		->build($this->admin_folder.'/asesmen/form_remedial');
	}
	public function get_ppk(){
		$semester_id = $this->input->post('semester_id');
		$siswa_id = $this->input->post('siswa_ppk');
		$find_ppk = $this->catatan_ppk->find("semester_id = $semester_id AND siswa_id = '$siswa_id'");
		$check_2018 = check_2018();
		if($check_2018){
			/*
			$all_karakter[] = (object) array('sikap_id' => 19, 'butir_sikap' => 'Integritas');
			$all_karakter[] = (object) array('sikap_id' => 1, 'butir_sikap' => 'Religius');
			$all_karakter[] = (object) array('sikap_id' => 20, 'butir_sikap' => 'Nasionalis');
			$all_karakter[] = (object) array('sikap_id' => 7, 'butir_sikap' => 'Mandiri');
			$all_karakter[] = (object) array('sikap_id' => 21, 'butir_sikap' => 'Gotong-royong');
			*/
			$catatan_ppk_id = ($find_ppk) ? $find_ppk->catatan_ppk_id : gen_uuid();
			$all_nilai_karakter = $this->nilai_karakter->find_all("catatan_ppk_id = '$catatan_ppk_id'");
			$output['capaian'] = ($find_ppk) ? $find_ppk->capaian : '';
			$output['nama_siswa'] = get_nama_siswa($siswa_id);
			if($all_nilai_karakter){
				foreach($all_nilai_karakter as $nilai_karakter){
					//$output['sugesti_'.$nilai_karakter->sikap_id] = $this->nilai_sikap->find_all("semester_id = $semester_id and siswa_id = '$siswa_id'");
					$all_sikap = $this->nilai_sikap->find_all("semester_id = $semester_id and siswa_id = '$siswa_id' AND butir_sikap = '$nilai_karakter->sikap_id'");
					if($all_sikap){
						foreach($all_sikap as $sikap){
							$a[$sikap->guru_id][] = butir_sikap($sikap->butir_sikap).' = '.$sikap->uraian_sikap.' ('.opsi_sikap($sikap->opsi_sikap,1).')';
							//$ajaran_id[$sikap->mata_pelajaran_id] = $sikap->semester_id;
							//$rombel_id[$sikap->mata_pelajaran_id] = $sikap->rombongan_belajar_id;
						}
						$nama_mapel = '';
						foreach($a as $b=>$c){
							//$nama_mapel .= '<p>'.get_nama_mapel($b).'</p>';
							$nama_mapel .= '<p>'.get_nama_guru($b).'</p>';
							$nama_mapel .= '<ul>';
							foreach($c as $d){
								$nama_mapel .= '<li>'.$d.'</li>';
							}
							$nama_mapel .= '</ul>';
						}
					} else {
						//echo 'Nilai sikap belum di entri';
						$nama_mapel = 'Nilai sikap belum di entri';
					}
					$output['sugesti_'.$nilai_karakter->sikap_id] = $nama_mapel;
					$output['sikap_id_'.$nilai_karakter->sikap_id] = $nilai_karakter->deskripsi;
				}
			} else {
				$output['sugesti_1']		= '';
				$output['sugesti_7']		= '';
				$output['sugesti_19']		= '';
				$output['sugesti_20']		= '';
				$output['sugesti_21']		= '';
				$output['sikap_id_1'] 		= '';
				$output['sikap_id_7'] 		= '';
				$output['sikap_id_19'] 		= '';
				$output['sikap_id_20'] 		= '';
				$output['sikap_id_21'] 		= '';
			}
			echo json_encode($output);
		} else {
			echo ($find_ppk) ? $find_ppk->capaian : '';
		}
	}
	public function simpan_remedial(){
		//test($_POST);
		//die();
		$ajaran_id = $_POST['ajaran_id'];
		$rombel_id = $_POST['rombel_id'];
		$kompetensi_id = $_POST['kompetensi_id'];
		$id_mapel = $_POST['id_mapel'];
		$data_siswa = $_POST['siswa_id'];
		$rerata = $_POST['rerata'];
		$rerata_akhir = $_POST['rerata_akhir'];
		$rerata_remedial = $_POST['rerata_remedial'];
		foreach($data_siswa as $k=>$siswa){
			if($rerata_remedial[$k]){
				foreach($rerata as $rata){
					array_walk($rata, 'check_great_than_one_fn','remedial');
					array_walk($rata, 'check_numeric','asesmen/remedial');
				}
				$remedial = $this->remedial->find("semester_id = $ajaran_id AND kompetensi_id = $kompetensi_id AND rombongan_belajar_id = '$rombel_id' AND mata_pelajaran_id = $id_mapel AND siswa_id = '$siswa'");
				if($remedial){
					$data_update = array(
						'nilai' 			=> serialize($rerata[$siswa]),
						'rerata_akhir' 		=> $rerata_akhir[$k],
						'rerata_remedial' 	=> $rerata_remedial[$k]
					);
					$this->remedial->update($remedial->remedial_id, $data_update);
					$this->session->set_flashdata('success', 'Berhasil memperbaharui data nilai remedial.');
				} else {
					$data_insert = array(
						'remedial_id'			=> gen_uuid(),
						'semester_id'			=> $ajaran_id,
						'kompetensi_id'			=> $kompetensi_id,
						'rombongan_belajar_id'	=> $rombel_id,
						'mata_pelajaran_id'		=> $id_mapel,
						'siswa_id'				=> $siswa,
						'nilai'					=> serialize($rerata[$siswa]),
						'rerata_akhir'			=> $rerata_akhir[$k],
						'rerata_remedial'		=> $rerata_remedial[$k],
					);
					foreach ($rerata[$siswa] as $key => $value) {
						if (empty($value)) {
							unset($rerata[$siswa][$key]);
						}
					}
					if ($rerata[$siswa]) {
						//echo 'not_empty'.$siswa.'<br />';
					//}
					//if($rerata[$siswa]){
						$this->remedial->insert($data_insert);
					}
					$this->session->set_flashdata('success', 'Tambah data nilai remedial berhasil.');
				}
			} //else {
				//$this->session->set_flashdata('error', 'Tidak ada perubahan data. Remedial tidak disimpan.');
			//}
		}
		redirect('admin/asesmen/remedial');
	}
	public function get_remedial(){
		$html = '';
		$settings 	= $this->settings->get(1);
		$ajaran_id = $_POST['ajaran_id'];
		$rombel_id	= $_POST['rombel_id'];
		kunci_nilai($rombel_id);
		$id_mapel = $_POST['id_mapel'];
		$kelas = $_POST['kelas'];
		$aspek = $_POST['aspek'];
		$start = isset($_POST['start']) ? $_POST['start'] : 0;
		$rows = 10;
		$kompetensi_id = ($aspek == 'P') ? 1 : 2;
		$find_kd_nilai = $this->kompetensi_dasar->find_all("id IN (SELECT kd_id FROM kd_nilai WHERE deleted_at IS NULL AND rencana_penilaian_id IN (SELECT rencana_penilaian_id FROM rencana_penilaian WHERE semester_id = $ajaran_id AND mata_pelajaran_id = $id_mapel AND rombongan_belajar_id = '$rombel_id' AND kompetensi_id = $kompetensi_id AND deleted_at IS NULL))");
		//$find_kd_nilai = $this->kompetensi_dasar->find_all("id IN (SELECT kd_id FROM kd_nilai WHERE rencana_penilaian_id IN (SELECT rencana_penilaian_id FROM rencana_penilaian WHERE semester_id = $ajaran_id AND mata_pelajaran_id = $id_mapel AND rombongan_belajar_id = '$rombel_id' AND kompetensi_id = $kompetensi_id AND deleted_at IS NULL))");
		//test($find_kd_nilai);
		//$find_kd_nilai = $this->kd_nilai->find_all("rencana_penilaian_id IN (SELECT rencana_penilaian_id FROM rencana_penilaian WHERE semester_id = $ajaran_id AND mata_pelajaran_id = $id_mapel AND rombongan_belajar_id = '$rombel_id' AND kompetensi_id = $kompetensi_id) GROUP BY kd_id", 'kd_nilai_id, kd_id');
		$nama_mapel = get_nama_mapel($id_mapel);
		$data_siswa = filter_agama_siswa($nama_mapel,$rombel_id,$start,$rows);
		//test($data_siswa);
		//die();
		$count_get_all_kd = count($find_kd_nilai);
		$kkm = get_kkm($ajaran_id, $rombel_id, $id_mapel);
		$html .= '<input type="hidden" name="kompetensi_id" value="'.$kompetensi_id.'" />';
		$html .= '<input type="hidden" id="get_kkm" value="'.$kkm.'" />';
		$html .= '<div class="table-responsive no-padding">';
		$html .= '<div class="row"><div class="col-md-6">';
		$html .= '<table class="table table-bordered">';
		$html .= '<tr>';
		$html .= '<td colspan="2" class="text-center">';
		$html .= '<strong>Keterangan</strong>';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td width="30%">';
		$html .= 'SKM';
		$html .= '</td>';
		$html .= '<td>';
		$html .= $kkm;
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>';
		$html .= '<input type="text" class="bg-red form-control input-sm" />';
		$html .= '</td>';
		$html .= '<td>';
		$html .= 'Tidak tuntas (input aktif)';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>';
		$html .= '<input type="text" class="bg-green form-control input-sm" />';
		$html .= '</td>';
		$html .= '<td>';
		$html .= 'Tuntas (input non aktif)';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '</div></div>';
		$html .= '<table class="table table-bordered table-hover" id="remedial">';
		$html .= '<thead>';
		$html .= '<tr>';
		$html .= '<th rowspan="2" style="vertical-align: middle;">Nama Peserta Didik</th>';
		$html .= '<th class="text-center" colspan="'.count($find_kd_nilai).'">Kompetensi Dasar</th>';
		$html .= '<th rowspan="2" style="vertical-align: middle;" class="text-center">Rerata Akhir</th>';
		$html .= '<th rowspan="2" style="vertical-align: middle;" class="text-center">Rerata Remedial</th>';
		$html .= '<th rowspan="2" style="vertical-align: middle;" class="text-center">Hapus</th>';
		$html .= '</tr>';
		$html .= '<tr>';
		$pembagi = count($find_kd_nilai);
		$id_kds = '';
		if($find_kd_nilai){
			$html .= '<script>$("a#rerata_remedial").show();</script>';
			foreach($find_kd_nilai as $kd_nilai){
				$kompetensi_dasar = ($kd_nilai->kompetensi_dasar_alias) ? $kd_nilai->kompetensi_dasar_alias : $kd_nilai->kompetensi_dasar;
				$id_kds[] = $kd_nilai->id;
				$html .= '<th><a href="javacript:void(0)" class="tooltip-left" title="'.trim(strip_tags($kompetensi_dasar)).'">&nbsp;&nbsp;&nbsp;'.$kd_nilai->id_kompetensi.'&nbsp;&nbsp;&nbsp;</a></th>';
			}
		} else {
			$html .= '<script>$("a#rerata_remedial").hide();</script>';
		}
		$html .= '</tr>';
		$html .= '</thead>';
		$html .= '<tbody>';
		$no=0;
		foreach($data_siswa['data'] as $siswa){
			$siswa_id = $siswa->siswa_id;
			$remedial = $this->remedial->find("semester_id = $ajaran_id AND kompetensi_id = $kompetensi_id AND rombongan_belajar_id = '$rombel_id' AND mata_pelajaran_id = $id_mapel AND siswa_id = '$siswa_id'");
			$html .= '<input type="hidden" name="siswa_id[]" value="'.$siswa_id.'" />';
			$html .= '<tr>';
			$html .= '<td>';
			$html .= get_nama_siswa($siswa_id);
			$html .= '</td>';
			if($remedial){
				$all_nilai = unserialize($remedial->nilai);
				//test($all_nilai);
				$set_nilai = 0;
				foreach($all_nilai as $key=>$nilai){
					$set_nilai += $nilai;
					if($nilai && $kkm > number_format($nilai)){
						$aktif = '';
						$bg = 'bg-red';
					} else {
						$aktif = 'readonly';
						$bg = 'bg-green';
					}
					$html .= '<td class="text-center">';
					if($nilai){
						$html .= '<input type="text" name="rerata['.$siswa_id.'][]" size="10" class="'.$bg.' form-control input-sm" value="'.number_format($nilai,0).'" '.$aktif.' />';
					} else {
						$html .= '<input type="hidden" name="rerata['.$siswa_id.'][]" value="0" />-';
					}
					$html .= '</td>';
				}
				$rerata_remedial = number_format($set_nilai / count($all_nilai), 0);
				if($kkm > $remedial->rerata_akhir){
					$bg_rerata_akhir = 'text-red';
				} else {
					$bg_rerata_akhir = 'text-green';
				}
				if($kkm > $rerata_remedial){
					$bg_rerata_remedial = 'text-red';
				} else {
					$bg_rerata_remedial = 'text-green';
				}
				$html .= '<td id="rerata_akhir" class="text-center '.$bg_rerata_akhir.'"><strong>';
				$html .= '<input type="hidden" id="rerata_akhir_input" name="rerata_akhir[]" value="'.$remedial->rerata_akhir.'" />';
				$html .= $remedial->rerata_akhir;
				$html .= '</strong></td>';
				$html .= '<td id="rerata_remedial" class="text-center '.$bg_rerata_remedial.'"><strong>';
				$html .= '<input type="hidden" id="rerata_remedial_input" name="rerata_remedial[]" value="'.$rerata_remedial.'" />';
				//$html .= $remedial->rerata_remedial;
				$html .= $rerata_remedial;
				$html .= '</td>';
				$html .= '<td class="text-center"><a href="'.site_url('admin/asesmen/delete_remedial/'.$remedial->remedial_id).'" class="confirm_remed btn btn-sm btn-danger"><i class="fa fa-trash"></i></td>';
			} elseif($id_kds){
				$set_rerata = 0;
				foreach($id_kds as $id_kd){
					$all_nilai = $this->nilai->with('rencana_penilaian')->find_all("semester_id = $ajaran_id AND kompetensi_id = $kompetensi_id AND siswa_id = '$siswa_id' AND kompetensi_dasar_id = $id_kd AND rencana_penilaian_id IN (SELECT rencana_penilaian_id FROM rencana_penilaian WHERE semester_id = $ajaran_id AND kompetensi_id = $kompetensi_id AND mata_pelajaran_id = $id_mapel AND rombongan_belajar_id = '$rombel_id') AND deleted_at IS NULL");// AND nilai > 0
					if($all_nilai){
						if($kompetensi_id == 1){
							$nilai_kd = 0;
							$total_bobot = 0;
							foreach($all_nilai as $set_nilai){
								$bobot_kd = ($set_nilai->rencana_penilaian) ? $set_nilai->rencana_penilaian->bobot : 1;
								//$nilai_kd += $set_nilai->nilai * $set_nilai->rencana_penilaian->bobot;
								//$total_bobot += $set_nilai->rencana_penilaian->bobot;
								$nilai_kd += $set_nilai->nilai * $bobot_kd;
								$total_bobot += $bobot_kd;
							}
							$nilai_remedial = ($nilai_kd / $total_bobot); 
						} else {
							$nilai_kd = 0;
							$nilai_siswa = array();
							foreach($all_nilai as $set_nilai){
								$bobot_kd = ($set_nilai->rencana_penilaian) ? $set_nilai->rencana_penilaian->bobot : 1;
								if(is_array($set_nilai->rencana_penilaian)){
									 $metode_id = $set_nilai->rencana_penilaian->metode_id;
								} else {
									$metode_id = GenerateID();
								}
								$nilai_siswa[$set_nilai->kompetensi_dasar_id.'_'.$metode_id][] = $set_nilai->nilai;
								$set_bobot_baru[$set_nilai->kompetensi_dasar_id.'_'.$metode_id][] = $bobot_kd;
							}
							ksort($nilai_siswa, SORT_NUMERIC);
							$n_s_final = 0;
							$total_bobot = 0;
							foreach($nilai_siswa as $key => $n_s){
								$total_bobot += $set_bobot_baru[$key][0];
								if(count($n_s) > 1){
									$nilai_kd += max($n_s) * $total_bobot;
								} else {
									$nilai_kd += $n_s[0] * $set_bobot_baru[$key][0];
								}
							}
							$nilai_remedial = ($nilai_kd / $total_bobot);
						}
						if($kkm > number_format($nilai_remedial,0)){
							$aktif = '';
							$bg = 'bg-red';
						} else {
							$aktif = 'readonly';
							$bg = 'bg-green';
						}
						$set_rerata += $nilai_remedial;
						$nilai = '<input type="text" name="rerata['.$siswa->siswa_id.'][]" size="10" class="'.$bg.' form-control input-sm" value="'.number_format($nilai_remedial,0).'" '.$aktif.' />';
					} else {
						$nilai = '<input type="hidden" name="rerata['.$siswa->siswa_id.'][]" value="0" />-';
					}
					$html .= '<td class="text-center">';
					$html .= $nilai;
					$html .= '</td>';
				}
				if($set_rerata){
					$rerata_akhir = number_format($set_rerata / $pembagi,0);
					$rerata_remedial = 0;
				} else {
					$rerata_akhir = '';
					$rerata_remedial = '';
				}
				if($kkm > $rerata_akhir){
					$bg_rerata_akhir = 'text-red';
				} else {
					$bg_rerata_akhir = 'text-green';
				}
				if($kkm > $rerata_remedial){
					$bg_rerata_remedial = 'text-red';
				} else {
					$bg_rerata_remedial = 'text-green';
				}
				$html .= '<td id="rerata_akhir" class="text-center '.$bg_rerata_akhir.'"><strong>';
				$html .= '<input type="hidden" id="rerata_akhir_input" name="rerata_akhir[]" value="'.$rerata_akhir.'" />';
				$html .= $rerata_akhir;
				$html .= '</strong></td>';
				$html .= '<td id="rerata_remedial" class="text-center '.$bg_rerata_remedial.'"><strong>';
				$html .= '<input type="hidden" id="rerata_remedial_input" name="rerata_remedial[]" value="'.$rerata_remedial.'" />';
				$html .= $rerata_remedial;
				$html .= '</strong></td>';
				$html .= '<td class="text-center">-</td>';
			} else {
				$html .= '<td class="text-center">-</td>';
				$html .= '<td class="text-center">-</td>';
				$html .= '<td class="text-center">-</td>';
				$html .= '<td class="text-center">-</td>';
			}
			$html .= '</tr>';
		}
		$html .= '<input type="hidden" id="get_all_kd" value="'.$pembagi.'" />';
		$html .= '</tbody>';
		$html .= '</table>';
		$html .= '</div>';
		$html .= '<div class="row">';
		$html .= '<div class="col-xs-6">Menampilkan '.$data_siswa['dari'].' sampai '.$data_siswa['ke'].' dari total '.$data_siswa['count'].' data</div>';
		$html .= '<div class="col-xs-6">';
		$html .= '<div class="dataTables_paginate paging_bootstrap">';
		$html .= '<ul class="pagination">';
		$start = ($start) ? $start : 1;
		for($i=1;$i<=$data_siswa['page'];$i++){
			if($i == $start){
				$class= ' class="active"';
			} else {
				$class = '';
			}
			$html .= '<li'.$class.'><a class="next_page" data-aksi="'.site_url('admin/asesmen/get_remedial').'" href="'.site_url('admin/ajax/get_next_siswa/'.$rombel_id.'/'.$i).'/'.$ajaran_id.'/'.$id_mapel.'/'.$kelas.'/'.$aspek.'">'.$i.'</a></li>';
		}
		//$html .='<li class="prev disabled"><a href="#">&laquo; Sebelumnya</a></li><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li class="next"><a href="#">Selanjutnya &raquo;</a></li>
		$html .= '</ul>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= link_tag('assets/css/tooltip-viewport.css', 'stylesheet', 'text/css');
		$html .= '<script src="'.base_url().'assets/js/tooltip-viewport.js"></script>';
		$html .= '<script src="'.base_url().'assets/js/remedial.js"></script>';
		$html .= '<script src="'.base_url().'assets/js/delete_remed.js"></script>';
		echo $html;
	}
	public function delete_remedial($id){
		$r = $this->remedial->get($id);
		$nilai = $this->nilai->find_all("semester_id = $r->semester_id AND kompetensi_id = $r->kompetensi_id AND rombongan_belajar_id = '$r->rombongan_belajar_id' AND mata_pelajaran_id = $r->mata_pelajaran_id AND siswa_id = '$r->siswa_id'");
		$kkm = get_kkm($r->semester_id, $r->rombongan_belajar_id, $r->mata_pelajaran_id);
		$status['nama_siswa'] = get_nama_siswa($r->siswa_id);
		$nilai_akhir = 0;
		foreach($nilai as $n){
			if($kkm > $n->nilai){
				$class = 'bg-red';
			} else {
				$class = 'bg-green';
			}
			$nilai_akhir += $n->nilai;
			$record= array();
			$record['class'] = $class;
			$record['siswa_id'] = $n->siswa_id;
			$record['nilai'] = $n->nilai;
			$status['nilai'][] 	= $record;
		}
		$rerata_akhir = number_format($nilai_akhir / count($nilai),0);
		if($kkm > $rerata_akhir){
			$status['bg_rerata_akhir'] = 'text-red';
		} else {
			$status['bg_rerata_akhir'] = 'text-green';
		}
		$status['rerata_akhir'] = $rerata_akhir;
		if($this->remedial->delete($id)){
			$status['type'] = 'success';
			$status['text'] = 'Data berhasil dihapus';
			$status['title'] = 'Data Terhapus!';
		} else {
			$status['type'] = 'error';
			$status['text'] = 'Coba beberapa saat lagi';
			$status['title'] = 'Data tidak terhapus!';
		}
		echo json_encode($status);
	}
	public function deskripsi_mapel(){
		$ajaran = get_ta();
		$loggeduser = $this->ion_auth->user()->row();
		$siswa = $this->siswa->get($loggeduser->siswa_id);
		if($siswa){
			$data['ajaran']	= $ajaran->tahun.' Semester '.$ajaran->smt;
			$data['deskripsi'] = Deskripsi::find_all_by_ajaran_id_and_siswa_id($ajaran->id, $siswa->id);
			$this->template->title('Administrator Panel')
			->set_layout($this->admin_tpl)
			->set('page_title', 'Deskripsi per Mata Pelajaran')
			->build($this->admin_folder.'/asesmen/catatan_siswa',$data);
		} else {
			//$data['ajarans'] = Ajaran::all();
			//$data['rombels'] = Datarombel::find('all', array('group' => 'tingkat','order'=>'tingkat ASC'));
			$this->template->title('Administrator Panel')
			->set_layout($this->admin_tpl)
			->set('page_title', 'Deskripsi per Mata Pelajaran')
			->set('form_action', 'admin/asesmen/simpan_deskripsi_mapel')
			->build($this->admin_folder.'/asesmen/deskripsi_mapel');
		}
	}
	public function simpan_deskripsi_mapel(){
		$ajaran_id = $_POST['ajaran_id'];
		$rombel_id = $_POST['rombel_id'];
		$mapel_id = $_POST['id_mapel'];
		$siswa_id = $_POST['siswa_id'];
		foreach($siswa_id as $key=>$siswa){
			//$deskripsi_pengetahuan = Deskripsi::find_by_ajaran_id_and_rombel_id_and_mapel_id_and_siswa_id($ajaran_id,$rombel_id,$mapel_id,$siswa);
			if($_POST['deskripsi_pengetahuan'][$key] || $_POST['deskripsi_keterampilan'][$key]){
				$deskripsi_mapel = $this->deskripsi_mata_pelajaran->find("semester_id = $ajaran_id and rombongan_belajar_id = '$rombel_id' and mata_pelajaran_id = $mapel_id and siswa_id = '$siswa'");
				if($deskripsi_mapel){
					$data_update = array(
						'deskripsi_pengetahuan' => $_POST['deskripsi_pengetahuan'][$key],
						'deskripsi_keterampilan' => $_POST['deskripsi_keterampilan'][$key],
					);
					$this->deskripsi_mata_pelajaran->update($deskripsi_mapel->deskripsi_mata_pelajaran_id, $data_update);
					//$this->session->set_flashdata('error', 'Terdeteksi data existing. Data di update!');
					$this->session->set_flashdata('success', 'Berhasil memperbaharui deskripsi per mata pelajaran');
				} else {
					$data_insert = array(
						'semester_id' 				=> $ajaran_id,
						'rombongan_belajar_id'		=> $rombel_id,
						'mata_pelajaran_id'			=> $mapel_id,
						'siswa_id'					=> $siswa,
						'deskripsi_pengetahuan'		=> $_POST['deskripsi_pengetahuan'][$key],
						'deskripsi_keterampilan'	=> $_POST['deskripsi_keterampilan'][$key],
					);
					$this->deskripsi_mata_pelajaran->insert($data_insert);
					$this->session->set_flashdata('success', 'Berhasil menambah deskripsi per mata pelajaran');
				}
			}
		}
		redirect('admin/asesmen/deskripsi_mapel');
	}
	public function get_prakerin(){
		$data['ajaran_id'] = $_POST['ajaran_id'];
		$data['rombel_id'] = $_POST['rombel_id'];
		$data['siswa_id'] = $_POST['siswa_id'];
		$data['check_2018'] = check_2018();
		$this->template->title('Administrator Panel')
		->set_layout($this->blank_tpl)
		->set('page_title', 'Entry Absensi')
		->build($this->admin_folder.'/laporan/add_prakerin',$data);
	}
	public function get_prestasi(){
		$data['ajaran_id'] = $_POST['ajaran_id'];
		$data['rombel_id'] = $_POST['rombel_id'];
		$data['siswa_id'] = $_POST['siswa_id'];
		$data['check_2018'] = check_2018();
		$this->template->title('Administrator Panel')
		->set_layout($this->blank_tpl)
		->set('page_title', 'Entry Absensi')
		->build($this->admin_folder.'/laporan/add_prestasi',$data);
	}
	function form_sikap($ajaran_id,$rombel_id,$mapel_id,$siswa_id){
		$loggeduser = $this->ion_auth->user()->row();
		$ajaran = get_ta();
		if(is_int($mapel_id)){
			$is_guru_sikap = is_guru_sikap($loggeduser->guru_id, $ajaran->id);
			if($is_guru_sikap){
				$data['all_sikap'] = $this->nilai_sikap->find_all("semester_id = $ajaran_id and siswa_id = '$siswa_id'");
			} else {
				$data['all_sikap'] = $this->nilai_sikap->find_all("semester_id = $ajaran_id and rombongan_belajar_id = '$rombel_id' and mata_pelajaran_id = $mapel_id and siswa_id = '$siswa_id'");
			}
		} else {
			$is_guru_sikap = is_guru_sikap($mapel_id, $ajaran->id);
			if($is_guru_sikap){
				$data['all_sikap'] = $this->nilai_sikap->find_all("semester_id = $ajaran_id and siswa_id = '$siswa_id'");
			} else {
				$data['all_sikap'] = $this->nilai_sikap->find_all("semester_id = $ajaran_id and rombongan_belajar_id = '$rombel_id' and siswa_id = '$siswa_id' and guru_id = '$mapel_id'");
			}
		}
		//Sikap::find_all_by_ajaran_id_and_rombel_id_and_mapel_id_and_siswa_id($ajaran_id,$rombel_id,$mapel_id,$siswa_id);
		//$data['data_sikap'] = Datasikap::find_all_by_ajaran_id($ajaran->id);
		$check_2018 = check_2018();
		$data['check_2018'] = $check_2018;
		if($check_2018){
			//1,7,19,20,21
			$all_karakter[] = (object) array('sikap_id' => 19, 'butir_sikap' => 'Integritas');
			$all_karakter[] = (object) array('sikap_id' => 1, 'butir_sikap' => 'Religius');
			$all_karakter[] = (object) array('sikap_id' => 20, 'butir_sikap' => 'Nasionalis');
			$all_karakter[] = (object) array('sikap_id' => 7, 'butir_sikap' => 'Mandiri');
			$all_karakter[] = (object) array('sikap_id' => 21, 'butir_sikap' => 'Gotong-royong');
			$data['data_sikap'] = $all_karakter;
			//$this->sikap->find_all("sikap_id IN (19,1,20,7,21)");
		} else {
			$data['data_sikap'] = $this->sikap->find_all("sikap_id NOT IN (1,7,19,20,21)");
		}
		$data['mapel_id'] = $mapel_id;
		$data['loggeduser'] = $loggeduser;
		$data['is_guru_sikap'] = $is_guru_sikap;
		$this->template->title('')
		->set_layout($this->blank_tpl)
		->set('page_title', '')
		->build($this->admin_folder.'/asesmen/form_sikap',$data);
	}
	public function get_sikap(){
		$ajaran_id = $_POST['ajaran_id'];
		$rombel_id = $_POST['rombel_id'];
		$mapel_id = $_POST['id_mapel'];
		$siswa_id = $_POST['siswa_id'];
		$this->form_sikap($ajaran_id,$rombel_id,$mapel_id,$siswa_id);
	}
	public function edit_sikap($id){
		if($_POST){
			$update_attributes = array(
				'butir_sikap' => $_POST['butir_sikap'], 
				'opsi_sikap' => $_POST['opsi_sikap'], 
				'uraian_sikap' => $_POST['uraian_sikap']
			);
			$sikap = $this->nilai_sikap->update($id, $update_attributes);
			//Sikap::find_by_id($id);
			$ajaran_id = $_POST['ajaran_id'];
			$rombel_id = $_POST['rombel_id'];
			$siswa_id = $_POST['siswa_id'];
			$mapel_id = $_POST['mapel_id'];
			//$ajaran_id,$rombel_id,$mapel_id,$siswa_id
			$this->form_sikap($ajaran_id,$rombel_id,$mapel_id, $siswa_id);
		} else {
			$ajaran = get_ta();
			$sikap = $this->nilai_sikap->get($id);
			//Sikap::find_by_id($id);
			$data_sikap = $this->sikap->get_all();
			//Datasikap::all();
			$this->template->title('Administrator Panel : Edit Sikap')
			->set_layout($this->modal_tpl)
			->set('page_title', 'Edit Sikap')
			->set('sikap', $sikap)
			->set('data_sikap', $data_sikap)
			->set('modal_footer', '<a class="btn btn-primary" id="button_form" href="javascript:void(0);">Update</a>')
			->build($this->admin_folder.'/asesmen/edit_sikap');
		}
	}
	public function simpan_sikap(){
		$ajaran_id		= $_POST['ajaran_id'];
		$rombel_id		= $_POST['rombel_id'];
		$mapel_id		= $_POST['mapel_id'];
		$siswa_id		= $_POST['siswa_id'];
		$tanggal_sikap	= $_POST['tanggal_sikap'];
		//$tanggal_sikap	= str_replace('/','-',$tanggal_sikap);
		$tanggal_sikap	= date('Y-m-d', strtotime($tanggal_sikap));
		$butir_sikap	= $_POST['butir_sikap'];
		$opsi_sikap		= $_POST['opsi_sikap'];
		$uraian_sikap	= $_POST['uraian_sikap'];
		$check_2018 = check_2018();
		if($check_2018){
			$sikap = $this->nilai_sikap->find_all("semester_id = $ajaran_id and rombongan_belajar_id = '$rombel_id' and guru_id = '$mapel_id' and siswa_id = '$siswa_id' AND butir_sikap = '$butir_sikap'");
		} else {
			$sikap = $this->nilai_sikap->find_all("semester_id = $ajaran_id and rombongan_belajar_id = '$rombel_id' and mata_pelajaran_id = $mapel_id and siswa_id = '$siswa_id' AND butir_sikap = '$butir_sikap'");
		}
		//Sikap::find_by_ajaran_id_and_rombel_id_and_mapel_id_and_siswa_id_and_butir_sikap($ajaran_id,$rombel_id, $mapel_id, $siswa_id,$butir_sikap);
		if($sikap){
			/*$sikap->ajaran_id		= $ajaran_id;
			$sikap->rombel_id		= $rombel_id;
			$sikap->siswa_id		= $siswa_id;
			$sikap->tanggal_sikap	= $tanggal_sikap;
			$sikap->butir_sikap		= $butir_sikap;
			$sikap->uraian_sikap	= $uraian_sikap;
			$sikap->save();*/
			$this->session->set_flashdata('error', 'Terdeteksi data existing');
		} else {
			if($check_2018){
				$attributes = array(
								'nilai_sikap_id'	=> gen_uuid(),
								'semester_id'	=> $ajaran_id,
								'rombongan_belajar_id'	=> $rombel_id,
								'guru_id'	=> $mapel_id,
								'siswa_id'	=> $siswa_id,
								'tanggal_sikap' => $tanggal_sikap,
								'butir_sikap'	=> $butir_sikap,
								'opsi_sikap'		=> $opsi_sikap,
								'uraian_sikap' => $uraian_sikap,
				);
			} else {
				$attributes = array(
								'nilai_sikap_id'	=> gen_uuid(),
								'semester_id'	=> $ajaran_id,
								'rombongan_belajar_id'	=> $rombel_id,
								'mata_pelajaran_id'	=> $mapel_id,
								'siswa_id'	=> $siswa_id,
								'tanggal_sikap' => $tanggal_sikap,
								'butir_sikap'	=> $butir_sikap,
								'opsi_sikap'		=> $opsi_sikap,
								'uraian_sikap' => $uraian_sikap,
				);
			}
			$this->nilai_sikap->insert($attributes);
			$this->session->set_flashdata('success', 'Berhasil menambah data sikap');
		}
		redirect('admin/asesmen/sikap');
	}
	public function get_rerata(){
		$siswa_id = $_POST['siswa_id'];
		$jumlah_kd = $_POST['jumlah_kd'];
		$bobot = $_POST['bobot_kd'];
		$all_bobot = $_POST['all_bobot'];
		$kompetensi_id = $_POST['kompetensi_id'];
		$kds = $_POST['kd'];
			$total_bobot = 0;
			foreach($all_bobot as $allbobot){
				$total_bobot += $allbobot;
			}
			$total_bobot = ($total_bobot > 0) ? $total_bobot : 1;
			$bobot = ($bobot > 0) ? $bobot : 1;
			$output['jumlah_form'] = count($siswa_id);
			foreach($siswa_id as $k=>$siswa){
				$hitung = 0;
				foreach($kds as $kd){
					$hitung += $kd[$k];
				}
				$hasil = $hitung/$jumlah_kd;
				$rerata_nilai = $hasil*$bobot;//($hasil*$bobot)/$total_bobot;
				$rerata_jadi = number_format($rerata_nilai/$total_bobot,2);
				$record['value'] 	= number_format($hitung/$jumlah_kd,0);
				//=F6*(C4/(C4+G4+J4+M4))
				$record['rerata_text'] 	= 'x '.$bobot.' / '.$total_bobot.' =';
				$record['rerata_jadi'] 	= $rerata_jadi;
				$output['rerata'][] = $record;
			}
			$settings 	= $this->settings->get(1);
			$html = '';
			//if($settings->rumus == 1){
				//$html .= '<p><strong>Rumus nilai akhir per penilaian: <br />Rerata * Bobot Penilaian / Total bobot penilaian per mapel</strong></p>';
				//$html .= '<p>Keterangan: <br />Bobot : '.$bobot.'<br />Total Bobot : '.$total_bobot.'</p>';
			//}
			$output['rumus'] = '';//$html;
		/*} else {
			$output['jumlah_form'] = count($siswa_id);
			foreach($siswa_id as $k=>$siswa){
				$hitung=0;
				for ($i = 1; $i <= $jumlah_kd; $i++) {
					if($_POST['kd_'.$i][$k]){
						$hitung += $_POST['kd_'.$i][$k];//
					} else {
						$hitung = 0;
					}
				}
				$hitung_nilai = $hitung / $jumlah_kd;
				$record['value'] 	= number_format($hitung_nilai,0);
				//=F6*(C4/(C4+G4+J4+M4))
				$record['rerata_text'] 	= '';
				$record['rerata_jadi'] 	= $hitung_nilai;
				$output['rerata'][] = $record;
			}
			$output['rumus'] = '';
		}*/
		echo json_encode($output);
	}
	public function simpan_nilai(){
		unset($_POST['query'], $_POST['query_2'], $_POST['kelas'], $_POST['rencana_id'], $_POST['import']);
		$ajaran_id = $_POST['ajaran_id'];
		$kompetensi_id = $_POST['kompetensi_id'];
		$rombel_id = $_POST['rombel_id'];
		$mapel_id = $_POST['id_mapel'];
		$bobot_kd = $_POST['bobot_kd'];
		$rencana_penilaian_id = $_POST['rencana_penilaian_id'];
		$all_bobot = $_POST['all_bobot'];
		$all_bobot = array_sum($all_bobot);
		$jumlah_kd = $_POST['jumlah_kd'];
		$siswa_id = $_POST['siswa_id'];
		$kds = $_POST['kd'];
		$redirect = '';
		$title = '';
		if($kompetensi_id == 1){
			$redirect = 'pengetahuan';
			$title = 'pengetahuan';
		} else {
			$redirect = 'keterampilan';
			$title = 'keterampilan';
		}
		//array_walk($kds, 'check_great_than_one_fn',$redirect);
		//array_walk($kds, 'check_numeric','asesmen/'.$redirect);
		foreach($siswa_id as $k=>$siswa){
			foreach($kds as $key=>$kd) {
				array_walk($kd, 'check_great_than_one_fn',$redirect);
				array_walk($kd, 'check_numeric','asesmen/'.$redirect);
				$get_kd_nilai_id = $this->kd_nilai->find("rencana_penilaian_id = '$rencana_penilaian_id' AND kd_id = $key");
				if($get_kd_nilai_id){
					$nilai = ($kd[$k]) ? $kd[$k] : 0;
					$get_nilai = $this->nilai->find("semester_id = $ajaran_id AND rencana_penilaian_id = '$rencana_penilaian_id' AND siswa_id = '$siswa' AND kompetensi_dasar_id = $key AND kd_nilai_id = '$get_kd_nilai_id->kd_nilai_id'");
					$insert_nilai = array(
						'semester_id' 			=> $ajaran_id, 
						'kompetensi_id' 		=> $kompetensi_id, 
						'rombongan_belajar_id'	=> $rombel_id, 
						'mata_pelajaran_id' 	=> $mapel_id, 
						'siswa_id' 				=> $siswa,
						'rencana_penilaian_id'	=> $rencana_penilaian_id,
						'kompetensi_dasar_id' 	=> $key,
						'kd_nilai_id'			=> $get_kd_nilai_id->kd_nilai_id,
						'nilai'					=> $nilai,
						'rerata'				=> $_POST['rerata'][$k],
						'rerata_jadi'			=> $_POST['rerata_jadi'][$k],
					);
					if($get_nilai){
						$this->nilai->update($get_nilai->nilai_id, $insert_nilai);
						$this->session->set_flashdata('success', 'Berhasil memperbaharui data nilai '.$title);
					//} elseif($nilai) { //off validasi agar semua nilai siswa tersimpan meskipun value nilai = 0
						//$insert_nilai['nilai_id'] = gen_uuid();
						//$this->nilai->insert($insert_nilai);
						//$this->session->set_flashdata('success', 'Berhasil menambah data nilai '.$title);
					} else {
						$insert_nilai['nilai_id'] = gen_uuid();
						$this->nilai->insert($insert_nilai);
						$this->session->set_flashdata('success', 'Berhasil menambah data nilai '.$title);
					}
				} else {
					$this->session->set_flashdata('error', 'Gagal menambah nilai '.$title.'. Periksa kembali isian Anda');
				}
			}
		}
		redirect('admin/asesmen/'.$redirect);
	}
	public function get_nilai(){
		$query = $_POST['query'];
		$data['ajaran_id'] = $_POST['ajaran_id'];
		$data['id_mapel'] = $_POST['id_mapel'];
		$data['rombel_id'] = $_POST['rombel_id'];
		$data['kompetensi_id'] = isset($_POST['kompetensi_id']) ? $_POST['kompetensi_id'] : '';
		if($query == 'nilai'){
			$query = 'portofolio';
		}
		$this->template->title('')
		->set_layout($this->blank_tpl)
		->set('page_title', '')
		->build($this->admin_folder.'/asesmen/form_'.$query,$data);
	}
	public function get_kd_penilaian(){
		$html = '';
		$settings 	= $this->settings->get(1);
		$ajaran_id = $_POST['ajaran_id'];
		$post	= $_POST['rencana_id'];
		$post = explode("#", $post);
		$rencana_penilaian_id = $post[0];
		$nama_penilaian = $post[1];
		$bobot_kd = $post[2];
		$bobot_kd = ($bobot_kd > 0) ? $bobot_kd : 1;
		$rombel_id	= $_POST['rombel_id'];
		kunci_nilai($rombel_id);
		$id_mapel = $_POST['id_mapel'];
		$kompetensi_id = $_POST['kompetensi_id'];
		$kkm = get_kkm($ajaran_id, $rombel_id, $id_mapel);
		$html .= '<input type="hidden" name="bobot_kd" value="'.$bobot_kd.'" />';
		$html .= '<div class="row">';
		$html .= '<div class="col-md-6">';
		//$html .= '<a class="btn btn-success btn-lg btn-block" href="'.site_url('admin/get_excel/nilai/'.$ajaran_id.'/'.$rencana_penilaian_id.'/'.$nama_penilaian.'/'.$rombel_id.'/'.$id_mapel.'/'.$kompetensi_id).'">Download Template Nilai</a>';
		$html .= '<a class="btn btn-success btn-lg btn-block" href="'.site_url('admin/get_excel/nilai/'.$rencana_penilaian_id).'">Download Template Nilai</a>';
		$html .= '</div>';
		$html .= '<div class="col-md-6">';
		$html .= '<p class="text-center"><span class="btn btn-danger btn-file btn-lg btn-block"> Import Template Nilai<input type="file" id="fileupload" name="import" /></span></p>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div style="clear:both; margin-bottom:10px;"></div>';
		$html .= '<input type="hidden" name="rencana_penilaian_id" value="'.$rencana_penilaian_id.'" />';
		$rencana_penilaian = $this->rencana_penilaian->with('mata_pelajaran')->get($rencana_penilaian_id);
		$all_kd_nilai = $this->kd_nilai->find_all_by_rencana_penilaian_id($rencana_penilaian_id);
		$data_siswa = filter_agama_siswa(get_nama_mapel($rencana_penilaian->mata_pelajaran_id), $rombel_id);
		$get_all_bobot = $this->rencana_penilaian->find_all("semester_id = $ajaran_id AND mata_pelajaran_id = $id_mapel AND rombongan_belajar_id = '$rombel_id' AND kompetensi_id = $kompetensi_id");
		foreach($get_all_bobot as $getbobot){
			$set_bobot = ($getbobot->bobot > 0) ? $getbobot->bobot : 1;
			$html .= '<input type="hidden" name="all_bobot[]" value="'.$set_bobot.'" />';
			$html .= '<input type="hidden" name="rencana_penilaian_id[]" value="'.$getbobot->rencana_penilaian_id.'" />';
		}
		if($all_kd_nilai){
			$jumlah_kd = count($all_kd_nilai);
			$html .= '<input type="hidden" name="jumlah_kd" id="jumlah_kd" value="'.$jumlah_kd.'" />';
			$html .= '<input type="hidden" name="kkm" id="kkm" value="'.$kkm.'" />';
			$html .= '<input type="hidden" name="rencana_penilaian_id" value="'.$rencana_penilaian_id.'" />';
			$html .= '<div class="table-responsive no-padding">';
			$html .= '<table class="table table-bordered table-hover">';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<th rowspan="2" style="vertical-align: middle;">Nama Peserta Didik</th>';
			$html .= '<th class="text-center" colspan="'.$jumlah_kd.'">Kompetensi Dasar</th>';
			if($rencana_penilaian->kompetensi_id == 1){
				$html .= '<th rowspan="2" style="vertical-align: middle;" class="text-center">Rerata Nilai</th>';
				//if($settings->rumus == 1){
					//$html .= '<th rowspan="2" style="vertical-align: middle;" class="text-center">Rumus</th>';
					//$html .= '<th rowspan="2" style="vertical-align: middle;" class="text-center">Nilai Akhir<br />Per Penilaian</th>';
				//}
			}
			$html .= '</tr>';
			$html .= '<tr>';
			foreach($all_kd_nilai as $kd_nilai){
				$kd = $this->kompetensi_dasar->get($kd_nilai->kd_id);
				//$html .= '<input type="text" name="bobot_kd" value="'.$allpengetahuan->bobot_penilaian.'" />';
				$id_kd = isset($kd->id_kompetensi) ? $kd->id_kompetensi : 0;
				$kompetensi_dasar = isset($kd->kompetensi_dasar) ? $kd->kompetensi_dasar : 0;
				$html .= '<th><a href="javacript:void(0)" class="tooltip-left" title="'.$kompetensi_dasar.'">'.$id_kd.'</a></th>';
			}
			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			$no=0;
			foreach($data_siswa['data'] as $siswa){
				$siswa_id = $siswa->siswa_id;
				$nama_siswa = strtoupper($siswa->nama);
				$html .= '<input class="set_nilai" type="hidden" name="siswa_id[]" value="'.$siswa_id.'" />';
				$html .= '<tr>';
				$html .= '<td>';
				$html .= strtoupper($nama_siswa);
				$html .= '</td>';
				$i=1;
				foreach($all_kd_nilai as $kd_nilai){
					$nilai = $this->nilai->find("siswa_id = '$siswa_id' and rencana_penilaian_id = '$rencana_penilaian->rencana_penilaian_id' AND kompetensi_dasar_id = $kd_nilai->kd_id");
					$nilai_value 	= ($nilai) ? $nilai->nilai : '';
					$rerata 		= ($nilai) ? $nilai->rerata : '';
					$rerata_jadi 	= ($nilai) ? $nilai->rerata_jadi : '';
					$html .= '<td><input type="text" name="kd['.$kd_nilai->kd_id.'][]" size="10" class="form-control" value="'.$nilai_value.'" autocomplete="off" maxlength="3" required /></td>';
					$i++;
				}
				if($rencana_penilaian->kompetensi_id == 1){
					$html .= '<td><input type="text" name="rerata[]" id="rerata_'.$no.'" size="10" class="form-control" value="'.$rerata.'" readonly /></td>';
					$html .= '<input type="hidden" name="rerata_jadi[]" id="rerata_jadi_'.$no.'" size="10" class="form-control" value="'.$rerata_jadi.'" />';
					//disini rumus rerata_jadi
					//if($settings->rumus == 1){
						//$html .= '<td class="text-center"><strong><span id="rerata_text_'.$no.'"></span></strong></td>';
						//$html .= '<td><input type="hidden" name="rerata_jadi[]" id="rerata_jadi_'.$no.'" size="10" class="form-control" value="'.$rerata_jadi.'" readonly /></td>';
					//}
				} else {
					$html .= '<input type="hidden" name="rerata[]" id="rerata_'.$no.'" size="10" class="form-control" value="'.$rerata.'" />';
					$html .= '<input type="hidden" name="rerata_jadi[]" id="rerata_jadi_'.$no.'" size="10" class="form-control" value="'.$rerata_jadi.'" />';
				}
				$html .= '</tr>';
				$no++;
			}
			$html .= '</tbody>';
			$html .= '</table>';
			$html .= '</div>';
		} else {
			$html .= '<h4>Tidak ada KD terpilih di Perencanaan Penilaian</h4>';
		}
		$html .= link_tag('assets/css/tooltip-viewport.css', 'stylesheet', 'text/css');
		$html .= '<script src="'.base_url().'assets/js/tooltip-viewport.js"></script>';
		$html .= '<script>';
		$html .= 'var SET_URL = "'.site_url('import/import_nilai').'";';
		$html .= '</script>';
		$html .= '<script src="'.base_url().'assets/js/file_upload.js"></script>';
		echo $html;
	}
	public function get_analisis_penilaian(){
		$data['ajaran_id'] = $_POST['ajaran_id'];
		$data['rombel_id'] = $_POST['rombel_id'];
		$data['mapel_id'] = $_POST['id_mapel'];
		$post	= $_POST['penilaian'];
		$post = explode("#", $post);
		$data['rencana_id'] = $post[0];
		if(!isset($post[1])){
			exit;
		}
		$data['nama_penilaian'] = $post[1];
		$data['kompetensi_id'] = $post[2];
		$this->template->title('Administrator Panel')
		->set_layout($this->blank_tpl)
		->set('page_title', 'Analisis Hasil Penilaian')
		->build($this->admin_folder.'/monitoring/analisis_penilaian',$data);
	}
	public function get_analisis_individu($id = NULL){
		$loggeduser = $this->ion_auth->user()->row();
		$html = '';
		if($id){
			$ajaran = get_ta();
			$siswa = $this->siswa->get($id);
			$mata_pelajaran_id = $_POST['id_mapel'];
			$anggota_rombel = $this->anggota_rombel->find("semester_id = $ajaran->id AND siswa_id = '$id'");
			//Anggotarombel::find_by_ajaran_id_and_datasiswa_id($ajaran->id, $id);
			//$mapel = Datamapel::find_by_id_mapel($_POST['id_mapel']);
			//$rombel_id = isset($anggota_rombel->rombel_id) ? $anggota_rombel->rombel_id : 0;
			//$data_rombel = Datarombel::find_by_id($rombel_id);
			$nilai_pengetahuan = $this->nilai->find_all("semester_id = $ajaran->id AND kompetensi_id = 1 AND rombongan_belajar_id = '$anggota_rombel->rombongan_belajar_id' AND mata_pelajaran_id = $mata_pelajaran_id AND siswa_id = '$id'");
			//Nilai::find_all_by_ajaran_id_and_kompetensi_id_and_rombel_id_and_mapel_id_and_data_siswa_id($ajaran->id, 1, $rombel_id, $mapel->id_mapel, $siswa->id);
			$nilai_keterampilan = $this->nilai->find_all("semester_id = $ajaran->id AND kompetensi_id = 2 AND rombongan_belajar_id = '$anggota_rombel->rombongan_belajar_id' AND mata_pelajaran_id = $mata_pelajaran_id AND siswa_id = '$id'");
			//Nilai::find_all_by_ajaran_id_and_kompetensi_id_and_rombel_id_and_mapel_id_and_data_siswa_id($ajaran->id, 2, $rombel_id, $mapel->id_mapel, $siswa->id);
			$html .= '<table class="table table-bordered table-hover">';
			$html .= '<tr>';
			$html .= '<th width="5%" class="text-center">ID KD</th>';
			$html .= '<th width="85%">Kompetensi Dasar Pengetahuan</th>';
			$html .= '<th width="10%" class="text-center">Rerata Nilai</th>';
			$html .= '</tr>';
			foreach($nilai_pengetahuan as $np){
				//$rencana_penilaian_pengetahuan = Rencanapenilaian::find_by_id($np->rencana_penilaian_id);
				$get_pengetahuan[$np->kompetensi_id][] = $np->nilai;
			}
			if(isset($get_pengetahuan)){
				ksort($get_pengetahuan);
				$rerata_akhir_p = 0;
				$jumlah_penilaian_p = 0;
				foreach($get_pengetahuan as $key=>$gp){
				//test($get_pengetahuan);
				//die();
					$get_kompetensi_p = $this->kompetensi_dasar->get($key);
					//Kd::find_by_id_kompetensi_and_id_mapel_and_kelas($key,$mapel->id_mapel,$data_rombel->tingkat);
					$jumlah_kd_p = array_sum($gp);
					$rerata_nilai_p = number_format(($jumlah_kd_p / count($gp)),2);
					$rerata_akhir_p += $jumlah_kd_p;
					$jumlah_penilaian_p += count($gp);
					$html .= '<tr>';
					$html .= '<td class="text-center">'.$key.'</td>';
					$html .= '<td>'.$get_kompetensi_p->kompetensi_dasar.'</td>';
					$html .= '<td class="text-center">'.number_format($rerata_nilai_p).'</td>';
					$html .= '</tr>';
				}
			}
			$html .= '<tr>';
			$html .= '<td colspan="2" class="text-right"><strong>Rerata Akhir = </strong></td>';
			if(isset($jumlah_penilaian_p)){
				$html .= '<td class="text-center">'.number_format($rerata_akhir_p / $jumlah_penilaian_p,0).'</td>';
			} else {
				$html .= '<td class="text-center"></td>';
			}
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '<table class="table table-bordered table-hover" style="margin-top:10px;">';
			$html .= '<tr>';
			$html .= '<th width="5%" class="text-center">ID KD</th>';
			$html .= '<th width="85%">Kompetensi Dasar Keterampilan</th>';
			$html .= '<th width="10%" class="text-center">Rerata Nilai</th>';
			$html .= '</tr>';
			foreach($nilai_keterampilan as $nk){
				$get_keterampilan[$nk->kompetensi_id][] = $np->nilai;
			}
			if(isset($get_keterampilan)){
				ksort($get_keterampilan);
				$rerata_akhir_k = 0;
				$jumlah_penilaian_k = 0;
				foreach($get_keterampilan as $key=>$gk){
					$get_kompetensi_k = $this->kompetensi_dasar->get($key);
					$jumlah_kd_k = array_sum($gk);
					$rerata_nilai_k = number_format(($jumlah_kd_k / count($gk)),2);
					$rerata_akhir_k += $jumlah_kd_k;
					$jumlah_penilaian_k += count($gk);
					$html .= '<tr>';
					$html .= '<td class="text-center">'.$key.'</td>';
					$html .= '<td>'.$get_kompetensi_k->kompetensi_dasar.'</td>';
					$html .= '<td class="text-center">'.number_format($rerata_nilai_k).'</td>';
					$html .= '</tr>';
				}
			}
			$html .= '<tr>';
			$html .= '<td colspan="2" class="text-right"><strong>Rerata Akhir = </strong></td>';
			if(isset($jumlah_penilaian_k)){
				$html .= '<td class="text-center">'.number_format($rerata_akhir_k / $jumlah_penilaian_k,0).'</td>';
			} else {
				$html .= '<td class="text-center"></td>';
			}
			$html .= '</tr>';
			$html .= '</table>';
			echo $html;
		} else {
			//exit;
			$ajaran_id = $_POST['ajaran_id'];
			$id_mapel = $_POST['id_mapel'];
			$rombel_id = $_POST['rombel_id'];
			$siswa_id = $_POST['siswa_id'];
			if($siswa_id){
				/*$siswa = $this->siswa->get($siswa_id);
				$mapel = $_POST['id_mapel'];
				$anggota_rombel = $this->anggota_rombel->find("semester_id = $ajaran_id AND siswa_id = '$siswa_id'");
				$rombongan_belajar_id = ($anggota_rombel) ? $anggota_rombel->rombongan_belajar_id : gen_uuid();
				$data_rombel = $this->rombongan_belajar->get($rombongan_belajar_id);
				$kelas = ($data_rombel) ? $data_rombel->tingkat : 0;
				$this->db->select('*');
				$this->db->from('ref_kompetensi_dasar as a');
				$this->db->join('kd_nilai as b', 'b.kd_id = a.id');
				$this->db->join('rencana_penilaian as c', 'c.rencana_penilaian_id = b.rencana_penilaian_id');
				$this->db->where('c.sekolah_id', $loggeduser->sekolah_id);
				$this->db->where('c.semester_id', $ajaran_id);
				$this->db->where('c.mata_pelajaran_id', $id_mapel);
				$this->db->where('c.rombongan_belajar_id', $rombel_id);
				$this->db->where('c.kompetensi_id', 1);
				$this->db->order_by('a.id', 'ASC');
				$query_pengetahuan = $this->db->get();
				$all_kd_pengetahuan = $query_pengetahuan->result();
				foreach($all_kd_pengetahuan as $kd_pengetahuan){
					$get_kd_pengetahuan[$kd_pengetahuan->id] = $kd_pengetahuan->id_kompetensi;
				}
				$this->db->select('*');
				$this->db->from('ref_kompetensi_dasar as a');
				$this->db->join('kd_nilai as b', 'b.kd_id = a.id');
				$this->db->join('rencana_penilaian as c', 'c.rencana_penilaian_id = b.rencana_penilaian_id');
				$this->db->where('c.sekolah_id', $loggeduser->sekolah_id);
				$this->db->where('c.semester_id', $ajaran_id);
				$this->db->where('c.mata_pelajaran_id', $id_mapel);
				$this->db->where('c.rombongan_belajar_id', $rombel_id);
				$this->db->where('c.kompetensi_id', 2);
				$this->db->order_by('a.id', 'ASC');
				$query_keterampilan = $this->db->get();
				$all_kd_keterampilan = $query_keterampilan->result();
				foreach($all_kd_keterampilan as $kd_keterampilan){
					$get_kd_keterampilan[$kd_keterampilan->id] = $kd_keterampilan->id_kompetensi;
				}
				//test($get_kd_pengetahuan);
				//test($get_kd_keterampilan);
				if(!isset($get_kd_pengetahuan)){
					echo '<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<h4><i class="icon fa fa-ban"></i> Error!</h4>
					Pilih Mata Pelajaran terlebih dahulu sebelum memilih nama peserta didik!
				  </div>';
					exit;
				}*/
				$rombel = $this->rombongan_belajar->get($rombel_id);
				$data['data']['ajaran_id'] = $ajaran_id;
				$data['data']['kelas'] = ($rombel) ? $rombel->tingkat : 0;
				$data['data']['rombel_id'] = $rombel_id;
				$data['data']['id_mapel'] = $id_mapel;
				$data['data']['siswa_id'] = $siswa_id;
				$this->template->title('')
				->set_layout($this->blank_tpl)
				->set('page_title', '')
				->build($this->admin_folder.'/monitoring/analisis_individu',$data);
			} else {
				//exit;
				$data_siswa = get_siswa_by_rombel($rombel_id);
				if($data_siswa){
			//sort($anggotarombel);
					foreach($data_siswa['data'] as $siswa){
						$record= array();
						$record['value'] 	= $siswa->siswa_id;
						$record['text'] 	= strtoupper($siswa->nama);
						$output['siswa'][] = $record;
					}
				} else {
					$record['value'] 	= '';
					$record['text'] 	= 'Tidak ditemukan siswa di rombel terpilih';
					$output['siswa'][] = $record;
				}
				echo json_encode($output);
			}
			//exit;
			/*$input_pengetahuan = preg_quote('KD-03', '~'); // don't forget to quote input string!
			$input_alt_pengetahuan = preg_quote('3.', '~'); // don't forget to quote input string!
			$input_keterampilan = preg_quote('KD-04', '~'); // don't forget to quote input string!
			$input_alt_keterampilan = preg_quote('4.', '~'); // don't forget to quote input string!
			$input_all = preg_quote('', '~'); // don't forget to quote input string!
			$result_pengetahuan = preg_grep('~' . $input_pengetahuan . '~', $get_kd);
			if(!$result_pengetahuan){
				$result_pengetahuan = preg_grep('~' . $input_alt_pengetahuan . '~', $get_kd_alternatif);
			}
			$rencana_pengetahuan = Rencana::find_all_by_ajaran_id_and_id_mapel_and_rombel_id_and_kompetensi_id($ajaran_id, $mapel->id_mapel, $rombel_id, 1);
			if($rencana_pengetahuan){
				foreach($rencana_pengetahuan as $rp){
					$get_kd_rencana = Rencanapenilaian::find_all_by_rencana_id($rp->id);
					$get_rencana_penilaian_pengetahuan = Rencanapenilaian::find('all', array('conditions' => "rencana_id = $rp->id",'group' => 'nama_penilaian','order'=>'id ASC'));
				}
				foreach($get_kd_rencana as $a=>$gkr){
					$get_kd_id[$gkr->id] = $gkr->kd_id;
				}
			}
			$html .= '<table class="table table-bordered table-hover">';
			$html .= '<tr>';
			$html .= '<th width="5%" class="text-center">ID KD</th>';
			$html .= '<th>Kompetensi Dasar</th>';
			$nilai_value = '';
			if(isset($get_rencana_penilaian_pengetahuan)){
				foreach($get_rencana_penilaian_pengetahuan as $grpp){
					$html .= '<th class="text-center">'.$grpp->nama_penilaian.'</th>';
				}
				foreach($result_pengetahuan as $key=>$rp){
					$get_kompetensi = Kd::find_by_id($key);
					$html .= '<tr>';
					$html .= '<td class="text-center">'.$rp.'</td>';
					$html .= '<td>'.$get_kompetensi->kompetensi_dasar.'</td>';
					if(in_array($key,$get_kd_id)){
						foreach($get_rencana_penilaian_pengetahuan as $grpp){
							$rencana_satuan = Rencanapenilaian::find_by_kd_id_and_nama_penilaian($get_kompetensi->id,$grpp->nama_penilaian);
							if($rencana_satuan){
							$nilai = Nilai::find_by_ajaran_id_and_kompetensi_id_and_rombel_id_and_mapel_id_and_data_siswa_id_and_rencana_penilaian_id( $ajaran_id, 1, $rombel_id, $id_mapel, $siswa_id, $rencana_satuan->id);
							if($nilai){
								$nilai_value = $nilai->nilai;
							}
							} else {
								$nilai_value = '';
							}
							$html .= '<td class="text-center">'.$nilai_value.'</td>';
						}
					} else {
						foreach($get_rencana_penilaian_pengetahuan as $grpp){
							$html .= '<td class="text-center"></td>';
						}
					}
					$html .= '</tr>';
				}
			} else {
				$html .= '<th>Belum ada penilaian</th>';
			}
			$html .= '</tr>';
			$html .= '</table>';

			$result_keterampilan = preg_grep('~' . $input_keterampilan . '~', $get_kd);
			if(!$result_keterampilan){
				$result_keterampilan = preg_grep('~' . $input_alt_keterampilan . '~', $get_kd_alternatif);
			}
			$rencana_keterampilan = Rencana::find_all_by_ajaran_id_and_id_mapel_and_rombel_id_and_kompetensi_id($ajaran_id, $mapel->id_mapel, $rombel_id, 2);
			if($rencana_keterampilan){
				foreach($rencana_keterampilan as $rp){
					$get_kd_rencana = Rencanapenilaian::find_all_by_rencana_id($rp->id);
					$get_rencana_penilaian_keterampilan = Rencanapenilaian::find('all', array('conditions' => "rencana_id = $rp->id",'group' => 'nama_penilaian','order'=>'id ASC'));
				}
				foreach($get_kd_rencana as $a=>$gkr){
					$get_kd_id[$gkr->id] = $gkr->kd_id;
				}
			}
			//foreach($get_rencana_penilaian_keterampilan as $grpp){
				//$cari_nama_penilaian[] = $grpp->nama_penilaian;
			//}
			$html .= '<table class="table table-bordered table-hover" style="margin-top:10px;">';
			$html .= '<tr>';
			$html .= '<th width="5%" class="text-center">ID KD</th>';
			$html .= '<th>Kompetensi Dasar</th>';
			if(isset($get_rencana_penilaian_keterampilan)){
				foreach($get_rencana_penilaian_keterampilan as $grpp){
					$html .= '<th class="text-center">'.$grpp->nama_penilaian.'</th>';
				}
				foreach($result_keterampilan as $key=>$rp){
					$get_kompetensi = Kd::find_by_id($key);
					$html .= '<tr>';
					$html .= '<td class="text-center">'.$rp.'</td>';
					$html .= '<td>'.$get_kompetensi->kompetensi_dasar.'</td>';
					if(in_array($key,$get_kd_id)){
						foreach($get_rencana_penilaian_keterampilan as $grpp){

							$rencana_satuan = Rencanapenilaian::find_by_kd_id_and_nama_penilaian($get_kompetensi->id,$grpp->nama_penilaian);
							if($rencana_satuan){
							$nilai = Nilai::find_by_ajaran_id_and_kompetensi_id_and_rombel_id_and_mapel_id_and_data_siswa_id_and_rencana_penilaian_id( $ajaran_id, 2, $rombel_id, $id_mapel, $siswa_id, $rencana_satuan->id);
							if($nilai){
								$nilai_value = $nilai->nilai;
							}
							} else {
								$nilai_value = '';
							}
							$html .= '<td class="text-center">'.$nilai_value.'</td>';
						}
					} else {
						foreach($get_rencana_penilaian_keterampilan as $grpp){
							$html .= '<td class="text-center"></td>';
						}
					}
					$html .= '</tr>';
				}
			} else {
				$html .= '<th>Belum ada penilaian</th>';
			}
			$html .= '</tr>';
			$html .= '</table>';*/		
		}
		//echo $html;
	}
	public function ekstrakurikuler(){
		$admin_group = array(1,2,3,5,6);
		$ajaran = get_ta();
		hak_akses($admin_group);
		$loggeduser = $this->ion_auth->user()->row();
		$nama_group = get_jabatan($loggeduser->id);
		$find_akses = get_akses($loggeduser->id);
		$guru_id = $find_akses['id'][0];
		if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
			$find_ekskul = $this->ekstrakurikuler->find("guru_id = '$guru_id' AND semester_id = $ajaran->id");
			if($find_ekskul){
				//$file = 'ekstrakurikuler';
				$file = 'form_ekstrakurikuler';
			} else {
				//$file = 'form_ekstrakurikuler';
				$file = 'list_ekstrakurikuler';
			}
		} else {
			$file = 'list_ekstrakurikuler';
		}
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('form_action', 'admin/laporan/simpan_ekstrakurikuler')
		->set('page_title', 'Ekstrakurikuler')
		->build($this->admin_folder.'/laporan/'.$file);
	}
	public function get_ekstrakurikuler(){
		$data['ajaran_id'] = $_POST['ajaran_id'];
		$data['ekskul_id'] = $_POST['ekskul_id'];
		$data['rombel_id'] = $_POST['rombel_id'];
		$this->template->title('Administrator Panel')
		->set_layout($this->blank_tpl)
		->set('page_title', 'Entry Nilai Ekstrakurikuler')
		->build($this->admin_folder.'/laporan/add_ekstrakurikuler',$data);
	}
	public function get_form_ekstrakurikuler(){
		$data['ajaran_id'] = $_POST['ajaran_id'];
		$data['ekskul_id'] = $_POST['ekskul_id'];
		$data['rombel_id'] = $_POST['rombel_id'];
		$this->template->title('Administrator Panel')
		->set_layout($this->blank_tpl)
		->set('page_title', 'Entry Nilai Ekstrakurikuler')
		->build($this->admin_folder.'/laporan/add_form_ekstrakurikuler',$data);
	}
	public function list_sikap($jurusan = NULL, $tingkat = NULL, $rombel = NULL){
		$loggeduser = $this->ion_auth->user()->row();
		$nama_group = get_jabatan($loggeduser->id);
		$find_akses = get_akses($loggeduser->id);
		$search = "";
		$start = 0;
		$rows = 10;
		$ajaran = get_ta();
		$check_2018 = check_2018();
		// get search value (if any)
		if (isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$search = $_GET['sSearch'];
		}

		// limit
		$start = get_start();
		$rows = get_rows();
		$join = joint($jurusan, $tingkat, $rombel);
		//test($find_akses);
		//$where = where($find_akses['name'], $nama_group, $loggeduser->guru_id);
		$where = '';
		$is_guru_sikap = is_guru_sikap($loggeduser->guru_id, $ajaran->id);
		if(in_array('guru',$find_akses['name']) && !in_array('waka',$nama_group)){
			$guru_id = $loggeduser->guru_id;
			//$where = "AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM pembelajaran WHERE guru_id = '$guru_id') OR semester_id =  $ajaran->id AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM pembelajaran WHERE guru_pengajar_id = '$guru_id')";
			if($is_guru_sikap){
			} else {
				if($check_2018){
					$where = "AND guru_id = '$guru_id'";
				} else {
					$where = "AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM pembelajaran WHERE guru_id = '$guru_id' AND semester_id = $ajaran->id AND deleted_at IS NULL OR guru_pengajar_id = '$guru_id' AND semester_id = $ajaran->id AND deleted_at IS NULL)";
				}
			$where .= " AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM pembelajaran WHERE guru_id = '$guru_id' AND semester_id = $ajaran->id  OR guru_pengajar_id = '$guru_id' AND semester_id = $ajaran->id)";
			}
		}
		$search_form = "siswa_id IN(SELECT siswa_id FROM ref_siswa WHERE nama LIKE '%$search%') OR siswa_id IN(SELECT siswa_id FROM ref_siswa WHERE nisn LIKE '%$search%')";
		$query = $this->nilai_sikap->with('siswa')->with('rombongan_belajar')->with('mata_pelajaran')->find_all("semester_id =  $ajaran->id $join $where AND ($search_form)", '*','nilai_sikap_id desc', $start, $rows);
		$filter = $this->nilai_sikap->with('siswa')->with('rombongan_belajar')->with('mata_pelajaran')->find_count("semester_id =  $ajaran->id $join $where AND ($search_form)", '*','nilai_sikap_id desc');
		$iFilteredTotal = count($query);
		
		$iTotal= $filter;
	    
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iTotal,
	        "aaData" => array()
	    );

	    // get result after running query and put it in array
		$i=$start;
	    foreach ($query as $temp) {
			$record = array();
            $record[] = $temp->siswa->nama;
			$record[] = $temp->rombongan_belajar->nama.'/'.$temp->rombongan_belajar->tingkat;
			//$record[] = $temp->mata_pelajaran->nama. ' ('.$temp->mata_pelajaran->mata_pelajaran_id.')';
			$record[] = butir_sikap($temp->butir_sikap);
			$record[] = '<div class="text-center">'.opsi_sikap($temp->opsi_sikap).'</div>';
			$record[] = $temp->uraian_sikap;
			$output['aaData'][] = $record;
		}
		$filter_table = filter_table($jurusan, $tingkat, $find_akses['name'], $nama_group);
		$output['rombel'] = $filter_table;
		// format it to JSON, this output will be displayed in datatable
		echo json_encode($output);
	}
/*-----------------------------------------------------------------------------------------------------------------------
	function to upload user photos
-------------------------------------------------------------------------------------------------------------------------*/
	public function upload_photo($fieldname) {
		//set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
		$config['upload_path'] = MEDIAFOLDER;
		// set the filter image types
		$config['allowed_types'] = 'png|gif|jpeg|jpg';
		//$config['max_width'] = '500'; 
		$this->load->helper('string');
		$config['file_name']	 = random_string('alnum', 32);
		//load the upload library
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
	
		//if not successful, set the error message
		if (!$this->upload->do_upload($fieldname)) {
			$data = array('success' => false, 'msg' => $this->upload->display_errors());
		} else { 
			$upload_details = $this->upload->data(); //uploading
			$data = array('success' => true, 'upload_data' => $upload_details, 'msg' => "Upload success!");
		}
		return $data;
	}
}