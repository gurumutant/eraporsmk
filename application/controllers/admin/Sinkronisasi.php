<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sinkronisasi extends Backend_Controller { 
	protected $activemenu = 'sinkronisasi';
	public function __construct() {
		parent::__construct();
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit', '-1'); 
		$this->load->helper(array('sync','file'));
		//$this->load->model('dapodik');
		$this->template->set('activemenu', $this->activemenu);
		$this->load->library('curl');
	}
	public function index(){
		if(file_exists('./assets/guru.json')){
			unlink('./assets/guru.json');
		}
		if(file_exists('./assets/rombongan_belajar.json')){
			unlink('./assets/rombongan_belajar.json');
		}
		if(file_exists('./assets/siswa_aktif.json')){
			unlink('./assets/siswa_aktif.json');
		}
		if(file_exists('./assets/siswa_keluar.json')){
			unlink('./assets/siswa_keluar.json');
		}
		if(file_exists('./assets/pembelajaran.json')){
			unlink('./assets/pembelajaran.json');
		}
		if(file_exists('./assets/ekskul.json')){
			unlink('./assets/ekskul.json');
		}
		if(file_exists('./assets/jurusan.json')){
			unlink('./assets/jurusan.json');
		}
		if(file_exists('./assets/kurikulum.json')){
			unlink('./assets/kurikulum.json');
		}
		if(file_exists('./assets/mata_pelajaran.json')){
			unlink('./assets/mata_pelajaran.json');
		}
		if(file_exists('./assets/mapel_kur.json')){
			unlink('./assets/mapel_kur.json');
		}
		$loggeduser = $this->ion_auth->user()->row();
		$password_dapo = ($loggeduser->password_dapo) ? 'yes' : 'no';
		$sekolah = $this->sekolah->get($loggeduser->sekolah_id);
		$ajaran = get_ta();
		$tahun = $ajaran->tahun;
		$smt = $ajaran->semester;
		$tahun = substr($tahun, 0,4); // returns "d"
		$semester_id = $tahun.$smt;
		$tahun_ajaran_id = substr($ajaran->tahun,0,4);
		$username_dapo = $loggeduser->email;
		$password_dapo = $loggeduser->password_dapo;
		$data_sync = array(
			'username_dapo'		=> $username_dapo,
			'password_dapo'		=> $password_dapo,
			'tahun_ajaran_id'	=> $tahun_ajaran_id,
			'semester_id'		=> $semester_id,
			'sekolah_id'		=> strtoupper($loggeduser->sekolah_id),
			'npsn'				=> $sekolah->npsn
		);
		$response = post_sync('status', $data_sync);
		//die();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Sinkronisasi Erapor dengan Dapodik (Online)')
		->set('response', $response)
		->set('password_dapo', $password_dapo)
		->build($this->sinkronisasi_folder.'/index');
	}
	public function sekolah(){
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah = $this->sekolah->get($loggeduser->sekolah_id);
		$sekolah_id = ($sekolah) ? $sekolah->sekolah_id : 0;
		$ajaran = get_ta();
		$tahun = $ajaran->tahun;
		$smt = $ajaran->semester;
		$tahun = substr($tahun, 0,4); // returns "d"
		$semester_id = $tahun.$smt;
		$tahun_ajaran_id = substr($ajaran->tahun,0,4);
		$data_sync = array(
			'username_dapo'		=> $loggeduser->email,
			'password_dapo'		=> $loggeduser->password_dapo,
			'tahun_ajaran_id'	=> $tahun_ajaran_id,
			'semester_id'		=> $semester_id,
			'sekolah_id'		=> strtoupper($loggeduser->sekolah_id),
			'npsn'				=> $sekolah->npsn
		);
		$response = post_sync('sekolah', $data_sync);
		if($response->post_login){
			$ptk = $response->kepsek;
			$data_guru = $this->guru->find_by_guru_id_dapodik($ptk->ptk_id);
			$ptk->email = ($ptk->email) ? $ptk->email : GenerateEmail().'@erapor-smk.net';
			$ptk->nuptk = ($ptk->nuptk) ? $ptk->nuptk : GenerateID();
			if($ptk->email == $loggeduser->email){
				$ptk->email = GenerateEmail().'@erapor-smk.net';
			}
			$query = $this->db->get_where('mst_wilayah', array('kode_wilayah' => $ptk->kode_wilayah));
			$kecamatan_kepsek = $query->row();
			$password = 12345678;
			$additional_data = array(
				"sekolah_id"	=> $loggeduser->sekolah_id,
				"nuptk"			=> $ptk->nuptk,
				'password_dapo'	=> md5($password),
				'created_at'	=> date('Y-m-d H:i:s'),
				'updated_at'	=> date('Y-m-d H:i:s'),
			);
			if($data_guru){
				$guru_id = $data_guru->guru_id;
				$this->guru->update($guru_id, array('guru_id_dapodik' => $ptk->ptk_id));
				$this->user->update($data_guru->user_id, array('guru_id' => $guru_id));
				$find_guru_aktif = $this->guru_terdaftar->find("guru_id = '$guru_id' and semester_id = $ajaran->id");
				if($find_guru_aktif){
					$update_guru_aktif = array(
						'status' => 1
					);
					$this->guru_terdaftar->update($find_guru_aktif->guru_terdaftar_id, $update_guru_aktif);
				} else {
					$attributes = array('guru_terdaftar_id' => gen_uuid(), 'semester_id' => $ajaran->id, 'guru_id' => $guru_id, 'status' => 1);
					$guru_aktif = $this->guru_terdaftar->insert($attributes);
				}
			} else {
				$guru_id = gen_uuid();
				$insert_guru = array(
					'guru_id'				=> $guru_id,
					'sekolah_id' 			=> $loggeduser->sekolah_id,
					'nama' 					=> $ptk->nama,
					'nuptk' 				=> $ptk->nuptk,
					'nip' 					=> $ptk->nip,
					'nik' 					=> $ptk->nik,
					'jenis_kelamin' 		=> $ptk->jenis_kelamin,
					'tempat_lahir' 			=> $ptk->tempat_lahir,
					'tanggal_lahir' 		=> $ptk->tanggal_lahir,
					'status_kepegawaian_id'	=> $ptk->status_kepegawaian_id,
					'jenis_ptk_id' 			=> $ptk->jenis_ptk_id,
					'agama_id' 				=> $ptk->agama_id,
					'alamat' 				=> $ptk->alamat_jalan,
					'rt' 					=> $ptk->rt,
					'rw' 					=> $ptk->rw,
					'desa_kelurahan' 		=> $ptk->desa_kelurahan,
					'kecamatan' 			=> $kecamatan_kepsek->nama,
					'kode_pos'				=> ($ptk->kode_pos) ? $ptk->kode_pos : 0,
					'no_hp'					=> ($ptk->no_hp) ? $ptk->no_hp : 0,
					'email' 				=> $ptk->email,
					'photo' 				=> '',
					//'active' 				=> 1,
					//'password' 				=> $password,
					//'petugas' 				=> $loggeduser->username,
					'guru_id_dapodik' 		=> $ptk->ptk_id,
				);
				$find_user = $this->user->find_by_username($ptk->nama);
				if(!$find_user){
					$group = array('3');
					$user_id = $this->ion_auth->register($ptk->nama, $password, $ptk->email, $additional_data, $group);
					if($user_id){
						$insert_guru['user_id'] = $user_id;
						$insert_guru_id = $this->guru->insert($insert_guru);
						$find_guru_aktif = $this->guru_terdaftar->find("guru_id = '$guru_id' and semester_id = $ajaran->id");
						if($find_guru_aktif){
							$update_guru_aktif = array(
								'status' => 1
							);
							$this->guru_terdaftar->update($find_guru_aktif->id, $update_guru_aktif);
						} else {
							$attributes = array('guru_terdaftar_id' => gen_uuid(), 'semester_id' => $ajaran->id, 'guru_id' => $guru_id, 'status' => 1);			
							$guru_aktif = $this->guru_terdaftar->insert($attributes);
						}
						$this->user->update($user_id, array('guru_id' => $guru_id));
					}
				} else {
					$data_guru = $this->guru->find_by_user_id($find_user->user_id);
					if(!$data_guru){
						$insert_guru['user_id'] = $find_user->user_id;
						$insert_guru_id = $this->guru->insert($insert_guru);
						$find_guru_aktif = $this->guru_terdaftar->find("guru_id = '$guru_id' and semester_id = $ajaran->id");
						if($find_guru_aktif){
							$update_guru_aktif = array(
								'status' => 1
							);
							$this->guru_terdaftar->update($find_guru_aktif->id, $update_guru_aktif);
						} else {
							$attributes = array('guru_terdaftar_id' => gen_uuid(), 'semester_id' => $ajaran->id, 'guru_id' => $guru_id, 'status' => 1);			
							$guru_aktif = $this->guru_terdaftar->insert($attributes);
						}
						$this->user->update($find_user->user_id, array('guru_id' => $guru_id));
					}
				}
			}
			$obj_guru_id = (object) array('guru_id' => $guru_id);
			$obj_merged = (object) array_merge((array) $response->data_sekolah, (array) $obj_guru_id);
			//test($response->jurusan_sp);
			if($this->sekolah->update($sekolah_id, $obj_merged)){
				foreach($response->jurusan_sp as $jurusan_sp){
					$insert_jur_sp = array(
						'jurusan_sp_id' => gen_uuid(),
						'sekolah_id'	=> $loggeduser->sekolah_id,
						'jurusan_id'	=> $jurusan_sp->jurusan_id,
						'nama_jurusan_sp'	=> $jurusan_sp->nama_jurusan_sp,
						'jurusan_sp_id_dapodik' => $jurusan_sp->jurusan_sp_id,
					);
					//$get_jurusan_sp = $this->jurusan_sp->find_by_jurusan_id($jurusan_sp->jurusan_id);
					$get_jurusan_sp = $this->jurusan_sp->find_by_jurusan_sp_id_dapodik($jurusan_sp->jurusan_sp_id);
					if($get_jurusan_sp){
						//echo 'update';
						$this->jurusan_sp->update($get_jurusan_sp->jurusan_sp_id, $insert_jur_sp);
					} else {
						//echo 'insert';
						$this->jurusan_sp->insert($insert_jur_sp);
					}
				}
				$this->session->set_flashdata('success', 'Data Sekolah berhasil di sinkronisasi');
			} else {
				$this->session->set_flashdata('error', 'Data Sekolah gagal di sinkronisasi');
			}
		}
		redirect('admin/sinkronisasi/');
	}
	public function guru(){
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah = $this->sekolah->get($loggeduser->sekolah_id);
		$ajaran = get_ta();
		$tahun = $ajaran->tahun;
		$smt = $ajaran->semester;
		$tahun = substr($tahun, 0,4); // returns "d"
		$semester_id = $tahun.$smt;
		$tahun_ajaran_id = substr($ajaran->tahun,0,4);
		$data_sync = array(
			'username_dapo'		=> $loggeduser->email,
			'password_dapo'		=> $loggeduser->password_dapo,
			'tahun_ajaran_id'	=> $tahun_ajaran_id,
			'semester_id'		=> $semester_id,
			'sekolah_id'		=> strtoupper($loggeduser->sekolah_id),
			'npsn'				=> $sekolah->npsn
		);
		$json_data = array();
		if(file_exists('./assets/guru.json')){
			$json_data = read_file('./assets/guru.json');
		} else {
			$response = post_sync('guru_sync', $data_sync);
			if (!write_file('./assets/guru.json', json_encode($response->data))){
				echo 'gagal';
				exit;
			} else {
				redirect('admin/sinkronisasi/guru');
			}
		}
		$data_dapodik = json_decode($json_data);
		$this->load->library('pagination');
		$config['base_url'] = site_url('admin/sinkronisasi/guru/');
		$config['total_rows'] = count($data_dapodik);
		$config['per_page'] = 10;
		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);
		$data['dapodik'] = array_slice($data_dapodik, $from, $config['per_page'], true);
		$data['inserted'] = $this->guru->find_count("guru_id IN (SELECT guru_id FROM guru_terdaftar WHERE sekolah_id = '$sekolah->sekolah_id' AND semester_id = $ajaran->id) AND guru_id_dapodik IS NOT NULL");
		$data['total_rows'] = count($data_dapodik);
		$data['pagination'] = $this->pagination->create_links();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Referensi PTK Dapodik')
		->set('loggeduser', $loggeduser)
		->set('ajaran', get_ta())
		->build($this->sinkronisasi_folder.'/guru', $data);
	}
	public function rombongan_belajar(){
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah = $this->sekolah->get($loggeduser->sekolah_id);
		$ajaran = get_ta();
		$tahun = $ajaran->tahun;
		$smt = $ajaran->semester;
		$tahun = substr($tahun, 0,4); // returns "d"
		$semester_id = $tahun.$smt;
		$tahun_ajaran_id = substr($ajaran->tahun,0,4);
		$data_sync = array(
			'username_dapo'		=> $loggeduser->email,
			'password_dapo'		=> $loggeduser->password_dapo,
			'tahun_ajaran_id'	=> $tahun_ajaran_id,
			'semester_id'		=> $semester_id,
			'sekolah_id'		=> strtoupper($loggeduser->sekolah_id),
			'npsn'				=> $sekolah->npsn
		);
		$json_data = array();
		if(file_exists('./assets/rombongan_belajar.json')){
			$json_data = read_file('./assets/rombongan_belajar.json');
		} else {
			$response = post_sync('rombongan_belajar_sync', $data_sync);
			if (!write_file('./assets/rombongan_belajar.json', json_encode($response->data))){
				echo 'gagal';
				exit;
			} else {
				redirect('admin/sinkronisasi/rombongan_belajar');
			}
		}
		$data_dapodik = json_decode($json_data);
		$this->load->library('pagination');
		$config['base_url'] = site_url('admin/sinkronisasi/rombongan_belajar/');
		$config['total_rows'] = count($data_dapodik);
		$config['per_page'] = 10;
		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);		
		$data['dapodik'] = array_slice($data_dapodik, $from, $config['per_page'], true);
		$data['inserted'] = $this->rombongan_belajar->find_count("sekolah_id = '$sekolah->sekolah_id' AND semester_id = $ajaran->id AND rombel_id_dapodik IS NOT NULL");
		$data['total_rows'] = count($data_dapodik);
		$data['pagination'] = $this->pagination->create_links();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Referensi Rombongan Belajar Dapodik')
		->set('loggeduser', $loggeduser)
		->set('ajaran', get_ta())
		->build($this->sinkronisasi_folder.'/rombongan_belajar', $data);
	}
	public function siswa_aktif(){
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah = $this->sekolah->get($loggeduser->sekolah_id);
		$ajaran = get_ta();
		$tahun = $ajaran->tahun;
		$smt = $ajaran->semester;
		$tahun = substr($tahun, 0,4); // returns "d"
		$semester_id = $tahun.$smt;
		$tahun_ajaran_id = substr($ajaran->tahun,0,4);
		$data_sync = array(
			'username_dapo'		=> $loggeduser->email,
			'password_dapo'		=> $loggeduser->password_dapo,
			'tahun_ajaran_id'	=> $tahun_ajaran_id,
			'semester_id'		=> $semester_id,
			'sekolah_id'		=> strtoupper($loggeduser->sekolah_id),
			'npsn'				=> $sekolah->npsn
		);
		$json_data = array();
		if(file_exists('./assets/siswa_aktif.json')){
			$json_data = read_file('./assets/siswa_aktif.json');
		} else {
			$response = post_sync('siswa_sync', $data_sync);
			//echo 'mana?';
			//test($response);
			//die();
			if (!write_file('./assets/siswa_aktif.json', json_encode($response->data))){
				echo 'gagal';
				exit;
			} else {
				redirect('admin/sinkronisasi/siswa_aktif');
			}
		}
		$data_dapodik = json_decode($json_data);
		$this->load->library('pagination');
		$config['base_url'] = site_url('admin/sinkronisasi/siswa_aktif/');
		//$config['total_rows'] = count($response->data);
		$config['total_rows'] = count($data_dapodik);
		$config['per_page'] = 10;
		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);		
		$data['dapodik'] = array_slice($data_dapodik, $from, $config['per_page'], true);
		//$data['dapodik'] = array_slice($response->data, $from, $config['per_page'], true);
		$data['inserted'] = $this->siswa->find_count("siswa_id IN (SELECT siswa_id FROM anggota_rombel WHERE sekolah_id = '$sekolah->sekolah_id' AND semester_id = $ajaran->id AND deleted_at IS NULL) AND siswa_id_dapodik IS NOT NULL");
		$data['total_rows'] = count($data_dapodik);
		//$data['total_rows'] = count($response->data);
		$data['pagination'] = $this->pagination->create_links();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Referensi Siswa Dapodik')
		->set('loggeduser', $loggeduser)
		->set('sekolah', $sekolah)
		->set('ajaran', get_ta())
		->build($this->sinkronisasi_folder.'/siswa_aktif', $data);
	}
	public function siswa_keluar(){
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah = $this->sekolah->get($loggeduser->sekolah_id);
		$ajaran = get_ta();
		$tahun = $ajaran->tahun;
		$smt = $ajaran->semester;
		$tahun = substr($tahun, 0,4); // returns "d"
		$semester_id = $tahun.$smt;
		$tahun_ajaran_id = substr($ajaran->tahun,0,4);
		$data_sync = array(
			'username_dapo'		=> $loggeduser->email,
			'password_dapo'		=> $loggeduser->password_dapo,
			'tahun_ajaran_id'	=> $tahun_ajaran_id,
			'semester_id'		=> $semester_id,
			'sekolah_id'		=> strtoupper($loggeduser->sekolah_id),
			'npsn'				=> $sekolah->npsn
		);
		$json_data = array();
		if(file_exists('./assets/siswa_keluar.json')){
			$json_data = read_file('./assets/siswa_keluar.json');
		} else {
			$response = post_sync('siswa_keluar_sync', $data_sync);
			if (!write_file('./assets/siswa_keluar.json', json_encode($response->data))){
				echo 'gagal';
				exit;
			} else {
				redirect('admin/sinkronisasi/siswa_keluar');
			}
		}
		$data_dapodik = json_decode($json_data);
		$this->load->library('pagination');
		$config['base_url'] = site_url('admin/sinkronisasi/siswa_keluar/');
		$config['total_rows'] = count($data_dapodik);
		$config['per_page'] = 10;
		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);		
		$data['dapodik'] = array_slice($data_dapodik, $from, $config['per_page'], true);
		$this->db->select('*');
		$this->db->from('ref_siswa');
		$this->db->join('anggota_rombel', 'anggota_rombel.siswa_id = ref_siswa.siswa_id');
		$this->db->join('users', 'users.user_id = ref_siswa.user_id');
		$this->db->where('ref_siswa.active', 0);
		$this->db->where("anggota_rombel.deleted_at IS NOT NULL");
		$this->db->where('users.active', 0);
		$this->db->where("ref_siswa.sekolah_id", $sekolah->sekolah_id);
		$data['inserted'] = $this->db->count_all_results();
		//$this->siswa->find_count("siswa_id IN (SELECT siswa_id FROM anggota_rombel WHERE sekolah_id = '$sekolah->sekolah_id' AND semester_id = $ajaran->id) AND siswa_id_dapodik IS NOT NULL");
		$data['total_rows'] = count($data_dapodik);
		$data['pagination'] = $this->pagination->create_links();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Referensi Siswa Keluar Dapodik')
		->set('loggeduser', $loggeduser)
		->set('sekolah', $sekolah)
		->set('ajaran', get_ta())
		->build($this->sinkronisasi_folder.'/siswa_keluar', $data);
	}
	public function pembelajaran(){
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah = $this->sekolah->get($loggeduser->sekolah_id);
		$ajaran = get_ta();
		$tahun = $ajaran->tahun;
		$smt = $ajaran->semester;
		$tahun = substr($tahun, 0,4); // returns "d"
		$semester_id = $tahun.$smt;
		$tahun_ajaran_id = substr($ajaran->tahun,0,4);
		$data_sync = array(
			'username_dapo'		=> $loggeduser->email,
			'password_dapo'		=> $loggeduser->password_dapo,
			'tahun_ajaran_id'	=> $tahun_ajaran_id,
			'semester_id'		=> $semester_id,
			'sekolah_id'		=> strtoupper($loggeduser->sekolah_id),
			'npsn'				=> $sekolah->npsn
		);
		$json_data = array();
		if(file_exists('./assets/pembelajaran.json')){
			$json_data = read_file('./assets/pembelajaran.json');
		} else {
			$response = post_sync('pembelajaran_sync', $data_sync);
			if (!write_file('./assets/pembelajaran.json', json_encode($response->data))){
				echo 'gagal';
				exit;
			} else {
				redirect('admin/sinkronisasi/pembelajaran');
			}
		}
		$data_dapodik = json_decode($json_data);
		$this->load->library('pagination');
		$config['base_url'] = site_url('admin/sinkronisasi/pembelajaran/');
		$config['total_rows'] = count($data_dapodik);
		$config['per_page'] = 10;
		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);		
		$data['dapodik'] = array_slice($data_dapodik, $from, $config['per_page'], true);
		$data['inserted'] = $this->pembelajaran->find_count("sekolah_id = '$sekolah->sekolah_id' AND semester_id = $ajaran->id AND is_dapodik = 1");
		$data['total_rows'] = count($data_dapodik);
		$data['pagination'] = $this->pagination->create_links();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Referensi Pembelajaran Dapodik')
		->set('loggeduser', $loggeduser)
		->set('ajaran', get_ta())
		->build($this->sinkronisasi_folder.'/pembelajaran', $data);
	}
	public function ekskul(){
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah = $this->sekolah->get($loggeduser->sekolah_id);
		$ajaran = get_ta();
		$tahun = $ajaran->tahun;
		$smt = $ajaran->semester;
		$tahun = substr($tahun, 0,4); // returns "d"
		$semester_id = $tahun.$smt;
		$tahun_ajaran_id = substr($ajaran->tahun,0,4);
		$username_dapo = $loggeduser->email;
		$password_dapo = $loggeduser->password_dapo;
		$data_sync = array(
			'username_dapo'		=> $username_dapo,
			'password_dapo'		=> $password_dapo,
			'tahun_ajaran_id'	=> $tahun_ajaran_id,
			'semester_id'		=> $semester_id,
			'sekolah_id'		=> strtoupper($loggeduser->sekolah_id),
			'npsn'				=> $sekolah->npsn
		);
		$json_data = array();
		if(file_exists('./assets/ekskul.json')){
			$json_data = read_file('./assets/ekskul.json');
		} else {
			$response = post_sync('ekskul_sync', $data_sync);
			if (!write_file('./assets/ekskul.json', json_encode($response->data))){
				echo 'gagal';
				exit;
			} else {
				redirect('admin/sinkronisasi/ekskul');
			}
		}
		$data_dapodik = json_decode($json_data);
		$this->load->library('pagination');
		$config['base_url'] = site_url('admin/sinkronisasi/ekskul/');
		$config['total_rows'] = count($data_dapodik);
		$config['per_page'] = 10;
		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);		
		$data['dapodik'] = array_slice($data_dapodik, $from, $config['per_page'], true);
		$data['inserted'] = $this->ekstrakurikuler->find_count("sekolah_id = '$sekolah->sekolah_id' AND semester_id = $ajaran->id AND is_dapodik = 1");
		$data['total_rows'] = count($data_dapodik);
		$data['pagination'] = $this->pagination->create_links();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Referensi Ekstrakurikuler Dapodik')
		->set('loggeduser', $loggeduser)
		->set('ajaran', get_ta())
		->build($this->sinkronisasi_folder.'/ekskul', $data);
	}
	public function jurusan(){
		$loggeduser = $this->ion_auth->user()->row();
		$this->db->select('*');
		$this->db->from('ref_jurusan');
		$this->db->order_by('updated_at', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->row();
		$updated_at = ($result) ? $result->updated_at : '2000-01-01 17:00:00';
		$updated_at = '2000-01-01 17:00:00';
		$data_sync = array(
			'updated_at'		=> $updated_at,
		);
		$json_data = array();
		if(file_exists('./assets/jurusan.json')){
			$json_data = read_file('./assets/jurusan.json');
		} else {
			$response = post_sync('jurusan', $data_sync);
			if (!write_file('./assets/jurusan.json', json_encode($response->data))){
				echo 'gagal';
				exit;
			} else {
				redirect('admin/sinkronisasi/jurusan');
			}
		}
		$data_dapodik = json_decode($json_data);
		$this->load->library('pagination');
		$config['base_url'] = site_url('admin/sinkronisasi/jurusan/');
		$config['total_rows'] = count($data_dapodik);
		$config['per_page'] = 50;
		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);		
		$data['dapodik'] = array_slice($data_dapodik, $from, $config['per_page'], true);
		$data['inserted'] = $this->jurusan->find_count("updated_at >= '$updated_at'");
		$data['total_rows'] = count($data_dapodik);
		$data['pagination'] = $this->pagination->create_links();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Referensi Jurusan Dapodik')
		->set('loggeduser', $loggeduser)
		->set('ajaran', get_ta())
		->set('data_sync', $data_sync)
		->build($this->sinkronisasi_folder.'/jurusan', $data);
	}
	public function kurikulum(){
		$loggeduser = $this->ion_auth->user()->row();
		$this->db->select('*');
		$this->db->from('ref_kurikulum');
		$this->db->order_by('updated_at', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->row();
		$updated_at = ($result) ? $result->updated_at : '2000-01-01 17:00:00';
		$updated_at = '2000-01-01 17:00:00';
		$data_sync = array(
			'updated_at'		=> $updated_at,
		);
		$json_data = array();
		if(file_exists('./assets/kurikulum.json')){
			$json_data = read_file('./assets/kurikulum.json');
		} else {
			$response = post_sync('kurikulum', $data_sync);
			//test($response);
			if (!write_file('./assets/kurikulum.json', json_encode($response->data))){
				echo 'gagal';
				exit;
			} else {
				redirect('admin/sinkronisasi/kurikulum');
			}
		}
		$data_dapodik = json_decode($json_data);
		$this->load->library('pagination');
		$config['base_url'] = site_url('admin/sinkronisasi/kurikulum/');
		$config['total_rows'] = count($data_dapodik);
		$config['per_page'] = 50;
		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);		
		$data['dapodik'] = array_slice($data_dapodik, $from, $config['per_page'], true);
		$data['inserted'] = $this->kurikulum->find_count("updated_at >= '$updated_at'");
		$data['total_rows'] = count($data_dapodik);
		$data['pagination'] = $this->pagination->create_links();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Referensi Kurikulum Dapodik')
		->set('loggeduser', $loggeduser)
		->set('ajaran', get_ta())
		->set('data_sync', $data_sync)
		->build($this->sinkronisasi_folder.'/kurikulum', $data);
	}
	public function mata_pelajaran(){
		$loggeduser = $this->ion_auth->user()->row();
		$this->db->select('*');
		$this->db->from('mata_pelajaran');
		$this->db->order_by('updated_at', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->row();
		$updated_at = ($result) ? $result->updated_at : '2000-01-01 17:00:00';
		$updated_at = '2000-01-01 17:00:00';
		$data_sync = array(
			'updated_at'		=> $updated_at,
		);
		$json_data = array();
		if(file_exists('./assets/mata_pelajaran.json')){
			$json_data = read_file('./assets/mata_pelajaran.json');
		} else {
			$response = post_sync('mata_pelajaran', $data_sync);
			//test($response);
			if (!write_file('./assets/mata_pelajaran.json', json_encode($response->data))){
				echo 'gagal';
				exit;
			} else {
				redirect('admin/sinkronisasi/mata_pelajaran');
			}
		}
		$data_dapodik = json_decode($json_data);
		$this->load->library('pagination');
		$config['base_url'] = site_url('admin/sinkronisasi/mata_pelajaran/');
		$config['total_rows'] = count($data_dapodik);
		$config['per_page'] = 50;
		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);		
		$data['dapodik'] = array_slice($data_dapodik, $from, $config['per_page'], true);
		$data['inserted'] = $this->mata_pelajaran->find_count("updated_at >= '$updated_at'");
		$data['total_rows'] = count($data_dapodik);
		$data['pagination'] = $this->pagination->create_links();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Referensi Mata Pelajaran Dapodik')
		->set('loggeduser', $loggeduser)
		->set('ajaran', get_ta())
		->set('data_sync', $data_sync)
		->build($this->sinkronisasi_folder.'/mata_pelajaran', $data);
	}
	public function mapel_kur(){
		$loggeduser = $this->ion_auth->user()->row();
		$this->db->select('*');
		$this->db->from('mata_pelajaran_kurikulum');
		$this->db->order_by('updated_at', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->row();
		$updated_at = ($result) ? $result->updated_at : '2000-01-01 17:00:00';
		$updated_at = '2017-05-08 17:00:00';
		$data_sync = array(
			'updated_at'		=> $updated_at,
		);
		$json_data = array();
		if(file_exists('./assets/mapel_kur.json')){
			$json_data = read_file('./assets/mapel_kur.json');
		} else {
			$response = post_sync('mapel_kur', $data_sync);
			//test($response);
			if (!write_file('./assets/mapel_kur.json', json_encode($response->data))){
				echo 'gagal';
				exit;
			} else {
				redirect('admin/sinkronisasi/mapel_kur');
			}
		}
		$data_dapodik = json_decode($json_data);
		$this->load->library('pagination');
		$config['base_url'] = site_url('admin/sinkronisasi/mapel_kur/');
		$config['total_rows'] = count($data_dapodik);
		$config['per_page'] = 50;
		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);
		if($data_dapodik){
			$array_slice = array_slice($data_dapodik, $from, $config['per_page'], true);
		} else {
			$array_slice = array();
		}		
		$data['dapodik'] = $array_slice;
		$data['inserted'] = $this->mata_pelajaran_kurikulum->find_count("updated_at >= '$updated_at'");
		$data['total_rows'] = count($data_dapodik);
		$data['pagination'] = $this->pagination->create_links();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Referensi Mata Pelajaran Kurikulum Dapodik')
		->set('loggeduser', $loggeduser)
		->set('ajaran', get_ta())
		->set('data_sync', $data_sync)
		->build($this->sinkronisasi_folder.'/mapel_kur', $data);
	}
	public function password_dapo(){
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah = $this->sekolah->get($loggeduser->sekolah_id);
		$sekolah_id = $loggeduser->sekolah_id;
		$password_dapo = $this->input->post('password_dapo');
		$password_dapo = md5($password_dapo);
		if($this->user->update($loggeduser->id, array('password_dapo' => $password_dapo, 'sekolah_id' => $sekolah_id))){
			echo 'berhasil';
		} else {
			echo 'gagal';
		}
	}
	public function sync(){
		$ajaran = get_ta();
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah = $this->sekolah->get($loggeduser->sekolah_id);
		$status_sync = status_sync();
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Kirim Data eRapor ke Server Direktorat')
		->set('sekolah', $sekolah)
		->set('status_sync', $status_sync)
		->set('semester_id', $ajaran->id)
		->build($this->sinkronisasi_folder.'/sync');
	}
	public function nilai($id = NULL){
		$this->load->config('db_dapodik');
		$this->dapodik = $this->config->item('dapodik');
		$this->_database = $this->load->database($this->config->item('dapodik'), TRUE);
		$semester = get_ta();
		$pembelajaran = '';
		$rombel_id_dapodik = '';
		$page_title = '';
		if($id){
			$pembelajaran = $this->pembelajaran->find_all("semester_id = $semester->id AND rombongan_belajar_id = '$id' AND kelompok_id IS NOT NULL");
			$rombel = $this->rombongan_belajar->get($id);
			$rombel_id_dapodik = ($rombel) ? $rombel->rombel_id_dapodik : 0;
			$page_title = ' Kelas '.get_nama_rombel($id);
		}
		$extension = 0;
		$connected = 0;
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Kirim Nilai Akhir ke Aplikasi Dapodik'.$page_title)
		->set('connected', $connected)
		->set('pembelajaran', $pembelajaran)
		->set('extension', $extension)
		->set('rombongan_belajar_id', $id)
		->set('ajaran_id', $semester->id)
		->set('rombel_id_dapodik', $rombel_id_dapodik)
		->set('updater_id', '')
		->build($this->sinkronisasi_folder.'/detil_nilai');
	}
	public function list_nilai($kompetensi = NULL, $tingkat = NULL){
		$semester = get_ta();
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah_id = $loggeduser->sekolah_id;
		$search = "";
		$start = 0;
		$rows = 25;
		$ajaran = get_ta();
		// get search value (if any)
		if (isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$search = $_GET['sSearch'];
		}

		// limit
		$start = get_start();
		$rows = get_rows();
		if($kompetensi && !$tingkat){
			$set_query = "AND jurusan_sp_id = $kompetensi AND";
		} elseif($kompetensi && $tingkat){
			$set_query = "AND tingkat = $tingkat AND jurusan_sp_id = $kompetensi AND";
		} else {
			$set_query = 'AND';
		}
		$query = $this->rombongan_belajar->with('pembelajaran')->find_all("sekolah_id = '$sekolah_id' AND semester_id = $semester->id $set_query (nama LIKE '%$search%')", '*','lower(nama) asc, tingkat asc', $start, $rows);
		$filter = $this->rombongan_belajar->find_all("sekolah_id = '$sekolah_id' AND semester_id = $semester->id $set_query (nama LIKE '%$search%')", '*','lower(nama) asc, tingkat asc');
		$iFilteredTotal = count($query);
		$iTotal=count($filter);
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iTotal,
	        "aaData" => array()
	    );

	    // get result after running query and put it in array
		$i=$start;
		//$barang = $query->result();
	    foreach ($query as $temp) {
			$get_semester = $this->semester->get($temp->semester_id);
			$class = 'btn-danger';
			$text = 'Salin Pembelajaran';
			$link = 'salin_pembelajaran';
			//$kurikulum = $this->pembelajaran->find("semester_id = $semester->id and rombel_id = $temp->id");
			//test($kurikulum);
			if($get_semester->semester == 1 || $temp->pembelajaran){
				$class = 'btn-success';
				$text = 'Pembelajaran';
				$link = 'pembelajaran';
			}
			$record = array();
			$record[] = $temp->nama;
			$record[] = get_wali_kelas($temp->rombongan_belajar_id);
			$record[] = '<div class="text-center">'.$temp->tingkat.'</div>';
			$record[] = get_jurusan($temp->jurusan_id);
			$record[] = get_kurikulum($temp->kurikulum_id);
			$record[] = '<div class="text-center"><a href="'.site_url('admin/sinkronisasi/nilai/'.$temp->rombongan_belajar_id).'" class="btn btn-success btn-sm"><i class="fa fa-arrow-circle-up"></i> Kirim Nilai</a></div>';
			$output['aaData'][] = $record;
		}
		// format it to JSON, this output will be displayed in datatable
		echo json_encode($output);
	}
}

