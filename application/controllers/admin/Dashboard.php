<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends Backend_Controller {
	protected $activemenu = 'dashboard';
	public function __construct() {
		parent::__construct(); 
		$this->template->set('activemenu', $this->activemenu);
	}
	public function index(){
		$loggeduser = $this->ion_auth->user()->row();
		if($this->ion_auth->in_group('admin')){
			cari_pembelajaran_id_kosong();
			generate_teknik_keterampilan($loggeduser->sekolah_id);
			generate_guru_id_nilai_sikap($loggeduser->sekolah_id);
		}
		$this->load->model('nilai_akhir_model','nilai_akhir');
		//update_jurusan_sp($loggeduser->sekolah_id);
		//$sekolah = $this->sekolah->find_by_sekolah_id_dapodik($loggeduser->sekolah_id);
		$sekolah_id = $loggeduser->sekolah_id;
		$semester = get_ta();
		$jurusan = $this->jurusan_sp->get_all();
		if(!$jurusan){
			redirect('admin/sinkronisasi');
		}
		$check_2018 = check_2018();
		$data['detil_user']			= $this->ion_auth->user()->row();
		$data['sekolah_id'] 		= $sekolah_id;
		$data['check_2018'] 		= $check_2018;
		$data['semester'] 			= get_ta();
		$data['siswa'] 				= $this->anggota_rombel->find_count("sekolah_id = '$sekolah_id' AND semester_id = $semester->id");
		$data['guru'] 				= $this->guru_terdaftar->find_count("sekolah_id = '$sekolah_id' AND semester_id = $semester->id");
		$data['rombongan_belajar']	= $this->rombongan_belajar->find_count("sekolah_id = '$sekolah_id' AND semester_id = $semester->id");
		$data['rencana_penilaian']	= $this->rencana_penilaian->find_count("semester_id = $semester->id AND rombongan_belajar_id IN(SELECT rombongan_belajar_id FROM rombongan_belajar WHERE sekolah_id = '$sekolah_id' AND semester_id = $semester->id)");
		//$data['nilai']				= $this->kd_nilai->find_count("rencana_penilaian_id IN(SELECT id FROM rencana_penilaian WHERE sekolah_id = $sekolah_id AND semester_id = $semester->id)");
		$data['nilai']				= $this->kd_nilai->find_count("sekolah_id = '$sekolah_id' AND rencana_penilaian_id IN(SELECT rencana_penilaian_id FROM rencana_penilaian WHERE semester_id = $semester->id AND rombongan_belajar_id IN(SELECT rombongan_belajar_id FROM rombongan_belajar WHERE sekolah_id = '$sekolah_id' AND semester_id = $semester->id))");
		//$this->nilai->find_count("semester_id = $semester->id");
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Beranda')
		->build($this->admin_folder.'/dashboard', $data);
	}
	public function skin($id){
		$id = str_replace('_','-',$id);
		$this->load->library('user_agent');
		$newdata = array(
			'template'  => $id
				);
		$this->session->set_userdata($newdata);
		redirect($this->agent->referrer());
	}
	public function kunci_nilai($rombongan_belajar_id, $status){
		$text = ($status) ? 'menonaktifkan' : 'mengaktifkan';
		$update_rombel = $this->rombongan_belajar->update($rombongan_belajar_id, array('kunci_nilai' => $status));
		if($update_rombel){
			$this->session->set_flashdata('success', "Berhasil $text penilaian");
		} else {
			$this->session->set_flashdata('error', "Gagal $teks penilaian");
		}
		redirect('admin/dashboard#pembelajaran');
	}
	public function list_dashboard($jurusan_id = NULL, $tingkat = NULL, $rombel = NULL){
		$loggeduser = $this->ion_auth->user()->row();
		$ajaran = get_ta();
		$search = "";
		$start = 0;
		$rows = 25;
		// get search value (if any)
		if (isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$search = $_GET['sSearch'];
		}

		// limit
		$start = get_start();
		$rows = get_rows();
		$filter_data = '';
		if($jurusan_id && $tingkat == NULL && $rombel == NULL){
			$filter_data = "AND rombongan_belajar_id IN(SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan_id)";
		}elseif($jurusan_id && $tingkat && $rombel == NULL){
			$filter_data = "AND rombongan_belajar_id IN(SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan_id AND tingkat = $tingkat)";
		} elseif($jurusan_id && $tingkat && $rombel){
			$filter_data = "AND rombongan_belajar_id = '$rombel'";
		}
		$search_form = "guru_id IN(SELECT guru_id FROM ref_guru WHERE nama LIKE '%$search%') OR guru_pengajar_id IN(SELECT guru_id FROM ref_guru WHERE nama LIKE '%$search%')";
		$query = $this->pembelajaran->find_all("sekolah_id = '$loggeduser->sekolah_id' AND semester_id = $ajaran->id $filter_data AND ($search_form)", '*','mata_pelajaran_id asc, rombongan_belajar_id desc', $start, $rows);
		//$this->catatan_ppk->with('siswa')->find_all("semester_id =  $ajaran->id AND siswa_id IN(SELECT siswa_id FROM anggota_rombel $where) AND ($search_form)", '*','catatan_ppk_id desc', $start, $rows);
		//$filter = $this->pembelajaran->find_count->find_all("sekolah_id = '$loggeduser->sekolah_id' AND semester_id = $ajaran->id AND ($search_form)");
		$filter = $this->pembelajaran->find_count("sekolah_id = '$loggeduser->sekolah_id' AND semester_id = $ajaran->id $filter_data AND ($search_form)");
		//$this->catatan_ppk->with('siswa')->find_count("semester_id =  $ajaran->id AND siswa_id IN(SELECT siswa_id FROM anggota_rombel $where) AND ($search_form)");
		$iFilteredTotal = count($query);
		$iTotal= $filter;
	    
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iTotal,
	        "aaData" => array()
	    );

	    // get result after running query and put it in array
		$i=$start;
		//$barang = $query->result();
		$check_2018 = check_2018();
	    foreach ($query as $temp) {
			$nama_rombel = get_nama_rombel($temp->rombongan_belajar_id);
			if($nama_rombel == '-'){
				$this->pembelajaran->delete($temp->pembelajaran_id);
			}
			$record = array();
			$record[] = $nama_rombel;
			$record[] = get_nama_mapel($temp->mata_pelajaran_id);
			$record[] = ($temp->guru_id) ? get_nama_guru($temp->guru_id) : get_nama_guru($temp->guru_pengajar_id);
			$record[] = '<div class="text-center">'.get_kkm($temp->semester_id, $temp->rombongan_belajar_id, $temp->mata_pelajaran_id).'</div>';
			$record[] = '<div class="text-center">'.get_jumlah_penilaian($temp->semester_id,$temp->rombongan_belajar_id,$temp->mata_pelajaran_id,1).'</div>';
			$record[] = '<div class="text-center">'.get_jumlah_penilaian($temp->semester_id,$temp->rombongan_belajar_id,$temp->mata_pelajaran_id,2).'</div>';
			$record[] = '<div class="text-center">'.get_jumlah_penilaian_telah_dinilai($temp->semester_id,$temp->rombongan_belajar_id,$temp->mata_pelajaran_id,1).'</div>';
			$record[] = '<div class="text-center">'.get_jumlah_penilaian_telah_dinilai($temp->semester_id,$temp->rombongan_belajar_id,$temp->mata_pelajaran_id,2).'</div>';
			$output['aaData'][] = $record;
		}
		if($jurusan_id && $tingkat){
			//echo $jurusan;
			$get_all_rombel = $this->rombongan_belajar->find_all("sekolah_id = '$loggeduser->sekolah_id' AND jurusan_id = $jurusan_id AND tingkat = $tingkat AND semester_id = $ajaran->id");
			if($get_all_rombel){
				foreach($get_all_rombel as $rombel){
					$all_rombel= array();
					$all_rombel['value'] = $rombel->rombongan_belajar_id;
					$all_rombel['text'] = $rombel->nama;
					$output['rombel'][] = $all_rombel;
				}
			} else {
				$record['value'] 	= '0';
				$record['text'] 	= 'Data rombongan belajar tidak ditemukan';
				$output['rombel'][] = $record;
			}
		}
		//test($output);
		// format it to JSON, this output will be displayed in datatable
		echo json_encode($output);
	}
}
