<?php
class CURLFile
{
    /**
     * Create CurlFile object
     * @param string $name File name
     * @param string $mimetype Mime type, optional
     * @param string $postfilename Post filename, defaults to actual filename
     */
    public function __construct($name, $mimetype = '', $postfilename = '')
    {}
 
    /**
     * Set mime type
     * @param string $mimetype
     * @return CurlFile
     */
    public function setMimeType($mimetype)
    {}
 
    /**
     * Set mime type
     * @param string $mimetype
     * @return string
     */
    public function getMimeType($mimetype)
    {}
 
    /**
     * Get file name from which the data will be read
     * @return string
     */
    public function getFilename()
    {}
 
    /**
     * Get file name which will be sent in the post
     * @param string $name File name
     * @return string
     */
    public function setPostFilename($name)
    {}
 
    /**
     * Set file name which will be sent in the post
     * @return string
     * @return CurlFile
     */
    public function getPostFilename()
    {}
	/**
     * Create CURLFile object
     * @param string $name File name
     * @param string $mimetype Mime type, optional
     * @param string $postfilename Post filename, defaults to actual filename
     */
    function curl_file_create($name, $mimetype = '', $postfilename = '')
    {}
}