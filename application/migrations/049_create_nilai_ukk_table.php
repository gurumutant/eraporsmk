<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_nilai_ukk_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'nilai_ukk_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rombongan_belajar_id' => array(
				'type' => 'uuid',
			),
			'siswa_id'	=> array(
				'type' => 'uuid',
			),
			'nilai_teori' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null'	=> true
			),
			'nilai_praktik' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('nilai_ukk_id', TRUE);
		$this->dbforge->create_table('nilai_ukk',TRUE); 
		
	}
	public function down(){
		$this->dbforge->drop_table('nilai_ukk', TRUE);
	}
}