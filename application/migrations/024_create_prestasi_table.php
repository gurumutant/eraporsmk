<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_prestasi_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'prestasi_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id'	=> array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rombongan_belajar_id'	=> array(
				'type' => 'uuid',
			),
			'siswa_id'	=> array(
				'type' => 'uuid',
			),
			'jenis_prestasi' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'keterangan_prestasi' => array(
				'type' => 'TEXT',
				'null'	=> true
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'last_sync' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('prestasi_id', TRUE);
		$this->dbforge->create_table('prestasi',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('prestasi', TRUE);
	}
}