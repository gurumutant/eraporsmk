<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `erapor_31`
//

// `erapor_31`.`ref_jenis_ptk`
$ref_jenis_ptk = array(
  array('id' => '3','nama' => 'Guru Kelas','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '4','nama' => 'Guru Mapel','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '5','nama' => 'Guru BK','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '6','nama' => 'Guru Inklusi','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '7','nama' => 'Pengawas Satuan Pendidikan','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '8','nama' => 'Pengawas PLB','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '9','nama' => 'Pengawas Metpel/Rumpun','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '10','nama' => 'Pengawas Bidang','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '11','nama' => 'Tenaga Administrasi Sekolah','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '12','nama' => 'Guru Pendamping','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '13','nama' => 'Guru Magang','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '14','nama' => 'Guru TIK','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '20','nama' => 'Kepala Sekolah','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '25','nama' => 'Pengawas BK','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '26','nama' => 'Pengawas SD','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '30','nama' => 'Laboran','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '40','nama' => 'Tenaga Perpustakaan','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '41','nama' => 'Tukang Kebun','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '42','nama' => 'Penjaga Sekolah','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '43','nama' => 'Petugas Keamanan','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '44','nama' => 'Pesuruh/Office Boy','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '51','nama' => 'Academic Advisor','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '52','nama' => 'Academic Specialist','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '53','nama' => 'Curriculum Development Advisor','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '54','nama' => 'Kindergarten Teacher','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '55','nama' => 'Management Advisor','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '56','nama' => 'Play Group Teacher','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '57','nama' => 'Principal','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '58','nama' => 'Teaching Assistant','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '59','nama' => 'Vice Principal','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL),
  array('id' => '99','nama' => 'Lainnya','created_at' => '2017-09-19 01:08:04','updated_at' => '2017-09-19 01:08:04','deleted_at' => NULL)
);
