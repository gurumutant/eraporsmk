<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `erapor_32`
//

// `erapor_32`.`ref_tingkat_pendidikan`
$ref_tingkat_pendidikan = array(
  array('tingkat_pendidikan_id' => '0','kode' => '0','nama' => '-','jenjang_pendidikan_id' => '99','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => '2013-09-08 00:00:00','last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '1','kode' => '1','nama' => 'Kelas 1','jenjang_pendidikan_id' => '4','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '2','kode' => '2','nama' => 'Kelas 2','jenjang_pendidikan_id' => '4','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '3','kode' => '3','nama' => 'Kelas 3','jenjang_pendidikan_id' => '4','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '4','kode' => '4','nama' => 'Kelas 4','jenjang_pendidikan_id' => '4','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '5','kode' => '5','nama' => 'Kelas 5','jenjang_pendidikan_id' => '4','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '6','kode' => '6','nama' => 'Kelas 6','jenjang_pendidikan_id' => '4','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '7','kode' => '7','nama' => 'Kelas 7','jenjang_pendidikan_id' => '5','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '8','kode' => '8','nama' => 'Kelas 8','jenjang_pendidikan_id' => '5','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '9','kode' => '9','nama' => 'Kelas 9','jenjang_pendidikan_id' => '5','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '10','kode' => '10','nama' => 'Kelas 10','jenjang_pendidikan_id' => '6','created_at' => '2013-10-25 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '11','kode' => '11','nama' => 'Kelas 11','jenjang_pendidikan_id' => '6','created_at' => '2013-10-25 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '12','kode' => '12','nama' => 'Kelas 12','jenjang_pendidikan_id' => '6','created_at' => '2013-10-25 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '13','kode' => '13','nama' => 'Kelas 13','jenjang_pendidikan_id' => '6','created_at' => '2013-10-25 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '71','kode' => 'KelA','nama' => 'Kelompok A','jenjang_pendidikan_id' => '1','created_at' => '2015-04-29 00:00:00','updated_at' => '2017-05-02 10:19:36','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('tingkat_pendidikan_id' => '72','kode' => 'KelB','nama' => 'Kelompok B','jenjang_pendidikan_id' => '1','created_at' => '2015-04-29 00:00:00','updated_at' => '2017-05-02 10:19:36','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00')
);
