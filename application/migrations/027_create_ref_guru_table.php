<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_guru_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'guru_id' => array(
				'type' => 'uuid',
			),
			'guru_id_dapodik' => array(
				'type' => 'uuid',
				'null' => true
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			//'sekolah_id_dapodik' => array(
				//'type' => 'uuid',
			//),
			'user_id' => array(
				'type' => 'uuid',
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'nuptk' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'nip' => array(
				'type' => 'VARCHAR',
				'constraint' => 18,
				'null' => true
			),
			'jenis_kelamin' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'tempat_lahir' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'tanggal_lahir' => array(
				'type' => 'DATE',
			),
			'nik' => array(
				'type' => 'VARCHAR',
				'constraint' => 16,
				'null' => true
			),
			'status_kepegawaian_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'jenis_ptk_id' => array(
				'type' => 'numeric(2,0)',
			),
			'agama_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'alamat' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'rt' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'rw' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'desa_kelurahan' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'kecamatan' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'kode_pos' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'no_hp' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'photo' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'last_sync' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('guru_id', TRUE);
		$this->dbforge->create_table('ref_guru',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('ref_guru', TRUE);
	}
}