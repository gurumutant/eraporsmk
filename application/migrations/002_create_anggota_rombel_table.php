<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_anggota_rombel_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'anggota_rombel_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rombongan_belajar_id'	=> array(
				'type' => 'uuid',
			),
			'siswa_id'	=> array(
				'type' => 'uuid',
			),
			'anggota_rombel_id_dapodik'	=> array(
				'type' => 'uuid',
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('anggota_rombel_id', TRUE);
		$this->dbforge->create_table('anggota_rombel',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('anggota_rombel', TRUE);
	}
}