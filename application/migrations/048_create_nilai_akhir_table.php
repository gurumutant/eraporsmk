<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_nilai_akhir_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'nilai_akhir_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'kompetensi_id'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rombongan_belajar_id' => array(
				'type' => 'uuid',
			),
			'mata_pelajaran_id'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'siswa_id'	=> array(
				'type' => 'uuid',
			),
			'nilai' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null'	=> true
			),
			'pembelajaran_id' => array(
				'type' => 'uuid',
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('nilai_akhir_id', TRUE);
		$this->dbforge->create_table('nilai_akhir',TRUE); 
		
	}
	public function down(){
		$this->dbforge->drop_table('nilai_akhir', TRUE);
	}
}