<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_jenis_ptk_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('ref_jenis_ptk',TRUE);
		$this->db->select('*');
		$this->db->from('ref_jenis_ptk');
		$this->db->where('id',3);
		$query = $this->db->get();
		$result = $query->row();
		if(!$result){
			include_once APPPATH."/migrations/referensi/ref_jenis_ptk.php";
			$this->db->insert_batch('ref_jenis_ptk', $ref_jenis_ptk);
		}
	}
	public function down(){
		$this->dbforge->drop_table('ref_jenis_ptk', TRUE);
	}
}