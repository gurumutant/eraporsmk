<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_groups_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 25
			),
			'description'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 100
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('groups',TRUE);
		$groups = array(
			array('id' => '1','name' => 'admin','description' => 'Administrator'),
			array('id' => '2','name' => 'tu','description' => 'Tata Usaha'),
			array('id' => '3','name' => 'guru','description' => 'Guru'),
			array('id' => '4','name' => 'siswa','description' => 'Peserta Didik'),
			array('id' => '5','name' => 'user','description' => 'General User'),
			array('id' => '6','name' => 'waka','description' => 'Waka Kurikulum'),
			array('id' => '7','name' => 'super','description' => 'Super Admin'),
			//array('id' => '7','name' => 'superadmin','description' => 'Super Admin'),
			//array('id' => '8','name' => 'pimpinan_satu','description' => 'Pimpinan 1'),
			//array('id' => '9','name' => 'pimpinan_dua','description' => 'Pimpinan 2'),
		);
		$this->db->select('*');
		$this->db->from('groups');
		$this->db->where('id',1);
		$query = $this->db->get();
		$result = $query->row();
		if(!$result){
			$this->db->insert_batch('groups', $groups);
		}
	}
	public function down(){
		$this->dbforge->drop_table('groups', TRUE);
	}
}