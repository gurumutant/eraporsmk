<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_pembelajaran_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'pembelajaran_id'	=> array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'pembelajaran_id_dapodik' => array(
				'type' => 'uuid',
				'null' => true,
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rombongan_belajar_id'	=> array(
				'type' => 'uuid',
			),
			'guru_id'	=> array(
				'type' => 'uuid',
				'null' => true
			),
			'guru_pengajar_id'	=> array(
				'type' => 'uuid',
				'null' => true
			),
			'mata_pelajaran_id'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'nama_mata_pelajaran'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'kelompok_id'	=> array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => true
			),
			'no_urut'	=> array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => true
			),
			
			'kkm'	=> array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => true
			),
			'is_dapodik'	=> array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => true
			),
			'rasio_p'	=> array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => true
			),
			'rasio_k'	=> array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => true
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'last_sync' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('pembelajaran_id', TRUE);
		$this->dbforge->create_table('pembelajaran',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('pembelajaran', TRUE);
	}
}