<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_gelar_ptk_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'gelar_ptk_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'gelar_akademik_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'guru_id'	=> array(
				'type' => 'uuid',
			),
			'ptk_id'	=> array(
				'type' => 'UUID',
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('gelar_ptk_id', TRUE);
		$this->dbforge->create_table('gelar_ptk',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('gelar_ptk', TRUE);
	}
}