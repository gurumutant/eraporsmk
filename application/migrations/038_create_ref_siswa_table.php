<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_siswa_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'siswa_id' => array(
				'type' => 'uuid',
			),
			'siswa_id_dapodik' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			//'sekolah_id_dapodik' => array(
				//'type' => 'uuid',
			//),
			'user_id' => array(
				'type' => 'uuid',
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'no_induk' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'nisn' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'jenis_kelamin' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'tempat_lahir' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'tanggal_lahir' => array(
				'type' => 'DATE',
			),
			'agama_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'anak_ke' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'alamat' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'rt' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'rw' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'desa_kelurahan' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'kecamatan' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'kode_pos' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'no_telp' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'sekolah_asal' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'diterima_kelas' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'diterima' => array(
				'type' => 'DATE',
			),
			'no_telp' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'nama_ayah' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'nama_ibu' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'kerja_ayah' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null'	=> true
			),
			'kerja_ibu' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null'	=> true
			),
			'nama_wali' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'alamat_wali' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'telp_wali' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'kerja_wali' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null'	=> true
			),
			'photo' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'active' => array(
				'type' => 'numeric(1,0) NOT NULL',
				'default' => 1
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('siswa_id', TRUE);
		$this->dbforge->create_table('ref_siswa',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('ref_siswa', TRUE);
	}
}