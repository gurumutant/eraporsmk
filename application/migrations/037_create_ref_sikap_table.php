<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_sikap_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'sikap_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'butir_sikap'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('sikap_id', TRUE);
		$this->dbforge->create_table('ref_sikap',TRUE);
		$ref_sikap = array(
			array('sikap_id' => '1','butir_sikap' => 'Religius','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '2','butir_sikap' => 'Jujur','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '3','butir_sikap' => 'Toleren','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '4','butir_sikap' => 'Disiplin','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '5','butir_sikap' => 'Bekerja Keras','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '6','butir_sikap' => 'Kreatif','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '7','butir_sikap' => 'Mandiri','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '8','butir_sikap' => 'Demokratis','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '9','butir_sikap' => 'Rasa Ingin Tahu','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '10','butir_sikap' => 'Semangat Kebangsaan','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '11','butir_sikap' => 'Cinta Tanah Air','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '12','butir_sikap' => 'Menghargai Prestasi','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '13','butir_sikap' => 'Komunikatif','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '14','butir_sikap' => 'Cinta Damai','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '15','butir_sikap' => 'Gemar Membaca','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '16','butir_sikap' => 'Peduli Lingkungan','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '17','butir_sikap' => 'Peduli Sosial','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00'),
			array('sikap_id' => '18','butir_sikap' => 'Bertanggung Jawab','created_at' => '2017-09-27 00:01:43','updated_at' => '2017-09-27 00:01:43','deleted_at' => NULL,'last_sync' => '2018-01-01 00:00:00')
		);
		$this->db->select('*');
		$this->db->from('ref_sikap');
		$this->db->where('sikap_id',1);
		$query = $this->db->get();
		$result = $query->row();
		if(!$result){
			$this->db->insert_batch('ref_sikap', $ref_sikap);
		}
	}
	public function down(){
		$this->dbforge->drop_table('ref_sikap', TRUE);
	}
}