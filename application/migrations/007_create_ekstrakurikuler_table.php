<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ekstrakurikuler_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'ekstrakurikuler_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'guru_id'	=> array(
				'type' => 'uuid',
			),
			'nama_ekskul'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'nama_ketua'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'nomor_kontak'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'alamat_ekskul'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'is_dapodik' => array(
				'type' 			=> 'INT',
				'constraint' 	=> 1,
				'default'		=> 0,
			),
			'id_kelas_ekskul' => array(
				'type' 	=> 'uuid',
				'null'	=> true
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('ekstrakurikuler_id', TRUE);
		$this->dbforge->create_table('ekstrakurikuler',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('ekstrakurikuler', TRUE);
	}
}