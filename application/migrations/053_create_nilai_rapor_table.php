<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_nilai_rapor_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'nilai_rapor_id' => array(
			'type' => 'uuid',
		),
			'catatan_ppk_id'	=> array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'sikap_id'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'deskripsi'	=> array(
				'type' => 'TEXT',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('nilai_rapor_id', TRUE);
		$this->dbforge->create_table('nilai_rapor',TRUE);  
	}
	public function down(){
		$this->dbforge->drop_table('nilai_rapor', TRUE);
	}
}