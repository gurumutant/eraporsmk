<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_sekolah_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			//'sekolah_id_dapodik' => array(
				//'type' => 'uuid',
			//),
			'npsn' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'nss' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'alamat' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'desa_kelurahan' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'kecamatan' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'kode_wilayah' => array(
				'type' => 'VARCHAR',
				'constraint' => 8,
				'null'	=> true
			),
			'kabupaten' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'provinsi' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'kode_pos' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'lintang' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'bujur' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'no_telp' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'no_fax' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'website' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'logo_sekolah' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'guru_id' => array(
				'type' => 'uuid',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('sekolah_id', TRUE);
		$this->dbforge->create_table('ref_sekolah',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('ref_sekolah', TRUE);
	}
}