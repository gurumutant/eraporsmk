<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_kenaikan_kelas_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'kenaikan_kelas_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'anggota_rombel_id'	=> array(
				'type' => 'uuid',
			),
			'status'	=> array(
				'type' => 'INT',
				'constraint' => 1,
				'default'	=> 2
			),
			'ke_kelas'	=> array(
				'type' => 'uuid',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('kenaikan_kelas_id', TRUE);
		$this->dbforge->create_table('kenaikan_kelas',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('kenaikan_kelas', TRUE);
	}
}