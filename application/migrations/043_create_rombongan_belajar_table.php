<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_rombongan_belajar_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'rombongan_belajar_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			//'sekolah_id_dapodik' => array(
				//'type' => 'uuid',
			//),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'jurusan_id'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'jurusan_sp_id'	=> array(
				'type' => 'uuid',
				'null'	=> true
			),
			'kurikulum_id'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'nama'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'guru_id'	=> array(
				'type' => 'uuid',
			),
			'tingkat'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'guru_id_dapodik' => array(
				'type' => 'uuid',
			),
			'rombel_id_dapodik' => array(
				'type' => 'uuid',
			),
			'kunci_nilai' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default'	=> 0,
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('rombongan_belajar_id', TRUE);
		$this->dbforge->create_table('rombongan_belajar',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('rombongan_belajar', TRUE);
	}
}