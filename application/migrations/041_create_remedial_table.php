<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_remedial_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'remedial_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'kompetensi_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rombongan_belajar_id'	=> array(
				'type' => 'uuid',
			),
			'mata_pelajaran_id'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'siswa_id'	=> array(
				'type' => 'uuid',
			),
			'nilai'	=> array(
				'type' => 'TEXT'
			),
			'rerata_akhir'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rerata_remedial'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('remedial_id', TRUE);
		$this->dbforge->create_table('remedial',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('remedial', TRUE);
	}
}