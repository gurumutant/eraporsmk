<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_nilai_sikap_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'nilai_sikap_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rombongan_belajar_id'	=> array(
				'type' => 'uuid',
			),
			'mata_pelajaran_id'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'siswa_id'	=> array(
				'type' => 'uuid',
			),
			'guru_id'	=> array(
				'type' => 'uuid',
			),
			'tanggal_sikap'	=> array(
				'type' => 'DATE',
			),
			'butir_sikap'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'opsi_sikap'	=> array(
				'type' => 'INT',
				'constraint' => 11,
				'null'	=> true
			),
			'uraian_sikap'	=> array(
				'type' => 'TEXT',
				'null'	=> true
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'last_sync' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('nilai_sikap',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('nilai_sikap', TRUE);
	}
}