<?php 
class MY_Controller extends CI_Controller {
	function __construct() {
		parent::__construct();
		check_installer();
		$this->load->library('ion_auth');
	}
}
class Backend_Controller extends MY_Controller {
	protected $admin_folder 		= 'backend';
	protected $sinkronisasi_folder 	= 'sinkronisasi';
	protected $styles  				= 'backend/partials/css';
	protected $header 				= 'backend/partials/header';
	protected $sidebar 				= 'backend/partials/sidebar';
	protected $footer 				= 'backend/partials/footer';
	protected $admin_tpl 			= 'admin_tpl';
	protected $modal_tpl 			= 'modal_tpl';
	protected $blank_tpl 			= 'blank_tpl';
	function __construct() {
		parent::__construct();
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit', '-1');
		$this->template->set_partial('styles', $this->styles)
        ->set_partial('header', $this->header)
        ->set_partial('sidebar', $this->sidebar)
        ->set_partial('footer', $this->footer);
		//login check
		$exception_urls = array(
			'admin/auth',
		);
		if(!isset($this->sekolah)){
			redirect('core/write_autoload');
		}
		$sekolah = $this->sekolah->get_all();
		if(!$sekolah){
			redirect('register');
		}
		if (in_array(uri_string(), $exception_urls) == FALSE) {
			if(!$this->ion_auth->logged_in()){
				redirect('admin/auth/');
			}
		}
	}
}
class Auth_Controller extends MY_Controller {
	protected $auth_folder 	= 'auth';
	protected $styles  		= 'auth/partials/css';
	protected $footer 		= 'auth/partials/footer';
	protected $auth_tpl		= 'auth_tpl';
	function __construct() {
		parent::__construct();
		$this->template->set_partial('styles', $this->styles)
        ->set_partial('footer', $this->footer);
	}
}